<?php

use Illuminate\Support\Facades\Route;
// Route::namespace('Admin')->group(function(){
//     route::get('admin',function(){
//         return 1;
//     });
// });

Route::namespace('Site')->group(function () {
    Route::get('/login', 'UserController@login')->name('site.login');
    Route::post('/login', 'UserController@login')->name('site.login');
    Route::post('/logout', 'UserController@logout')->name('site.logout');
    Route::get('/register', 'UserController@register')->name('site.register');
    Route::post('/register', 'UserController@register')->name('site.register');

    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/danh-muc-bai-viet/{slug}', 'ContentController@categoryContent')->name('categoryContent');
    Route::get('/chi-tiet-bai-viet/{slug}', 'ContentController@detailContent')->name('detailContent');
    Route::get('/page/{page_id}', 'HomeController@page')->name('home.page');
    Route::get('/tim-kiem', 'ProductController@search')->name('search');
    Route::get('/listProductByCategory/{category_slug}', 'ProductController@listProductByCategory')->name('product');
    Route::get('/productDetail/{slug}', 'ProductController@productDetail')->name('detailProduct');
    Route::get('/certificate', 'CertificateController@index')->name('certificate');
    Route::get('/detailCertificate/{certificate_slug}', 'CertificateController@detailCertificate')->name('detailCertificate');
    Route::get('/feedback', 'FeedbackController@index')->name('feedback');
    Route::get('/list-file', 'HomeController@listFile')->name('file');
    Route::get('/video', 'HomeController@listVideo')->name('video');
    Route::get('/quang-cao', 'QcController@index')->name('qc');
    Route::get('/chi-tiet-quang-cao/{id}', 'QcController@detailQc')->name('detailQc');
});
Route::namespace('Admin')->prefix('admin')->group(function () {
    Route::get('/login', 'LoginController@view')->name('login');
    Route::post('/logout', 'LoginController@logout')->name('admin.logout');
    Route::post('/checkLogin', 'LoginController@checkLogin')->name('checkLogin');
    Route::get('/logOut', 'LoginController@logout')->name('logout');
});

Route::prefix('admin')->namespace('Admin')->name('admin.')->middleware('checkLoginAdmin')->group(function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/page/{page_id}', 'PageController@page')->name('page');
    Route::get('/nextPage/{page_id}', 'PageController@nextPage')->name('nextPage');
    Route::get('/prevPage/{page_id}', 'PageController@prevPage')->name('prevPage');

    Route::prefix('categoryProduct')->name('categoryProduct.')->group(function () {
        Route::get('/', 'CategoryProductController@index')->name('index');
        Route::get('/edit/{categoryProduct_id}', 'CategoryProductController@edit')->name('edit');
        Route::post('/store', 'CategoryProductController@store')->name('store');
        Route::PUT('/update/{categoryProduct_id}', 'CategoryProductController@update')->name('update');
        Route::delete('/delete/{categoryProduct_id}', 'CategoryProductController@delete')->name('delete');
    });
    Route::prefix('product')->name('product.')->group(function () {
        Route::get('/', 'ProductController@index')->name('index');
        Route::get('/create', 'ProductController@create')->name('create');
        Route::get('/edit/{product_id}', 'ProductController@edit')->name('edit');
        Route::get('/page/{page_id}', 'ProductController@page')->name('page');
        Route::post('/store', 'ProductController@store')->name('store');
        Route::post('/storeAddInCategory/{category_id}', 'ProductController@storeAddInCategory')->name('storeAddInCategory');

        Route::PUT('/update/{product_id}', 'ProductController@update')->name('update');
        Route::delete('/delete/{product_id}', 'ProductController@delete')->name('delete');
    });


    Route::prefix('categoryContent')->name('categoryContent.')->group(function () {
        Route::get('/', 'CategoryContentController@index')->name('index');
        Route::get('/edit/{categoryContent_id}', 'CategoryContentController@edit')->name('edit');
        Route::post('/store', 'CategoryContentController@store')->name('store');
        Route::PUT('/update/{categoryContent_id}', 'CategoryContentController@update')->name('update');
        Route::delete('/delete/{categoryContent_id}', 'CategoryContentController@delete')->name('delete');
    });
    Route::prefix('content')->name('content.')->group(function () {
        Route::get('/', 'ContentController@index')->name('index');
        Route::get('/create', 'ContentController@create')->name('create');
        Route::get('/edit/{Content_id}', 'ContentController@edit')->name('edit');
        Route::get('/page/{page_id}', 'ContentController@page')->name('page');
        Route::post('/store', 'ContentController@store')->name('store');
        Route::post('/storeAddInCategory/{category_id}', 'ContentController@storeAddInCategory')->name('storeAddInCategory');

        Route::PUT('/update/{Content_id}', 'ContentController@update')->name('update');
        Route::delete('/delete/{Content_id}', 'ContentController@delete')->name('delete');
    });

    Route::prefix('certificate')->name('certificate.')->group(function () {
        Route::get('/', 'CertificateController@index')->name('index');
        Route::get('/page/{page_id}', 'CertificateController@page')->name('page');
        Route::get('/create', 'CertificateController@create')->name('create');
        Route::get('/edit/{certificate_id}', 'CertificateController@edit')->name('edit');
        Route::get('/editItem/{item_id}', 'CertificateController@editItem')->name('editItem');
        Route::post('/store', 'CertificateController@store')->name('store');
        Route::post('/addItem', 'CertificateController@addItem')->name('addItem');
        Route::PUT('/update/{certificate_id}', 'CertificateController@update')->name('update');
        Route::PUT('/updateItem/{item_id}', 'CertificateController@updateItem')->name('updateItem');
        Route::delete('/delete/{certificate_id}', 'CertificateController@delete')->name('delete');
        Route::delete('/deleteItem/{item_id}', 'CertificateController@deleteItem')->name('deleteItem');
    });
    Route::prefix('feedback')->name('feedback.')->group(function () {
        Route::get('/', 'FeedbackController@index')->name('index');
        Route::get('/page/{page_id}', 'FeedbackController@page')->name('page');

        Route::get('/edit/{feedback_id}', 'FeedbackController@edit')->name('edit');
        Route::post('/store', 'FeedbackController@store')->name('store');
        Route::PUT('/update/{feedback_id}', 'FeedbackController@update')->name('update');
        Route::delete('/delete/{feedback_id}', 'FeedbackController@delete')->name('delete');
    });
    Route::prefix('file')->name('file.')->group(function () {
        Route::get('/', 'FileController@index')->name('index');
        Route::get('/page/{page_id}', 'FileController@page')->name('page');

        Route::get('/edit/{id}', 'FileController@edit')->name('edit');
        Route::get('/edit-file/{id}', 'FileController@editFile')->name('edit_file');
        Route::post('/store', 'FileController@store')->name('store');
        Route::post('/store-file', 'FileController@storeFile')->name('store_file');
        Route::PUT('/update/{id}', 'FileController@update')->name('update');
        Route::PUT('/update-file/{id}', 'FileController@updateFile')->name('update_file');
        Route::delete('/delete/{id}', 'FileController@delete')->name('delete');
        Route::delete('/delete-file/{id}', 'FileController@deleteFile')->name('delete_file');
        Route::get('/list-file/{category_file_id}', 'FileController@listFile')->name('list_file');
    });
    Route::prefix('video')->name('video.')->group(function () {
        Route::get('/', 'VideoController@index')->name('index');
        Route::get('/edit/{id}', 'VideoController@edit')->name('edit');
        Route::post('/store', 'VideoController@store')->name('store');
        Route::PUT('/update/{id}', 'VideoController@update')->name('update');
        Route::delete('/delete/{id}', 'VideoController@delete')->name('delete');
    });
    Route::prefix('adv')->name('advertisement.')->group(function () {
        Route::get('/', 'AdvController@index')->name('index');
        Route::get('/edit/{id}', 'AdvController@edit')->name('edit');
        Route::post('/store', 'AdvController@store')->name('store');
        Route::PUT('/update/{id}', 'AdvController@update')->name('update');
        Route::delete('/delete/{id}', 'AdvController@delete')->name('delete');
    });

    Route::prefix('slider')->name('slider.')->group(function () {
        Route::get('/', 'SliderController@index')->name('index');
        Route::get('/edit/{slider_id}', 'SliderController@edit')->name('edit');
        Route::post('/store', 'SliderController@store')->name('store');
        Route::PUT('/update/{slider_id}', 'SliderController@update')->name('update');
        Route::PUT('/active/{slider_id}', 'SliderController@active')->name('active');
        Route::delete('/delete/{slider_id}', 'SliderController@delete')->name('delete');
    });
    Route::prefix('setting')->name('setting.')->group(function () {
        Route::get('/', 'SettingController@index')->name('index');
        Route::post('/store', 'SettingController@store')->name('store');
    });
});
// Route::get('/video', function () {
//     return view('site.video');
// })->name('video');
// Route::get('/quang-cao', function () {
//     return view('site.qc');
// })->name('qc');
