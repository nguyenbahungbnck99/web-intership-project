<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
    <!-- Loading Screen -->
    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="{{ asset('template/slider/svg/loading/static-svg/spin.svg')}}" />
    </div>
    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
        <div>
            <img data-u="image" src="{{ asset('template/slider/img/gallery/980x380/001.jpg')}}" />
        </div>
        <div data-p="170.00">
            <img data-u="image" src="{{ asset('template/slider/img/gallery/980x380/002.jpg')}}" />
        </div>
        <div>
            <img data-u="image" src="{{ asset('template/slider/img/gallery/980x380/003.jpg')}}" />
        </div>
        <div>
            <img data-u="image" src="{{ asset('template/slider/img/gallery/980x380/004.jpg')}}" />
        </div>
        <div>
            <img data-u="image" src="{{ asset('template/slider/img/gallery/980x380/005.jpg')}}" />
        </div>
        <div>
            <img data-u="image" src="{{ asset('template/slider/img/gallery/980x380/006.jpg')}}" />
        </div>
        <div>
            <img data-u="image" src="{{ asset('template/slider/img/gallery/980x380/007.jpg')}}" />
        </div>
        <div data-b="0">
            <img data-u="image" src="{{ asset('template/slider/img/gallery/980x380/008.jpg')}}" />
        </div>
        <div data-p="170.00">
            <img data-u="image" src="{{ asset('template/slider/img/gallery/980x380/009.jpg')}}" />
        </div>
        <div data-p="170.00">
            <img data-u="image" src="{{ asset('template/slider/img/gallery/980x380/010.jpg')}}" />
        </div>
        <div style="background-color:#ff7c28;">
            <div style="position:absolute;top:50px;left:50px;width:450px;height:62px;z-index:0;font-size:16px;color:#000000;line-height:24px;text-align:left;padding:5px;box-sizing:border-box;">Photos in this slider are to demostrate jssor slider,<br />
                which are not licensed for any other purpose.
            </div>
        </div>
    </div>
    <!-- Bullet Navigator -->
    <div data-u="navigator" class="jssorb052" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
            </svg>
        </div>
    </div>
    <!-- Arrow Navigator -->
    <div data-u="arrowleft" class="jssora053" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
        </svg>
    </div>
    <div data-u="arrowright" class="jssora053" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
        </svg>
    </div>
</div>

