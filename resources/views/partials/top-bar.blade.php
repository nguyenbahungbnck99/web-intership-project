<div id="top-bar" class="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <ul class="top-info text-center text-md-left">
                    <li>
                        <i class="far fa-envelope"></i>
                        <p class="info-text">{{$infor['email']}}</p>
                    </li>
                    <li>
                        <i class="fas fa-phone-alt"></i>
                        <p class="info-text">{{$infor['phone']}}</p>
                    </li>
                </ul>
            </div>
            <!--/ Top info end -->

            <div class="col-lg-4 col-md-4 top-social text-center text-md-right d_none">
                <ul class="list-unstyled">
                    <li>
                        <a title="Facebook" href="{{$infor['fb_ref']}}" target="_blank">
                            <span class="social-icon bold">
                                <i class="fab fa-facebook-f"></i> Facebook
                            </span>
                        </a>
                        {{-- <a title="Twitter" href="#">
                            <span class="social-icon"><i class="fab fa-twitter"></i></span>
                        </a>
                        <a title="Instagram" href="#">
                            <span class="social-icon"><i class="fab fa-instagram"></i></span>
                        </a>
                        <a title="Linkdin" href="#">
                            <span class="social-icon"><i class="fab fa-github"></i></span>
                        </a> --}}
                    </li>
                </ul>
            </div>
            <!--/ Top social end -->
        </div>
        <!--/ Content row end -->
    </div>
    <!--/ Container end -->
</div>
