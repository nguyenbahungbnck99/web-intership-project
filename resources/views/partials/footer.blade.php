<footer id="footer" class="footer bg-overlay">
    <div class="footer-main">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-5 col-md-5 col-12 ">
                    <img loading="lazy" class="footer-logo" src="{{ $infor['logo'] }}" alt="Constra">
                    <div class="footer-social">
                        <p>
                            {{$infor['introduce']}}

                        </p>
                    </div><!-- Footer social end -->
                </div><!-- Col end -->

                <div class="col-lg-2 col-12 col-md-2 ">
                    <h3 class="widget-title-footer">Danh mục</h3>
                    <li class="list-style-none mb-2"><a href="{{ route('home') }}" class="color-theme">Trang chủ</a>
                    </li>
                    <li class="list-style-none mb-2"><a href="#" class="color-theme">Sản phẩm</a></li>

                </div><!-- Col end -->

                <div class="col-lg-5 col-md-5 col-12 ">
                    <h3 class="widget-title-footer">Thông tin</h3>
                    <div class="label-footer">Địa chỉ</div>
                    <div class="title-footer">
                        {{$infor['address']}}

                    </div>
                    <div class="label-footer">Hotline</div>
                    <div class="title-footer">
                        {{$infor['phone']}}
                    </div>

                </div><!-- Col end -->
            </div><!-- Row end -->
        </div><!-- Container end -->
    </div><!-- Footer main end -->

    <div class="copyright">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="copyright-info text-center">
                        <script>
                            document.write(new Date().getFullYear())
                        </script>,
                        {{$infor['copyright']}}
                    </div>
                </div>

                {{-- <div class="col-md-12">
                    <div class="copyright-info text-center">
                        <span>Distributed by <a href="#">CH APP</a></span>
                    </div>
                </div> --}}


            </div><!-- Row end -->

            <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top position-fixed">
                <button class="btn btn-primary" title="Back to Top">
                    <i class="fa fa-angle-double-up"></i>
                </button>
            </div>
            {{-- <div class="link-to-ctv position-fixed">
                <a href="http://ctv.healthmoomy.vn/dang-ki" target="_blank" class="rounded-circle">
                    <div class="box-link">
                        <span class="text-ctv">Trở thành cộng tác viên của chúng tôi?</span>

                        <img src="{{ asset('images/ctv.jpg') }}" alt="ctv" class="icon-ctv rounded-circle"
                            title="Bạn muốn trở thành cộng tác viên của chúng tôi?">
                    </div>

                </a>
            </div> --}}
        </div><!-- Container end -->
    </div><!-- Copyright end -->
</footer><!-- Footer end -->
