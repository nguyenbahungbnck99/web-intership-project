<header id="header" class="header-one">
    <div class="site-navigation">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-dark p-0">
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target=".navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <a href="/" class="d_md_none">
                            <img src="{{ asset('images/logo.png') }}" class="w-10 d_md_none" alt="Logo" width="140px">

                        </a>
                        <div id="navbar-collapse" class="collapse navbar-collapse">
                            <div class="logo d_none">
                                <a href="/">
                                    <img src="{{ asset('images/logo.png') }}" class="w-10" alt="Logo">
                                </a>
                            </div>
                            <ul class="nav navbar-nav ml-auto nav-center">
                                <li class="nav-item dropdown active">
                                    <a href="/" class="nav-link ">Trang chủ </a>
                                </li>

                                <li class="nav-item dropdown">
                                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Sản phẩm
                                        <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu" role="menu">

                                        @foreach ($category_products as $product)
                                            <li>
                                                <a href="{{ route('product', $product['slug']) }}">
                                                    {{ $product['name'] }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Tin Tức
                                        <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu" role="menu">

                                        @foreach ($category_contents as $category)
                                            <li>
                                                <a href="{{ route('categoryContent', $category['slug']) }}">
                                                    {{ $category['name'] }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @php
                                    $name = Cookie::get('name_customer');
                                @endphp
                                @if (empty($name))
                                    <li class="nav-item dropdown">
                                        <a href="{{ route('site.register') }}" class=" ">Đăng ký </a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a href="{{ route('site.login') }}" class=" ">Đăng nhập </a>
                                    </li>
                                @else
                                    <a href="#" class="mr-4">{{ $name }}</a>
                                    <a href="#" data-toggle="modal" data-target="#logoutModal">Đăng xuất</a>

                                @endif

                                <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Đăng xuất?</h5>
                                                <button class="close" type="button" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">Bạn có chắc chắn muốn đăng xuất?</div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" type="button"
                                                    data-dismiss="modal">Cancel</button>
                                                <form action="{{ route('site.logout') }}" method="post">
                                                    @method('post')
                                                    @csrf
                                                    <button class="btn btn-primary" href="">Đăng xuất</button>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <li class="nav-item">
                                </li>
                                <li class="nav-item">
                                    <form action="{{ route('search') }}" method="get" id="form-search-header">

                                        <div class="input-group ">
                                            <input type="text" class="form-control input-search-header"
                                                name="key_search" placeholder="Tìm kiếm" autocomplete="off">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text btn-search-header">
                                                    <i class="fa fa-search"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <!--/ Col end -->
            </div>
            <!--/ Row end -->

        </div>
        <!--/ Container end -->

    </div>
    <!--/ Navigation end -->
</header>
