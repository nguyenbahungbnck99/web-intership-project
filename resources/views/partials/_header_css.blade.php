<link rel="icon" type="image/png" href="{{ asset('template/constra/images/favicon.png') }}">

<!-- CSS====== -->
<!-- Bootstrap -->
<link rel="stylesheet" href="{{ asset('template/constra/plugins/bootstrap/bootstrap.min.css') }}">
<!-- FontAwesome -->
<link rel="stylesheet" href="{{ asset('template/constra/plugins/fontawesome/css/all.min.css') }}">
<!-- Animation -->
<link rel="stylesheet" href="{{ asset('template/constra/plugins/animate-css/animate.css') }}">
<!-- slick Carousel -->
<link rel="stylesheet" href="{{ asset('template/constra/plugins/slick/slick.css') }}">
<link rel="stylesheet" href="{{ asset('template/constra/plugins/slick/slick-theme.css') }}">
<!-- Colorbox -->
<link rel="stylesheet" href="{{ asset('template/constra/plugins/colorbox/colorbox.css') }}">
<!-- Template styles-->
<link rel="stylesheet" href="{{ asset('template/constra/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
<link rel="stylesheet" href="{{ asset('css/header.css') }}">
<link rel="stylesheet" href="{{ asset('css/footer.css') }}">
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" href="{{ asset('css/product.css') }}">
<link rel="stylesheet" href="{{ asset('css/category.css') }}">
