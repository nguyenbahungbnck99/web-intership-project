<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{ asset('template/AdminLTE/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Kho gỗ Hiền Tiến</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('template/AdminLTE/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">
                    {{-- {{ Auth::user()->name }} --}}
                </a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="/admin" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Tổng quan
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ asset('admin/san-pham/danh-sach/1') }}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt fa-fw"></i>
                        <p>
                            Sản phẩm
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ asset('admin/don-hang/danh-sach/1') }}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt fa-fw"></i>
                        <p>
                            Quản lý đơn hàng
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Bài viết
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ asset('admin/bai-dang/danh-muc/1') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Danh mục</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ asset('admin/bai-dang/danh-sach/1') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Bài viết</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Gói sữa doanh nghiệp
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('danh-sach-goi-sua') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Danh sách gói sữa</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('them-goi-sua') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Thêm mới gói sữa</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ asset('admin/mua-goi/danh-sach/1') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Danh sách mua gói</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Cộng tác viên
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ asset('admin/ctv/danh-sach') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Danh sách doanh số</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ asset('admin/ctv/danh-sach-cong-doanh-thu/1') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Danh sách cộng doanh số</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ asset('admin/ctv/danh-sach-dinh-danh') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Danh sách định danh</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ asset('admin/ctv/danh-sach-cay-he-thong') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Danh sách cây hệ thống</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
