<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Basic Page Needs
================================================== -->
    <meta charset="utf-8">
    <title>@yield('title-page')</title>

    <!-- Mobile Specific Metas
================================================== -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Construction Html5 Template">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">


    {{-- seo facebook --}}


    @if (!empty($information))
        <meta property="og:url" content="{{ $information['url'] }}" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="{{ $information['title'] }}" />

        <meta property="og:description" content="{{ $information['description'] }}" />
        <meta property="og:image" content="{{ $information['image'] }}" />
    @endif

    <!-- Favicon
================================================== -->

    @include('partials._header_css')
    @yield('after-css')

    <link rel="stylesheet" href="{{ asset('css/h-responsive.css') }}">

</head>

<body>
    <div class="body-inner">

        @include('partials.top-bar')
        <!--/ Topbar end -->
        @include('partials.header')
        <!--/ Header end -->
        @include('partials.slider')

        @yield('content')

        @include('partials.footer')


        @include('partials.scripts')
        @yield('after-scripts')
        <script>
            $('.btn-search-header').click(function() {
                $('#form-search-header').submit();
            });
            $(document).ready(function() {
                // $('.box-product').matchHeight();
                $('.line-two').matchHeight();
                $('.item-height').matchHeight();
                $('.item-hot-product-image').matchHeight();
                $('.item-hot-product-content').matchHeight();
                $('.item-height-image').matchHeight();
                $('.item-height-certificate').matchHeight();
                $('.box-content').matchHeight();
            });
        </script>
    </div><!-- Body inner end -->
</body>

</html>
