@extends('admin.layouts.app')
@section('title-page', 'Danh sách quảng cáo')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        {{-- <div class="card-body">
                            <form action="" method="GET" id="formSearchData">
                                <div class="row d-flex align-items-end">
                                    <div class="col-12 col-md-2">
                                        <label>Tiêu đề</label>
                                        <div class="input-group">
                                            <input type="text" autocomplete="off" class="form-control" name="title"
                                                value="{{ request('title') }}" placeholder="Tiêu đề" id="startdatepicker">
                                        </div>
                                        <!-- input-group -->
                                    </div>


                                    <button class="btn btn-primary float-right ml-auto"
                                        style="height: 40px; float:right">Search</button>

                                </div>
                            </form>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalAddCategoryFile">
                <div class="modal-dialog modal-dialog-centered ">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Thêm mới quảng cáo</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ route('admin.advertisement.store') }}" method="post" id="formAddCertificateItem"
                                enctype="multipart/form-data">
                                @csrf
                                @method('post')
                                <input type="hidden" name="certificate_id" id="certificate_id">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 ">
                                            <div class="form-group">
                                                <label for="inputName">Tên</label>
                                                <input type="text" class="form-control" id="inputName" required name="name"
                                                    placeholder="Tên chứng chỉ">
                                            </div>
                                        </div>
                                        <div class="col-12 ">
                                            <div class="form-group">
                                                <label for="imageInput">Ảnh</label>
                                                <div class="image-input">
                                                    <input type="file" accept="image/*" id="imageInput" name="main_image" required>
                                                    <img src="" class="image-preview" style="max-width:100px">
        
                                                </div>
        
                                            </div>
                                        </div>
                                        <div class="col-12 ">
                                            <div class="form-group">
                                                <label for="inputName">Nội dung</label>
                                                <textarea name="content" class="form-control " rows="5">
                                                </textarea>
                                            </div>
                                        </div>
                                        <div class="col-12 ">
                                            <div class="form-group">
                                                <label for="inputName">Mô tả</label>
                                                <textarea name="description" class="form-control " rows="5">
                                                </textarea>
                                            </div>
                                        </div>
                                        <div class="col-12 ">
                                            <div class="form-group">
                                                <label for="inputListImage">Danh sách ảnh mô tả</label>
                                                <div class="image-input">
                                                    <input type="file" accept="image/*" id="inputListImage" multiple
                                                        id="imageInput" name="list_image[]">
                                                    <div class="list-image">
    
                                                    </div>
                                                </div>
    
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- /.card-body -->
        
                                <div class="modal-footer justify-content-between">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    <button type="submit" class="btn btn-primary">Lưu lại</button>
                                </div>
                            </form>
                        </div>
                        {{-- <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn btn-primary">Lưu lại</button>
                        </div> --}}
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h3 class="card-title">Danh sách quảng cáo</h3>
                            <div class="float-right ml-auto">
                                <a class="col-md-2 col-12" href="#" data-toggle="modal" data-target="#modalAddCategoryFile">
                                    <button class="btn btn-primary btn-sm" style="">Thêm mới + </button>
                                </a>
                            </div>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="table" class="table table-bordered table-hover ">

                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col" class="text-center">Ảnh</th>
                                        <th scope="col-4">Tên</th>
                                        <th scope="col">Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($responseData as $index => $adv)
                                    <tr>
                                        <th scope="row" style="vertical-align: middle ">{{ ++$index }}</th>
                                        <td class="products" style="vertical-align: middle "><img
                                                src="{{ $adv['main_image'] }}" width="300" class="object-fit-cover">
                                        </td>
                                        <td style="vertical-align: middle ">{{ $adv['name'] }}</td>



                                        {{-- <td style="vertical-align: middle ">
                                        <a href="{{ asset('admin/san-pham/cam-nhan/' . $feedback['id']) }}"
                                            class="btn btn-info btn-circle btn-sm">
                                            <i class="fas fa-info-circle"></i>
                                        </a>
                                    </td> --}}
                                        <td style="vertical-align: middle ">
                                            <a  href="{{ route('admin.advertisement.edit', $adv['id']) }}"
                                                class="btn btn-info btn-circle btn-sm editFeedback">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a class="btn btn-danger btn-circle btn-sm" href="" data-toggle="modal"
                                                data-target="#delete{{ $adv['id'] }}">
                                                <i class="fas fa-trash"></i>

                                            </a>
                                        </td>
                                        <!-- delete Modal-->
                                        <div class="modal fade" id="delete{{ $adv['id'] }}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Bạn có chắn chắn
                                                            muốn
                                                            xoá?</h5>
                                                        <button class="close" type="button" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">Danh mục file: {{ $adv['name'] }}</div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">Huỷ</button>
                                                        <form
                                                            action="{{ route('admin.advertisement.delete', $adv['id']) }}"
                                                            method="post">
                                                            @csrf
                                                            @method('delete')
                                                            <button class="btn btn-danger" href="">Xoá</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                            {{-- <ul class="pagination justify-content-end">
                                @php
                                    $totalPage = $responseData['last_page'];
                                    $currentPage = $responseData['current_page'];

                                @endphp
                                <li class="page-item"><a class="page-link" href="#">Trước</a></li>
                                @for ($i = 1; $i <= $totalPage; $i++)
                                    <li class="page-item @if ($i==$currentPage) {{ 'active' }} @endif"><a class="page-link"
                                            href="{{ asset('/admin/san-pham/danh-sach/' . $i) }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                <li class="page-item"><a class="page-link" href="#">Sau</a></li>
                            </ul> --}}
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <div class="modal fade" id="modalUpdateCategoryFile">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cập nhật danh mục file</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="formUpdateFeedback" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputName">Tên</label>
                                        <input type="text" class="form-control" id="inputName" required name="name"
                                            placeholder="Tên chứng chỉ">
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="imageInput">Ảnh</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="imageInput" name="main_image" required>
                                            <img src="" class="image-preview" style="max-width:100px">

                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputName">Nội dung</label>
                                        <textarea name="content" class="form-control " rows="5">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputName">Mô tả</label>
                                        <textarea name="description" class="form-control " rows="5">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputListImage">Danh sách ảnh mô tả</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="inputListImage" multiple
                                                id="imageInput" name="list_image[]">
                                            <div class="list-image">

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('after-scripts')
<script src="{{ asset('template/AdminLTE/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
         $(function() {
            $('#imageInput').on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('.image-preview').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                    $('.image-button').css('display', 'none');
                    $('.image-preview').css('display', 'block');
                    $('.change-image').css('display', 'block');
                }
            });
            $('.select2').select2();
            $('.change-image').on('click', function() {
                $control = $(this);
                $('#imageInput').val('');
                $preview = $('.image-preview');
                $preview.attr('src', '');
                $preview.css('display', 'none');
                $control.css('display', 'none');
                $('.image-button').css('display', 'block');
            });
            $('.formatPrice').priceFormat({
                prefix: '',
                centsLimit: 0,
                thousandsSeparator: '.'
            });
            // Summernote
            $('.summernote').summernote()
        })
        $('.editFeedback').click(function(e) {
            e.preventDefault();

            let href = $(this).attr('href');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: href,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(res) {
                    let feedback = res.data;
                    $('#formUpdateFeedback input[name=name]').val(feedback.name);
                    $('#formUpdateFeedback textarea[name=content]').val(feedback.content);
                    $('#formUpdateFeedback textarea[name=description]').val(feedback.description);
                    $('#formUpdateFeedback .image-preview').attr('src', feedback.main_image);
                    $('#formUpdateFeedback').attr('action', res.action);
                    $('#modalUpdateCategoryFile').modal('show');
                    console.log(res);
                },
                dataType: false
            });
        });
    </script>
@endsection
