@extends('admin.layouts.app')
@section('title-page', 'Cài đặt thông tin website')
@section('after-css')
    <link rel="stylesheet" href="{{ asset('template/AdminLTE/plugins/summernote/summernote-bs4.min.css') }}">
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h3 class="card-title">Cài đặt thông tin website</h3>


                        </div>
                        <!-- /.card-header -->
                        <div class="row">
                            <div class="offset-md-2"></div>
                            <div class="col-12 col-md-8">
                                <div class="card-body">
                                    <form action="{{ route('admin.setting.store') }}" method="post"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label class="form-label">Email webiste </label>
                                            <input name="email" placeholder="Email webiste ..." type="text"
                                                value="{{ $infor['email'] }}" class="form-control" value="">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Số điện thoại website</label>
                                            <input name="phone" placeholder="Số điện thoại website ..." type="text"
                                                class="form-control" value="{{ $infor['phone'] }}">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Địa chỉ cửa hàng</label>
                                            <input name="address" placeholder="Địa chỉ cửa hàng ..." type="text"
                                                class="form-control" value="{{ $infor['address'] }}">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Link trang Facebook </label>
                                            <input name="fb_ref" placeholder="Link trang Facebook ..." type="text"
                                                class="form-control" value="{{ $infor['fb_ref'] }}">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Mã số Page Facebook(Add chat message)</label>
                                            <label class="form-label"><a href="https://lucidgen.com/cach-nhung-messenger-vao-website" target="_blank">Hướng dẫn thêm tên miền website vào phần Miền trang web </a></label>

                                            <input name="page_id" placeholder="Mã số Page Facebook(Add chat message) ..."
                                                type="text" class="form-control" value="{{ $infor['fb_page_id'] }}">

                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Bản quyền website</label>
                                            <input name="copyright" placeholder="Bản quyền website ..." type="text"
                                                class="form-control" value="{{ $infor['copyright'] }}">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Giới thiệu website(footer)</label>
                                            <textarea name="introduce" id=""  class="form-control" placeholder="Giới thiệu website(footer)..." rows="4">
                                                {{ $infor['introduce'] }}
                                            </textarea>
                                            {{-- <input name="introduce" placeholder="Giới thiệu website ..." type="text"
                                                class="form-control" value="{{ $infor['introduce'] }}"> --}}
                                        </div>
                                        <div class="form-group">
                                            <label for="">Logo</label>
                                            <input type="file" class="form-control imageInput" name="logo"
                                                placeholder="Logo">
                                            <div class="d-flex mt-4 justify-content-center align-items-center">
                                                <img src="{{ $infor['logo'] }}" width="240" class="image-preview " alt="">
                                            </div>
                                        </div>
                                        <div class="col-12 text-center">
                                            <button type="submit" class="btn btn-primary">Cập nhật</button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- /.card-body -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

@endsection

@section('after-scripts')

    <script>
        $(function() {

            $('.imageInput').on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('.image-preview').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                }
            });
        })
    </script>

@endsection
