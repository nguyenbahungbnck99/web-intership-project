@extends('admin.layouts.app')
@section('title-page', 'Thêm mới sản phẩm')

@section('content')

    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Thông tin sản phẩm</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ route('admin.product.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="inputNameProduct">Tên sản phẩm</label>
                                            <input type="text" class="form-control" id="inputNameProduct" name="name"
                                                placeholder="Tên sản phẩm">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="inputNameProduct">Loại sản phẩm</label>
                                            <select name="category_id" class="d-block select2 form-control" id="">
                                                @foreach ($category_products as $category)
                                                    <option value="{{ $category['id'] }}">
                                                        {{ $category['name'] }}
                                                    </option>

                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="inputSell">Giá bán</label>
                                            <input type="text" class="form-control formatPrice" id="inputSell" name="price"
                                                placeholder="Giá bán">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="inputSale">Giá khuyến mãi</label>
                                            <input type="text" class="form-control formatPrice" id="inputSale"
                                                name="price_sale" placeholder="Giá khuyến mãi">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputFile">Ảnh(Kích cỡ khuyên dùng >= 420*420)</label>
                                            <div class="image-input">
                                                <input type="file" accept="image/*" title="Kích cỡ khuyên dùng >= 420*420" id="exampleInputFile" name="image">
                                                <img src="" class="image-preview" style="max-width:100px">

                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="inputListImage">Danh sách ảnh(Kích cỡ khuyên dùng >= 420*420)</label>
                                            <div class="image-input">
                                                <input type="file" accept="image/*" title="Kích cỡ khuyên dùng >= 420*420" id="inputListImage" multiple
                                                    id="imageInput" name="list_images[]">
                                                <div class="list-image">

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="inputInventory">Tồn kho</label>
                                            <input type="text" class="form-control formatPrice" id="inputInventory"
                                                name="total_inventory" placeholder="Tồn kho">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="inputUserObject">Đối tượng sử dụng</label>
                                            <input type="text" class="form-control " id="inputUserObject" name="user_object"
                                                placeholder="Đối tượng sử dụng">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="inputUserManual">Hướng dẫn sử dụng</label>
                                            <textarea name="user_manual" class="form-control summernote" cols="4" rows="10">
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="inputUsefulness">Công dụng</label>
                                            <textarea name="usefulness" class="form-control summernote" cols="4" rows="10">
                                        </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="">Mô tả sản phẩm</label>
                                            <textarea name="description" class="form-control summernote" cols="4" rows="10">
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="">Chi tiết sản phẩm</label>
                                            <textarea name="content" class="form-control summernote" cols="4" rows="10">
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Lưu</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('after-css')
    <link rel="stylesheet" href="{{ asset('template/AdminLTE/plugins/summernote/summernote-bs4.min.css') }}">
@endsection
@section('after-scripts')
    <script src="{{ asset('template/AdminLTE/plugins/summernote/summernote-bs4.min.js') }}"></script>

    <script>
        $(function() {
            $('#imageInput').on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('.image-preview').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                    $('.image-button').css('display', 'none');
                    $('.image-preview').css('display', 'block');
                    $('.change-image').css('display', 'block');
                }
            });
            $('.select2').select2();
            $('.change-image').on('click', function() {
                $control = $(this);
                $('#imageInput').val('');
                $preview = $('.image-preview');
                $preview.attr('src', '');
                $preview.css('display', 'none');
                $control.css('display', 'none');
                $('.image-button').css('display', 'block');
            });
            $('.formatPrice').priceFormat({
                prefix: '',
                centsLimit: 0,
                thousandsSeparator: '.'
            });
            // Summernote
            $('.summernote').summernote()
        })
    </script>

@endsection
