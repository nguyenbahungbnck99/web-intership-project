@extends('admin.layouts.app')
@section('title-page', 'Danh sách bài viết')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="GET" id="formSearchData">
                                <div class="row d-flex align-items-end">

                                    <div class="col-12 col-md-2">
                                        <label>Tên bài viết</label>
                                        <div class="input-group">
                                            <input type="text" autocomplete="off" class="form-control" name="title"
                                                value="{{ request('title') }}" placeholder="Tên bài viết" id="">
                                        </div>
                                        <!-- input-group -->
                                    </div>

                                    <div class="col-12 col-md-4">
                                        <label>Loại bài viết</label>
                                        <div class="input-group">
                                            <select name="category_id" class="select2 d-block form-control">
                                                <option value="">--Loại bài viết--</option>
                                                @foreach ($category_contents as $cate)
                                                    @if ($cate['children'])
                                                        @foreach ($cate['children'] as $item)
                                                            <option value="{{ $item['id'] }}" @if ($item['id'] == request('category_id')) selected @endif>
                                                                {{ $cate['name'] }}--
                                                                {{ $item['name'] }}</option>

                                                        @endforeach
                                                    @else
                                                        <option value="{{ $cate['id'] }}" @if ($cate['id'] == request('category_id')) selected @endif>
                                                            {{ $cate['name'] }}</option>

                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- input-group -->
                                    </div>
                                    <button class="btn btn-primary float-right ml-auto"
                                        style="height: 40px; float:right">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h3 class="card-title">Danh sách bài viết</h3>
                            <div class="float-right  ml-auto">
                                <a class="col-md-2 col-12" href="#" data-toggle="modal" data-target="#modalAddcategory">
                                    <button class="btn btn-primary btn-sm" style="">Thêm mới + </button>
                                </a>
                            </div>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="" class="table table-bordered table-hover ">

                                <thead class="thead-light">
                                    <tr>
                                        <th>STT</th>
                                        <th class="text-center">Ảnh</th>
                                        <th>Tên bài viết</th>
                                        <th>Loại bài viết</th>
                                        <th>Trạng thái</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($responseData['data'] as $index => $content)
                                        <tr>
                                            <th scope="row" style="vertical-align: middle ">{{ ++$index }}</th>
                                            <td class="products" style="vertical-align: middle ">
                                                @if (!empty($content['list_image']))
                                                    <img src="{{ $content['list_image'][0]['image'] }}" width="60"
                                                        class="object-fit-cover">

                                                @endif
                                            </td>
                                            <td style="vertical-align: middle ">{{ $content['title'] }}</td>
                                            <td style="vertical-align: middle ">
                                                {{ !empty($content['category']) ? $content['category']['name'] : null }}
                                            </td>
                                            <td style="vertical-align: middle ">
                                                {{ $content['status'] == 1 ? 'Hiển thị' : 'Ẩn' }}
                                            </td>

                                            <td style="vertical-align: middle ">
                                                <a href="" data-toggle="modal" data-target="#update{{ $content['id'] }}"
                                                    class="btn btn-info btn-circle btn-sm">
                                                    <i class="fas fa-edit"></i>
                                                </a>

                                                <a class="btn btn-danger btn-circle btn-sm" href="" data-toggle="modal"
                                                    data-target="#delete{{ $content['id'] }}">
                                                    <i class="fas fa-trash"></i>

                                                </a>
                                            </td>
                                            <div class="modal fade" id="update{{ $content['id'] }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Cập nhật bài
                                                                viết</h5>
                                                            <button class="close" type="button"
                                                                data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form
                                                                action="{{ route('admin.content.update', $content['id']) }}"
                                                                method="post" id="formAddcategory"
                                                                enctype="multipart/form-data">
                                                                @csrf
                                                                @method('put')
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="inputNameProduct">Tiêu đề bài
                                                                                    viết </label>
                                                                                <input type="text" class="form-control"
                                                                                    value="{{ $content['title'] }}"
                                                                                    required id="inputNameProduct"
                                                                                    name="title" placeholder="Tên bài viết">
                                                                            </div>
                                                                        </div>

                                                                    </div>


                                                                    <div class="row">
                                                                        <div class="col-12 col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="inputListImage">Danh sách
                                                                                    ảnh(Kích cỡ khuyên dùng >=
                                                                                    420*420)</label>
                                                                                <div class="image-input">
                                                                                    <input type="file" accept="image/*"
                                                                                        title="Kích cỡ khuyên dùng >= 420*420"
                                                                                        id="inputListImage" multiple
                                                                                        id="imageInput" name="image[]">
                                                                                    <div class="list-image">
                                                                                        @if (!empty($content['list_image']))
                                                                                            @foreach ($content['list_image'] as $image)
                                                                                                <img src="{{ $image['image'] }}"
                                                                                                    width="60"
                                                                                                    class="object-fit-cover">
                                                                                            @endforeach


                                                                                        @endif
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="col-12 col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="inputNameProduct">Loại bài
                                                                                    viết</label>
                                                                                <select name="category_id"
                                                                                    class="d-block  form-control" id="">
                                                                                    @foreach ($category_contents as $cate)
                                                                                        @if ($cate['children'])
                                                                                            @foreach ($cate['children'] as $item)
                                                                                                <option
                                                                                                    value="{{ $item['id'] }}"
                                                                                                    @if ($item['id'] == $content['category_id']) selected @endif>
                                                                                                    {{ $cate['name'] }}--
                                                                                                    {{ $item['name'] }}
                                                                                                </option>

                                                                                            @endforeach
                                                                                        @else
                                                                                            <option
                                                                                                value="{{ $cate['id'] }}"
                                                                                                @if ($cate['id'] == $content['category_id']) selected @endif>
                                                                                                {{ $cate['name'] }}
                                                                                            </option>

                                                                                        @endif
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="form-group">
                                                                                <label for="">Nội dung bài viết</label>
                                                                                <textarea name="content"
                                                                                    class="form-control summernote" cols="4"
                                                                                    required
                                                                                    rows="10"> {{ $content['content'] }}</textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="form-group">
                                                                                <label for="">Trạng thái</label>
                                                                                <select name="status" class="form-control"
                                                                                    id="">
                                                                                    <option value="1"
                                                                                        @if (1 == $content['status']) selected @endif>Hiển
                                                                                    </option>
                                                                                    <option value="0"
                                                                                        @if (0 == $content['status']) selected @endif>Ẩn</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- /.card-body -->

                                                                <div class="modal-footer justify-content-between">
                                                                    <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Đóng</button>
                                                                    <button type="submit" class="btn btn-primary">Lưu
                                                                        lại</button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- delete Modal-->
                                            <div class="modal fade" id="delete{{ $content['id'] }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Bạn có chắn
                                                                chắn
                                                                muốn
                                                                xoá?</h5>
                                                            <button class="close" type="button"
                                                                data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">bài viết: {{ $content['title'] }}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" type="button"
                                                                data-dismiss="modal">Huỷ</button>
                                                            <form
                                                                action="{{ route('admin.content.delete', $content['id']) }}"
                                                                method="post">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-danger" href="">Xoá</button>
                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </tr>

                                    @endforeach

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col" class="text-center">Ảnh</th>
                                        <th scope="col">Tên bài viết</th>
                                        <th scope="col">Loại bài viết</th>
                                        <th scope="col">Thao tác</th>
                                    </tr>

                                </tfoot>
                            </table>
                            <ul class="pagination justify-content-end">
                                @php
                                    $totalPage = $responseData['last_page'];
                                    $currentPage = $responseData['current_page'];

                                @endphp
                                <li class="page-item"><a class="page-link" href="#">Trước</a></li>
                                @for ($i = 1; $i <= $totalPage; $i++)
                                    <li class="page-item @if ($i == $currentPage) {{ 'active' }} @endif"><a class="page-link"
                                            href="{{ route('admin.content.page', $i) }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                <li class="page-item"><a class="page-link" href="#">Sau</a></li>
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <div class="modal fade" id="modalAddcategory">
        <div class="modal-dialog modal-lg modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Thêm mới bài viết</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin.content.store') }}" method="post" id="formAddcategory"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="inputNameProduct">Tiêu đề bài viết </label>
                                        <input type="text" class="form-control" id="inputNameProduct" name="title"
                                            required placeholder="Tên bài viết">
                                    </div>
                                </div>

                            </div>


                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="inputListImage">Danh sách ảnh(Kích cỡ khuyên dùng >= 420*420)</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" title="Kích cỡ khuyên dùng >= 420*420"
                                                id="inputListImage" multiple id="imageInput" name="image[]">
                                            <div class="list-image">

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="inputNameProduct">Loại bài viết</label>
                                        <select name="category_id" class="d-block  form-control" id="">
                                            @foreach ($category_contents as $cate)
                                                @if ($cate['children'])
                                                    @foreach ($cate['children'] as $item)
                                                        <option value="{{ $item['id'] }}" @if ($item['id'] == request('category_id')) selected @endif>
                                                            {{ $cate['name'] }}--
                                                            {{ $item['name'] }}</option>

                                                    @endforeach
                                                @else
                                                    <option value="{{ $cate['id'] }}" @if ($cate['id'] == request('category_id')) selected @endif>
                                                        {{ $cate['name'] }}</option>

                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="">Nội dung bài viết</label>
                                        <textarea name="content" class="form-control summernote" cols="4" rows="10"
                                            required>
                                                                                            </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="">Trạng thái</label>
                                        <select name="status" class="form-control" id="">
                                            <option value="1">Hiển
                                            </option>
                                            <option value="0">Ẩn</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('after-css')
    <link rel="stylesheet" href="{{ asset('template/AdminLTE/plugins/summernote/summernote-bs4.min.css') }}">
@endsection
@section('after-scripts')
    <script src="{{ asset('template/AdminLTE/plugins/summernote/summernote-bs4.min.js') }}"></script>

    <script>
        $(function() {
            $('#imageInput').on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('.image-preview').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                    $('.image-button').css('display', 'none');
                    $('.image-preview').css('display', 'block');
                    $('.change-image').css('display', 'block');
                }
            });
            $('.select2').select2();
            $('.change-image').on('click', function() {
                $control = $(this);
                $('#imageInput').val('');
                $preview = $('.image-preview');
                $preview.attr('src', '');
                $preview.css('display', 'none');
                $control.css('display', 'none');
                $('.image-button').css('display', 'block');
            });
            $('.formatPrice').priceFormat({
                prefix: '',
                centsLimit: 0,
                thousandsSeparator: '.'
            });
            // Summernote
            $('.summernote').summernote()
        })
    </script>

@endsection
