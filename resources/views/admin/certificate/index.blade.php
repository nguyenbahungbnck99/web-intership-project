@extends('admin.layouts.app')
@section('title-page', 'Danh sách chứng chỉ')
@section('after-css')
    <link rel="stylesheet" href="{{ asset('template/AdminLTE/plugins/summernote/summernote-bs4.min.css') }}">
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="GET" id="formSearchData">
                                <div class="row d-flex align-items-end">

                                    <div class="col-12 col-md-2">
                                        <label>Tên chứng chỉ</label>
                                        <div class="input-group">
                                            <input type="text" autocomplete="off" class="form-control" name="name"
                                                value="{{ request('name') }}" placeholder="Tên chứng chỉ" id="">
                                        </div>
                                        <!-- input-group -->
                                    </div>

                                    <button class="btn btn-primary float-right ml-auto"
                                        style="height: 40px; float:right">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h3 class="card-title">Danh sách chứng chỉ</h3>
                            <div class="float-right ml-auto">
                                <a class="col-md-2 col-12" href="#" data-toggle="modal" data-target="#modalAddCertificate">
                                    <button class="btn btn-primary btn-sm" style="">Thêm mới + </button>
                                </a>
                            </div>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="" class="table table-bordered table-hover ">

                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col" class="text-center">Ảnh</th>
                                        <th scope="col-2">Tên chứng chỉ</th>

                                        <th scope="col">Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($responseData['data'] as $index => $certificate)

                                        <tr data-widget="expandable-table" aria-expanded="false">
                                            <th scope="row" style="vertical-align: middle ">{{ ++$index }}</th>
                                            <td class="products" style="vertical-align: middle "><img
                                                    src="{{ $certificate['image'] }}" width="60" class="object-fit-cover">
                                            </td>
                                            <td style="vertical-align: middle ">{{ $certificate['name'] }}</td>




                                            <td style="vertical-align: middle ">
                                                <a href="{{ route('admin.certificate.addItem') }}"
                                                    certificate_id="{{ $certificate['id'] }}"
                                                    class="btn btn-info btn-circle btn-sm addItemCertificate">
                                                    <i class="fas fa-plus-circle"></i>
                                                </a>
                                                <a href="{{ route('admin.certificate.edit', $certificate['id']) }}"
                                                    class="btn btn-info btn-circle btn-sm editCertificate">
                                                    <i class="fas fa-edit"></i>
                                                </a>

                                                <a class="btn btn-danger btn-circle btn-sm" href="" data-toggle="modal"
                                                    data-target="#delete{{ $certificate['id'] }}">
                                                    <i class="fas fa-trash"></i>

                                                </a>
                                            </td>
                                            <!-- delete Modal-->
                                            <div class="modal fade" id="delete{{ $certificate['id'] }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Bạn có chắn chắn
                                                                muốn
                                                                xoá?</h5>
                                                            <button class="close" type="button" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">Chứng chỉ: {{ $certificate['name'] }}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" type="button"
                                                                data-dismiss="modal">Huỷ</button>
                                                            <form
                                                                action="{{ route('admin.certificate.delete', $certificate['id']) }}"
                                                                method="post">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-danger" href="">Xoá</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </tr>
                                        <tr class="expandable-body">
                                            <td colspan="5">
                                                @foreach ($certificate['certificate_item'] as $key => $item)
                                                    <div class="row">
                                                        <div class="col-1">{{ ++$key }}</div>
                                                        <div class="col-1">
                                                            <img src="{{ $item['image'] }}" width="60"
                                                                class="object-fit-cover">
                                                        </div>
                                                        <div class="col-7">{{ $item['name'] }}</div>
                                                        <div class="col-3">
                                                            <a href="{{ route('admin.certificate.editItem', $item['id']) }}"
                                                                class="btn btn-info btn-circle btn-sm editCertificateItem">
                                                                <i class="fas fa-edit"></i>
                                                            </a>

                                                            <a class="btn btn-danger btn-circle btn-sm" href=""
                                                                data-toggle="modal"
                                                                data-target="#delete{{ $item['id'] }}">
                                                                <i class="fas fa-trash"></i>

                                                            </a>
                                                        </div>
                                                        <div class="modal fade" id="delete{{ $item['id'] }}"
                                                            tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                            aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Bạn
                                                                            có chắn chắn
                                                                            muốn
                                                                            xoá?</h5>
                                                                        <button class="close" type="button"
                                                                            data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">×</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">Chứng chỉ:
                                                                        {{ $item['name'] }}
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button class="btn btn-secondary" type="button"
                                                                            data-dismiss="modal">Huỷ</button>
                                                                        <form
                                                                            action="{{ route('admin.certificate.deleteItem', $item['id']) }}"
                                                                            method="post">
                                                                            @csrf
                                                                            @method('delete')
                                                                            <button class="btn btn-danger"
                                                                                href="">Xoá</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col" class="text-center">Ảnh</th>
                                        <th scope="col">Tên chứng chỉ</th>
                                        <th scope="col">Thao tác</th>
                                    </tr>

                                </tfoot>
                            </table>
                            <ul class="pagination justify-content-end">
                                @php
                                    $totalPage = $responseData['last_page'];
                                    $currentPage = $responseData['current_page'];

                                @endphp
                                <li class="page-item"><a class="page-link" href="#">Trước</a></li>
                                @for ($i = 1; $i <= $totalPage; $i++)
                                    <li class="page-item @if ($i==$currentPage) {{ 'active' }} @endif"><a class="page-link"
                                            href="{{ route('admin.certificate.page', $i) }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                <li class="page-item"><a class="page-link" href="#">Sau</a></li>
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <div class="modal fade" id="modalAddCertificateItem">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Thêm mới loại chứng chỉ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin.certificate.addItem') }}" method="post" id="formAddCertificateItem"
                        enctype="multipart/form-data">
                        @csrf
                        @method('post')
                        <input type="hidden" name="certificate_id" id="certificate_id">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputName">Tên loại chứng chỉ</label>
                                        <input type="text" class="form-control" id="inputName" required name="name"
                                            placeholder="Tên chứng chỉ">
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputName">Tiêu đề loại chứng chỉ</label>
                                        <input type="text" class="form-control" id="inputName" required name="title"
                                            placeholder="Tiêu đề chứng chỉ">
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="imageInput">Item ảnh(Kích cỡ khuyên dùng ảnh tỉ lệ 1:1)</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="itemImageInput" name="item_image"
                                                required>
                                            <img src="" class="item-image-preview" style="max-width:100px">

                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="imageInput">>Ảnh(Kích cỡ khuyên dùng ảnh tỉ lệ 7:10)</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="imageInput" name="image" required>
                                            <img src="" class="image-preview" style="max-width:100px">

                                        </div>

                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="description">Mô tả</label>
                                        <textarea name="description" class="form-control " cols="" rows="3" required
                                            placeholder="Mô tả"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </form>
                </div>
                {{-- <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary">Lưu lại</button>
                </div> --}}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modalUpdateCertificateItem">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cập nhật chứng chỉ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="formUpdateCertificateItem" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <input type="hidden" name="certificate_id" id="certificate_id">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputName">Tên chứng chỉ</label>
                                        <input type="text" class="form-control" id="inputUpdateName" required name="name"
                                            placeholder="Tên chứng chỉ">
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputName">Tiêu đề chứng chỉ</label>
                                        <input type="text" class="form-control" id="inputName" required name="title"
                                            placeholder="Tiêu đề chứng chỉ">
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="imageInput">Item ảnh(Kích cỡ khuyên dùng ảnh tỉ lệ 1:1)</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="imageInputItemImage" name="item_image">
                                            <img src="" class="imageInputItemImage" style="max-width:100px">

                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="imageInput">>Ảnh(Kích cỡ khuyên dùng ảnh tỉ lệ 7:10)</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="imageInputImage" name="image">
                                            <img src="" class="imageInputImage" style="max-width:100px">

                                        </div>

                                    </div>
                                </div>

                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="description">Mô tả</label>
                                        <textarea name="description" class="form-control " cols="" rows="3" required
                                            placeholder="Mô tả"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </form>
                </div>
                {{-- <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary">Lưu lại</button>
                </div> --}}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modalAddCertificate">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Thêm mới chứng chỉ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin.certificate.store') }}" method="post" id="formAddCertificate"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputName">Tên chứng chỉ</label>
                                        <input type="text" class="form-control" id="inputName" required name="name"
                                            placeholder="Tên chứng chỉ">
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputSlogan">Tiêu đề chứng chỉ trên</label>
                                        <input type="text" class="form-control" id="inputSlogan" required name="slogan"
                                            placeholder="Tiêu đề chứng chỉ 1">
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="description">Mô tả trên</label>
                                        <textarea name="description" class="form-control " cols="" rows="3" required
                                            placeholder="Mô tả 1"></textarea>
                                    </div>
                                </div>

                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputSlogan2">Tiêu đề chứng chỉ dưới</label>
                                        <input type="text" class="form-control" id="inputSlogan2" required name="slogan_2"
                                            placeholder="Tiêu đề chứng chỉ 2">
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="description">Mô tả dưới</label>
                                        <textarea name="description_2" class="form-control " cols="" rows="3" required
                                            placeholder="Mô tả 2"></textarea>
                                    </div>
                                </div>

                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputAddImage">Ảnh(Kích cỡ khuyên dùng 445*250 )</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="inputAddImage" name="image" required>
                                            <img src="" class="image-preview-add" style="max-width:100px">

                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </form>
                </div>
                {{-- <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary">Lưu lại</button>
                </div> --}}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modalUpdateCertificate">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cập nhật chứng chỉ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="formUpdateCertificate" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputName">Tên chứng chỉ</label>
                                        <input type="text" class="form-control" id="inputName" required name="name" required
                                            placeholder="Tên chứng chỉ">
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputSlogan">Tiêu đề chứng chỉ trên</label>
                                        <input type="text" class="form-control" id="inputSlogan" required name="slogan" required
                                            placeholder="Tiêu đề chứng chỉ 1">
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="description">Mô tả trên</label>
                                        <textarea name="description" class="form-control " cols="" rows="3" required
                                            placeholder="Mô tả 1"></textarea>
                                    </div>
                                </div>

                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputSlogan2">Tiêu đề chứng chỉ dưới</label>
                                        <input type="text" class="form-control" id="inputSlogan2" required name="slogan_2" required
                                            placeholder="Tiêu đề chứng chỉ 2">
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="description">Mô tả dưới</label>
                                        <textarea name="description_2" class="form-control " cols="" rows="3" required
                                            placeholder="Mô tả 2"></textarea>
                                    </div>
                                    <div class="col-12 ">
                                        <div class="form-group">
                                            <label for="imageInput">Ảnh(Kích cỡ khuyên dùng 445*250 )</label>
                                            <div class="image-input">
                                                <input type="file" accept="image/*" id="imageInputUpdate" name="image" required>
                                                <img src="" id="imageCertificateUpdate" class="image-preview"
                                                    style="max-width:100px">

                                            </div>

                                        </div>
                                    </div>


                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                <button type="submit" class="btn btn-primary">Lưu lại</button>
                            </div>
                    </form>
                </div>
                {{-- <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary">Lưu lại</button>
                </div> --}}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('after-scripts')
    <script src="{{ asset('template/AdminLTE/plugins/summernote/summernote-bs4.min.js') }}"></script>

    <script>
        $(function() {
            $('#inputAddImage').on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('.image-preview-add').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                }
            });
            $('#imageInput').on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('.image-preview').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                }
            });

            $('#imageInputUpdate').on('change', function() {
                console.log('edit');
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('#imageCertificateUpdate').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                }
            });

            $('#itemImageInput').on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('.item-image-preview').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                }
            });
            $('#imageInputItemImage').on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('.imageInputItemImage').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                }
            });
            $('#imageInputImage').on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('.imageInputImage').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                }
            });
            $('.select2').select2();

            $('.formatPrice').priceFormat({
                prefix: '',
                centsLimit: 0,
                thousandsSeparator: '.'
            });
            // Summernote
            $('.summernote').summernote()
        })
        $('.addItemCertificate').click(function(e) {
            e.preventDefault();
            let method = $(this).attr('href');
            let certificate_id = $(this).attr('certificate_id');
            $('#certificate_id').val(certificate_id);
            $('#modalAddCertificateItem').modal('show');
        });
        $('.editCertificate').click(function(e) {
            e.preventDefault();

            let href = $(this).attr('href');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: href,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(res) {
                    let certificate = res.data;
                    $('#formUpdateCertificate input[name=name]').val(certificate.name);
                    $('#formUpdateCertificate input[name=slogan]').val(certificate.slogan);
                    $('#formUpdateCertificate input[name=slogan_2]').val(certificate.slogan_2);
                    $('#formUpdateCertificate textarea[name=description]').text(certificate.description);
                    $('#formUpdateCertificate textarea[name=description_2]').text(certificate.description_2);
                    $('#formUpdateCertificate #imageCertificateUpdate').attr('src', certificate.image);
                    $('#formUpdateCertificate').attr('action', res.action);
                    $('#modalUpdateCertificate').modal('show');
                    console.log(res);
                },
                dataType: false
            });
        });
        $('.editCertificateItem').click(function(e) {
            e.preventDefault();

            let href = $(this).attr('href');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: href,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(res) {
                    let certificate = res.data;
                    console.log(certificate);
                    $('#formUpdateCertificateItem input[name=certificate_id]').val(certificate
                        .certificate_id);
                    $('#formUpdateCertificateItem input[name=name]').val(certificate.name);
                    $('#formUpdateCertificateItem input[name=title]').val(certificate.title);
                    $('#formUpdateCertificateItem .imageInputItemImage').attr('src', certificate
                        .item_image);
                    $('#formUpdateCertificateItem .imageInputImage').attr('src', certificate.image);
                    $('#formUpdateCertificateItem textarea[name=description]').text(certificate
                        .description);
                    $('#formUpdateCertificateItem').attr('action', res.action);
                    $('#modalUpdateCertificateItem').modal('show');
                    console.log(res);
                },
                dataType: false
            });
        });
    </script>

@endsection
