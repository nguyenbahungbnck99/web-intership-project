@extends('admin.layouts.app')
@section('title-page', 'Danh sách slider')
@section('content')
    <section class="content">
        <div class="container-fluid">
            {{-- <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="GET" id="formSearchData">
                                <div class="row d-flex align-items-end">
                                    <div class="col-12 col-md-2">
                                        <label>Tiêu đề</label>
                                        <div class="input-group">
                                            <input type="text" autocomplete="off" class="form-control" name="title"
                                                value="{{ request('title') }}" placeholder="Tiêu đề" id="startdatepicker">
                                        </div>
                                        <!-- input-group -->
                                    </div>


                                    <button class="btn btn-primary float-right ml-auto"
                                        style="height: 40px; float:right">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> --}}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h3 class="card-title">Danh sách slider</h3>
                            <div class="float-right ml-auto">
                                <a class="col-md-2 col-12" href="#" data-toggle="modal" data-target="#modalAddSlider">
                                    <button class="btn btn-primary btn-sm" style="">Thêm mới + </button>
                                </a>
                            </div>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="table" class="table table-bordered table-hover ">

                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col" class="text-center">Ảnh</th>
                                        <th scope="col-2">Tiêu đề</th>
                                        <th scope="col">Trạng thái</th>
                                        <th scope="col">Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($responseData['data'] as $index => $slider)
                                        <tr>
                                            <th scope="row" style="vertical-align: middle ">{{ ++$index }}</th>
                                            <td class="products" style="vertical-align: middle "><img
                                                    src="{{ $slider['image'] }}" width="60" class="object-fit-cover">
                                            </td>
                                            <td style="vertical-align: middle ">{{ $slider['title'] }}</td>
                                            <td style="vertical-align: middle ">
                                                {{ $slider['is_active'] == 1 ? 'Hiện' : 'Ẩn' }}
                                            </td>




                                            <td style="vertical-align: middle ">
                                                <a href="{{ route('admin.slider.edit', $slider['id']) }}"
                                                    class="btn btn-info btn-circle btn-sm editSlider">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                @if ($slider['is_active'] == 0)
                                                    <a class="btn btn-success btn-circle btn-sm" href="" data-toggle="modal"
                                                        data-target="#active{{ $slider['id'] }}">
                                                        <i class="fas fa-eye"></i>

                                                    </a>
                                                @else
                                                    <a class="btn btn-warning btn-circle btn-sm" href="" data-toggle="modal"
                                                        data-target="#hiddenActive{{ $slider['id'] }}">
                                                        <i class="fas fa-eye-slash"></i>

                                                    </a>
                                                @endif


                                                <a class="btn btn-danger btn-circle btn-sm" href="" data-toggle="modal"
                                                    data-target="#delete{{ $slider['id'] }}">
                                                    <i class="fas fa-trash"></i>

                                                </a>
                                            </td>
                                            <!-- delete Modal-->
                                            <div class="modal fade" id="delete{{ $slider['id'] }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Bạn có chắn chắn
                                                                muốn
                                                                xoá?</h5>
                                                            <button class="close" type="button" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">Slider: {{ $slider['title'] }}</div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" type="button"
                                                                data-dismiss="modal">Huỷ</button>
                                                            <form
                                                                action="{{ route('admin.slider.delete', $slider['id']) }}"
                                                                method="post">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-danger" href="">Xoá</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal fade" id="active{{ $slider['id'] }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Bạn có chắn chắn
                                                                thay đổi trạng thái slider?</h5>
                                                            <button class="close" type="button" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">Slider: {{ $slider['title'] }}</div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" type="button"
                                                                data-dismiss="modal">Huỷ</button>
                                                            <form
                                                                action="{{ route('admin.slider.active', $slider['id']) }}"
                                                                method="post">
                                                                <input type="hidden" name="is_active" value="1">
                                                                @csrf
                                                                @method('put')
                                                                <button class="btn btn-success" href="">Đồng ý</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal fade" id="hiddenActive{{ $slider['id'] }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Bạn có chắn chắn
                                                                thay đổi trạng thái slider?</h5>
                                                            <button class="close" type="button" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">Slider: {{ $slider['title'] }}</div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" type="button"
                                                                data-dismiss="modal">Huỷ</button>
                                                            <form
                                                                action="{{ route('admin.slider.active', $slider['id']) }}"
                                                                method="post">
                                                                <input type="hidden" name="is_active" value="0">
                                                                @csrf
                                                                @method('put')
                                                                <button class="btn btn-warning" href="">Đồng ý</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </tr>

                                    @endforeach

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col" class="text-center">Ảnh</th>
                                        <th scope="col">Tiêu đề</th>
                                        <th scope="col">Trạng thái</th>
                                        <th scope="col">Thao tác</th>
                                    </tr>

                                </tfoot>
                            </table>
                            <ul class="pagination justify-content-end">
                                @php
                                    $totalPage = $responseData['last_page'];
                                    $currentPage = $responseData['current_page'];

                                @endphp
                                <li class="page-item"><a class="page-link" href="#">Trước</a></li>
                                @for ($i = 1; $i <= $totalPage; $i++)
                                    <li class="page-item @if ($i==$currentPage) {{ 'active' }} @endif"><a class="page-link"
                                            href="#">{{ $i }}</a>
                                    </li>
                                @endfor
                                <li class="page-item"><a class="page-link" href="#">Sau</a></li>
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <div class="modal fade" id="modalAddSlider">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Thêm mới slider</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin.slider.store') }}" method="post" id="formAddslider"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputTitle">Tiêu đề slider</label>
                                        <input type="text" class="form-control" id="inputTitle" name="title"
                                            placeholder="Tiêu đề slider">
                                    </div>
                                </div>


                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="imageInput">Ảnh</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="imageInput" name="image" required>
                                            <img src="" class="image-preview" style="max-width:100px">

                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputLinkUpdate">Link slider</label>
                                        <input type="text" class="form-control" id="inputLinkUpdate" required name="link"
                                            placeholder="Link slider">
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputContent">Content</label>
                                        <input type="text" class="form-control" id="inputContent" name="content"
                                            placeholder="Link slider">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modalUpdateSlider">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cập nhật slider</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="formUpdateSlider" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputTitle">Tiêu đề slider</label>
                                        <input type="text" class="form-control" id="inputTitle" name="title"
                                            placeholder="Tiêu đề slider">
                                    </div>
                                </div>


                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="imageUpdate">Ảnh</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="imageUpdate" name="image">
                                            <img src="" class="image-preview-update" style="max-width:100px">

                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputLinkUpdate">Link slider</label>
                                        <input type="text" class="form-control" id="inputLinkUpdate" required name="link"
                                            placeholder="Link slider">
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputContent">Content</label>
                                        <input type="text" class="form-control" id="inputContent" name="content"
                                            placeholder="Link slider">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('after-scripts')
    <script>
        $('.editSlider').click(function(e) {
            e.preventDefault();

            let href = $(this).attr('href');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: href,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(res) {
                    let slider = res.data;

                    $('#formUpdateSlider input[name=link]').val(slider.link);
                    $('#formUpdateSlider input[name=content]').val(slider.content);
                    $('#formUpdateSlider input[name=title]').val(slider.title);
                    $('#formUpdateSlider .image-preview-update').attr('src', slider.image);
                    $('#formUpdateSlider').attr('action', res.action);
                    $('#modalUpdateSlider').modal('show');
                    console.log(res);
                },
                dataType: false
            });
        });
        $('#imageInput').on('change', function() {
            $input = $(this);
            if ($input.val().length > 0) {
                fileReader = new FileReader();
                fileReader.onload = function(data) {
                    $('.image-preview').attr('src', data.target.result);
                }
                fileReader.readAsDataURL($input.prop('files')[0]);
            }
        });
        $(document).ready(function() {
            $('#imageUpdate').on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('.image-preview-update').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                }
            });
        });
    </script>
@endsection
