<!-- Custom styles for this template-->
{{-- <link rel="stylesheet" href="{{ asset('css/admin.css') }}"> --}}
{{-- <link href="{{ asset('css/admin.min.css') }}" rel="stylesheet"> --}}
<link href="{{ asset('js/sweet-alert.js') }}" rel="stylesheet">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="{{ asset('img/chapp_3.png') }}" type="image/x-icon">
<title>Admin</title>

<!-- Custom fonts for this template-->
<link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

<!-- Custom styles for this template-->


<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link href="{{ asset('fontawesome/css/all.min.css') }}" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="{{ asset('template/AdminLTE/plugins/fontawesome-free/css/all.min.css') }}">
{{-- select2 --}}
<link rel="stylesheet" href="{{ asset('template/AdminLTE/plugins/select2/css/select2.min.css') }}">

<!-- overlayScrollbars -->
<link rel="stylesheet"
    href="{{ asset('template/AdminLTE/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('template/AdminLTE/dist/css/adminlte.min.css') }}">

<link rel="stylesheet" href="{{ asset('css/admin/style.css') }}">

