<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title-page')</title>

    @include('admin.layouts.header-style')
    @yield('after-css')

</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- main-header -->
        @include('admin.layouts.main-header')
        <!-- main-sidebar -->

        @include('admin.layouts.main-sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            @include('admin.layouts.content-header')
            <!-- content -->
            <div class="content">
                @include('admin.layouts._alert')

            </div>
            @yield('content')

        </div>
        <!-- /.content-wrapper -->



        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
    @include('admin.layouts.scripts')
    @yield('after-scripts')

</body>

</html>
