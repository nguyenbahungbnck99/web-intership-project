@extends('admin.layouts.app')
@section('title-page', 'Danh sách category')\
@section('after-css')
    <link rel="stylesheet" href="{{ asset('template/AdminLTE/plugins/summernote/summernote-bs4.min.css') }}">
    <style>
        .select2-container {
            width: 100% !important
        }

    </style>
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h3 class="card-title">Danh sách bài viết</h3>
                            <div class="float-right ml-auto">
                                <a class="col-md-2 col-12" href="#" data-toggle="modal" data-target="#modalAddcategory">
                                    <button class="btn btn-primary btn-sm" style="">Thêm mới + </button>
                                </a>
                            </div>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="table" class="table table-bordered table-hover ">

                                <thead class="thead-light">
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên danh mục bài viết</th>

                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($responseData['data'] as $index => $category)
                                        <tr data-widget="expandable-table" aria-expanded="false">
                                            <th scope="row" style="vertical-align: middle ">{{ ++$index }}</th>
                                            <td style="vertical-align: middle ">{{ $category['name'] }}</td>
                                            <td style="vertical-align: middle ">


                                                <a href="" href="" data-toggle="modal"
                                                    data-target="#update{{ $category['id'] }}"
                                                    class="btn btn-info btn-circle btn-sm editcategory">
                                                    <i class="fas fa-edit"></i>
                                                </a>

                                                <a class="btn btn-danger btn-circle btn-sm" href="" data-toggle="modal"
                                                    data-target="#delete{{ $category['id'] }}">
                                                    <i class="fas fa-trash"></i>

                                                </a>
                                            </td>
                                            <div class="modal fade" id="update{{ $category['id'] }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Cập nhật danh
                                                                mục bài viết</h5>
                                                            <button class="close" type="button"
                                                                data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form
                                                                action="{{ route('admin.categoryContent.update', $category['id']) }}"
                                                                method="post" id="formAddcategory"
                                                                enctype="multipart/form-data">
                                                                @csrf
                                                                @method('put')
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-12 ">
                                                                            <div class="form-group">
                                                                                <label for="inputTitle">Tên danh mục sản
                                                                                    phẩm</label>
                                                                                <input type="text" class="form-control"
                                                                                    value="{{ $category['name'] }}"
                                                                                    id="inputTitle" required name="name"
                                                                                    placeholder="Tên danh mục bài viết">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-12 ">
                                                                            <div class="form-group">
                                                                                <label for="inputTitle">Danh mục cha</label>
                                                                                <div class="input-group">

                                                                                    <select name="parent"
                                                                                        class="select2 d-block form-control"
                                                                                        id="">
                                                                                        <option value="0">--Danh mục cha--
                                                                                        </option>
                                                                                        @foreach ($responseData['data'] as $cate)
                                                                                            <option @if ($cate['id'] == $category['parent']) selected @endif
                                                                                                value="{{ $cate['id'] }}">
                                                                                                {{ $cate['name'] }}
                                                                                            </option>
                                                                                            @if ($cate['children'])
                                                                                                @foreach ($cate['children'] as $item)
                                                                                                    <option
                                                                                                        @if ($item['id'] == $category['parent']) selected @endif
                                                                                                        value="{{ $item['id'] }}">
                                                                                                        {{ $cate['name'] }}--
                                                                                                        {{ $item['name'] }}
                                                                                                    </option>

                                                                                                @endforeach

                                                                                            @endif
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- /.card-body -->

                                                                <div class="modal-footer justify-content-between">
                                                                    <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Đóng</button>
                                                                    <button type="submit" class="btn btn-primary">Lưu
                                                                        lại</button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- delete Modal-->
                                            <div class="modal fade" id="delete{{ $category['id'] }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Bạn có chắn
                                                                chắn
                                                                muốn
                                                                xoá?</h5>
                                                            <button class="close" type="button"
                                                                data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">category: {{ $category['name'] }}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" type="button"
                                                                data-dismiss="modal">Huỷ</button>
                                                            <form
                                                                action="{{ route('admin.categoryContent.delete', $category['id']) }}"
                                                                method="post">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-danger" href="">Xoá</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </tr>

                                    @endforeach

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col">Tiêu đề</th>

                                        <th scope="col">Thao tác</th>
                                    </tr>

                                </tfoot>
                            </table>
                            <ul class="pagination justify-content-end">
                                @php
                                    $totalPage = $responseData['last_page'];
                                    $currentPage = $responseData['current_page'];

                                @endphp
                                <li class="page-item"><a class="page-link" href="#">Trước</a></li>
                                @for ($i = 1; $i <= $totalPage; $i++)
                                    <li class="page-item @if ($i == $currentPage) {{ 'active' }} @endif"><a class="page-link"
                                            href="{{ asset('/admin/san-pham/danh-sach/' . $i) }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                <li class="page-item"><a class="page-link" href="#">Sau</a></li>
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <div class="modal fade" id="modalAddcategory">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Thêm mới category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin.categoryContent.store') }}" method="post" id="formAddcategory"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputTitle">Tên danh mục bài viết</label>
                                        <input type="text" class="form-control" id="inputTitle" required name="name"
                                            placeholder="Tên danh mục bài viết">
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputTitle">Danh mục cha</label>
                                        <div class="input-group">

                                            <select name="parent" class="select2 d-block form-control" id="">
                                                <option value="0">--Danh mục cha--</option>
                                                @foreach ($responseData['data'] as $cate)
                                                    <option value="{{ $cate['id'] }}">{{ $cate['name'] }}</option>
                                                    @if ($cate['children'])
                                                        @foreach ($cate['children'] as $item)
                                                            <option value="{{ $item['id'] }}">{{ $cate['name'] }}--
                                                                {{ $item['name'] }}</option>

                                                        @endforeach

                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modalUpdatecategory">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cập nhật category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="formUpdatecategory" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputTitle">Tên danh mục bài viết</label>
                                        <input type="text" class="form-control" id="inputTitle" required name="name"
                                            placeholder="ên danh mục bài viết">
                                    </div>
                                </div>

                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputTitle" class="col-12">Danh mục cha</label>
                                        <div class="input-group">
                                            <select name="parent" class="form-control select2 d-block" id="parent">
                                                <option value="0">--Danh mục cha--</option>
                                                @foreach ($responseData['data'] as $cate)
                                                    <option value="{{ $cate['id'] }}">{{ $cate['name'] }}</option>
                                                    @if ($cate['children'])
                                                        @foreach ($cate['children'] as $item)
                                                            <option value="{{ $item['id'] }}">{{ $cate['name'] }}--
                                                                {{ $item['name'] }}</option>

                                                        @endforeach

                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modalAddProduct">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Thêm mới bài viết</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" enctype="multipart/form-data" id="formAddProductByCate">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="inputNameProduct">Tên bài viết</label>
                                        <input type="text" class="form-control" id="inputNameProduct" name="name"
                                            value="{{ request('name') }}" placeholder="Tên bài viết">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="inputNameProduct">Loại bài viết</label>
                                        <input type="text" class="form-control" id="name_cate" readonly
                                            placeholder="Loại bài viết">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="inputSell">Giá bán</label>
                                        <input type="text" class="form-control formatPrice" id="inputSell" name="price"
                                            placeholder="Giá bán">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="inputSale">Giá khuyến mãi</label>
                                        <input type="text" class="form-control formatPrice" id="inputSale"
                                            name="price_sale" placeholder="Giá khuyến mãi">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputFile">Ảnh</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="imageInput" name="image">
                                            <img src="" class="image-preview" style="max-width:100px">

                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="inputListImage">Danh sách ảnh</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="inputListImage" multiple
                                                id="imageInputList" name="list_images[]">
                                            <div class="list-image">

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="inputInventory">Tồn kho</label>
                                        <input type="text" class="form-control formatPrice" id="inputInventory"
                                            name="total_inventory" placeholder="Tồn kho">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="inputUserObject">Đối tượng sử dụng</label>
                                        <input type="text" class="form-control " id="inputUserObject" name="user_object"
                                            placeholder="Đối tượng sử dụng">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="inputUserManual">Hướng dẫn sử dụng</label>
                                        <textarea name="user_manual" class="form-control summernote" cols="4" rows="10">
                                                                                                                            </textarea>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label for="inputUsefulness">Công dụng</label>
                                        <textarea name="usefulness" class="form-control summernote" cols="4" rows="10">
                                                                                                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="">Mô tả bài viết</label>
                                        <textarea name="description" class="form-control summernote" cols="4" rows="10">
                                                                                                                            </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="">Chi tiết bài viết</label>
                                        <textarea name="content" class="form-control summernote" cols="4" rows="10">
                                                                                                                            </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Lưu</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('after-scripts')
    <script src="{{ asset('template/AdminLTE/plugins/summernote/summernote-bs4.min.js') }}"></script>

    <script>
        $('.addProductByCate').click(function(e) {
            e.preventDefault();
            let action = $(this).attr('href');
            let name_cate = $(this).attr('cate_name');
            $('#formAddProductByCate').attr('action', action);
            $('#name_cate').val(name_cate);
            $('#modalAddProduct').modal('show');
        })
        $('.editcategory').click(function(e) {
            e.preventDefault();

            let href = $(this).attr('href');
            console.log(href);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: href,
                data: {},
                success: function(res) {
                    let category = res.data;
                    console.log(res);
                    $('#formUpdatecategory input[name=name]').val(category.name);
                    $('#formUpdatecategory').attr('action', res.action);
                    $("#parent option").each(function() {
                        if (category.parent != 0) {
                            if ($(this).val() == category.parent) $(this).attr('selected',
                                true);
                        }
                    });
                    $('#modalUpdatecategory').modal('show');
                },
                dataType: false
            });
        });
        $(function() {
            $('#imageInput').on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('.image-preview').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                    $('.image-button').css('display', 'none');
                    $('.image-preview').css('display', 'block');
                    $('.change-image').css('display', 'block');
                }
            });
            $('.select2').select2();
            $('.change-image').on('click', function() {
                $control = $(this);
                $('#imageInput').val('');
                $preview = $('.image-preview');
                $preview.attr('src', '');
                $preview.css('display', 'none');
                $control.css('display', 'none');
                $('.image-button').css('display', 'block');
            });
            $('.formatPrice').priceFormat({
                prefix: '',
                centsLimit: 0,
                thousandsSeparator: '.'
            });
            // Summernote
            $('.summernote').summernote()
        })
    </script>

@endsection
