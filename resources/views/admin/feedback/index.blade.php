@extends('admin.layouts.app')
@section('title-page', 'Danh sách feedback')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="GET" id="formSearchData">
                                <div class="row d-flex align-items-end">
                                    <div class="col-12 col-md-2">
                                        <label>Tiêu đề</label>
                                        <div class="input-group">
                                            <input type="text" autocomplete="off" class="form-control" name="title"
                                                value="{{ request('title') }}" placeholder="Tiêu đề" id="startdatepicker">
                                        </div>
                                        <!-- input-group -->
                                    </div>


                                    <button class="btn btn-primary float-right ml-auto"
                                        style="height: 40px; float:right">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h3 class="card-title">Danh sách Feedback</h3>
                            <div class="float-right ml-auto">
                                <a class="col-md-2 col-12" href="#" data-toggle="modal" data-target="#modalAddFeedback">
                                    <button class="btn btn-primary btn-sm" style="">Thêm mới + </button>
                                </a>
                            </div>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="table" class="table table-bordered table-hover ">

                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col" class="text-center">Ảnh</th>
                                        <th scope="col-2">Tiêu đề</th>

                                        <th scope="col">Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($responseData['data'] as $index => $feedback)
                                        <tr>
                                            <th scope="row" style="vertical-align: middle ">{{ ++$index }}</th>
                                            <td class="products" style="vertical-align: middle ">
                                                <img src="{{ $feedback['image'] }}" width="60" class="object-fit-cover">
                                            </td>
                                            <td style="vertical-align: middle ">{{ $feedback['title'] }}</td>



                                            {{-- <td style="vertical-align: middle ">
                                            <a href="{{ asset('admin/san-pham/cam-nhan/' . $feedback['id']) }}"
                                                class="btn btn-info btn-circle btn-sm">
                                                <i class="fas fa-info-circle"></i>
                                            </a>
                                        </td> --}}
                                            <td style="vertical-align: middle ">
                                                <a href="{{ route('admin.feedback.edit', $feedback['id']) }}"
                                                    class="btn btn-info btn-circle btn-sm editFeedback">
                                                    <i class="fas fa-edit"></i>
                                                </a>

                                                <a class="btn btn-danger btn-circle btn-sm" href="" data-toggle="modal"
                                                    data-target="#delete{{ $feedback['id'] }}">
                                                    <i class="fas fa-trash"></i>

                                                </a>
                                            </td>
                                            <!-- delete Modal-->
                                            <div class="modal fade" id="delete{{ $feedback['id'] }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Bạn có chắn chắn
                                                                muốn
                                                                xoá?</h5>
                                                            <button class="close" type="button" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">Feedback: {{ $feedback['title'] }}</div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" type="button"
                                                                data-dismiss="modal">Huỷ</button>
                                                            <form
                                                                action="{{ route('admin.feedback.delete', $feedback['id']) }}"
                                                                method="post">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-danger" href="">Xoá</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </tr>

                                    @endforeach

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col" class="text-center">Ảnh</th>
                                        <th scope="col">Tiêu đề</th>

                                        <th scope="col">Thao tác</th>
                                    </tr>

                                </tfoot>
                            </table>
                            <ul class="pagination justify-content-end">
                                @php
                                    $totalPage = $responseData['last_page'];
                                    $currentPage = $responseData['current_page'];

                                @endphp
                                <li class="page-item"><a class="page-link" href="#">Trước</a></li>
                                @for ($i = 1; $i <= $totalPage; $i++)
                                    <li class="page-item @if ($i==$currentPage) {{ 'active' }} @endif"><a class="page-link"
                                            href="{{ asset('/admin/san-pham/danh-sach/' . $i) }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                <li class="page-item"><a class="page-link" href="#">Sau</a></li>
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <div class="modal fade" id="modalAddFeedback">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Thêm mới feedback</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin.feedback.store') }}" method="post" id="formAddFeedback"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputTitle">Tiêu đề feedback</label>
                                        <input type="text" class="form-control" id="inputTitle" required name="title"
                                            placeholder="Tiêu đề feedback">
                                    </div>
                                </div>


                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="imageInput">>Ảnh(Kích cỡ khuyên dùng ảnh tỉ lệ 1:2)</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="imageInput" name="image" required>
                                            <img src="" class="image-preview" style="max-width:100px">

                                        </div>

                                    </div>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="is_hot" id="CheckHotAdd">
                                    <label class="form-check-label" for="CheckHotAdd">Có là feedback nổi bật không</label>
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modalUpdateFeedback">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cập nhật feedback</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="formUpdateFeedback" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputTitle">Tiêu đề feedback</label>
                                        <input type="text" class="form-control" id="inputTitle" required name="title"
                                            placeholder="Tiêu đề feedback">
                                    </div>
                                </div>


                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="imageUpdate">Ảnh(Kích cỡ khuyên dùng ảnh tỉ lệ 1:2)</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="imageUpdate" name="image">
                                            <img src="" class="image-preview-update" style="max-width:100px">

                                        </div>

                                    </div>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="is_hot" id="CheckHotUpdate">
                                    <label class="form-check-label" for="CheckHotAdd">Có là feedback nổi bật không</label>
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('after-scripts')
    <script>
        $('.editFeedback').click(function(e) {
            e.preventDefault();

            let href = $(this).attr('href');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: href,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(res) {
                    let feedback = res.data;
                    if (feedback.is_hot == 1) {
                        $('#formUpdateFeedback input[name=is_hot]').val(feedback.is_hot);
                        $('#CheckHotUpdate').attr('checked', true);
                    }
                    $('#formUpdateFeedback input[name=title]').val(feedback.title);
                    $('#formUpdateFeedback .image-preview-update').attr('src', feedback.image);
                    $('#formUpdateFeedback').attr('action', res.action);
                    $('#modalUpdateFeedback').modal('show');
                    console.log(res);
                },
                dataType: false
            });
        });
        $('#imageInput').on('change', function() {
            $input = $(this);
            if ($input.val().length > 0) {
                fileReader = new FileReader();
                fileReader.onload = function(data) {
                    $('.image-preview').attr('src', data.target.result);
                }
                fileReader.readAsDataURL($input.prop('files')[0]);
            }
        });
        $(document).ready(function() {
            $('#imageUpdate').on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('.image-preview-update').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                }
            });
        });
    </script>
@endsection
