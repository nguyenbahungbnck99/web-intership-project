@extends('admin.layouts.app')
@section('title-page', 'Danh sách sản phẩm')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="GET" id="formSearchData">
                                <div class="row d-flex align-items-end">

                                    <div class="col-12 col-md-2">
                                        <label>Tên sản phẩm</label>
                                        <div class="input-group">
                                            <input type="text" autocomplete="off" class="form-control" name="name"
                                                value="{{ request('name') }}" placeholder="Tên sản phẩm" id="">
                                        </div>
                                        <!-- input-group -->
                                    </div>

                                    <div class="col-12 col-md-4">
                                        <label>Loại sản phẩm</label>
                                        <div class="input-group">
                                            <select name="category_id" class="select2 d-block form-control">
                                                <option value="">--Loại sản phẩm--</option>
                                                @foreach ($category_products as $cate)
                                                    @if ($cate['children'])
                                                        @foreach ($cate['children'] as $item)
                                                            <option value="{{ $item['id'] }}" @if($item['id'] == request('category_id')) selected @endif>{{ $cate['name'] }}--
                                                                {{ $item['name'] }}</option>

                                                        @endforeach
                                                    @else
                                                        <option value="{{ $cate['id'] }}" @if($cate['id'] == request('category_id')) selected @endif>{{ $cate['name'] }}</option>

                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- input-group -->
                                    </div>
                                    <button class="btn btn-primary float-right ml-auto"
                                        style="height: 40px; float:right">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h3 class="card-title">Danh sách nhập kho</h3>
                            <div class="float-right  ml-auto">
                                <a class="col-md-2 col-12" href="{{ route('admin.product.create') }}">
                                    <button class="btn btn-primary btn-sm" style="">Thêm mới + </button>
                                </a>
                            </div>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="" class="table table-bordered table-hover ">

                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col" class="text-center">Ảnh</th>
                                        <th scope="col-2">Tên sản phẩm</th>
                                        <th scope="col-2">Loại sản phẩm</th>
                                        <th scope="col-2">Giá sản phẩm</th>
                                        <th scope="col-2">Giá khuyến mãi</th>
                                        <th scope="col">Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($responseData['data'] as $index => $product)
                                        <tr>
                                            <th scope="row" style="vertical-align: middle ">{{ ++$index }}</th>
                                            <td class="products" style="vertical-align: middle "><img
                                                    src="{{ $product['image'] }}" width="60" class="object-fit-cover">
                                            </td>
                                            <td style="vertical-align: middle ">{{ $product['name'] }}</td>
                                            <td style="vertical-align: middle ">
                                                {{ !empty($product['category_product']) ? $product['category_product']['name'] : null }}
                                            </td>

                                            <td style="vertical-align: middle ">

                                                {{ number_format($product['price']) }} đ
                                            </td>
                                            <td style="vertical-align: middle ">
                                                @if ($product['price_sale'] != null)
                                                    {{ number_format($product['price_sale']) }} đ

                                                @endif
                                            </td>


                                            {{-- <td style="vertical-align: middle ">
                                            <a href="{{ asset('admin/san-pham/cam-nhan/' . $product['id']) }}"
                                                class="btn btn-info btn-circle btn-sm">
                                                <i class="fas fa-info-circle"></i>
                                            </a>
                                        </td> --}}
                                            <td style="vertical-align: middle ">
                                                <a href="{{ route('admin.product.edit', $product['id']) }}"
                                                    class="btn btn-info btn-circle btn-sm">
                                                    <i class="fas fa-edit"></i>
                                                </a>

                                                <a class="btn btn-danger btn-circle btn-sm" href="" data-toggle="modal"
                                                    data-target="#delete{{ $product['id'] }}">
                                                    <i class="fas fa-trash"></i>

                                                </a>
                                            </td>
                                            <!-- delete Modal-->
                                            <div class="modal fade" id="delete{{ $product['id'] }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Bạn có chắn chắn
                                                                muốn
                                                                xoá?</h5>
                                                            <button class="close" type="button" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">Sản phẩm: {{ $product['name'] }}</div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" type="button"
                                                                data-dismiss="modal">Huỷ</button>
                                                            <form
                                                                action="{{ route('admin.product.delete', $product['id']) }}"
                                                                method="post">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-danger" href="">Xoá</button>
                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </tr>

                                    @endforeach

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col" class="text-center">Ảnh</th>
                                        <th scope="col">Tên sản phẩm</th>
                                        <th scope="col">Loại sản phẩm</th>
                                        <th scope="col">Giá sản phẩm</th>
                                        <th scope="col">Giá khuyến mãi</th>
                                        <th scope="col">Thao tác</th>
                                    </tr>

                                </tfoot>
                            </table>
                            <ul class="pagination justify-content-end">
                                @php
                                    $totalPage = $responseData['last_page'];
                                    $currentPage = $responseData['current_page'];

                                @endphp
                                <li class="page-item"><a class="page-link" href="#">Trước</a></li>
                                @for ($i = 1; $i <= $totalPage; $i++)
                                    <li class="page-item @if ($i==$currentPage) {{ 'active' }} @endif"><a class="page-link"
                                            href="{{ route('admin.product.page', $i) }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                <li class="page-item"><a class="page-link" href="#">Sau</a></li>
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

@endsection
