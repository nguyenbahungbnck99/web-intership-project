@php
$totalPage = $listOrder['last_page'];
$currentPage = $listOrder['current_page'];

@endphp
@include('admin-old.layout.head')


<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('admin-old.layout.menu')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('admin-old.layout.header')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <div class="row">
                        <h2 class="text-left col-md-10 col-12">Danh sách đơn hàng: </h2>
                        <a class="col-md-2 col-12" href="{{ asset('admin/don-hang/them-moi') }}">
                            <button class="btn btn-primary btn-sm" style="">Thêm mới + </button>
                        </a>

                    </div>
                    <div class="row">

                        <table class="table ">
                            <tr>
                                <th style="width:150px">Tổng đơn hàng:</th>
                                <td colspan="2">{{ $total_order}}</td>
                            </tr>
                            <tr >
                                <th>Tổng doanh thu:</th>
                                <td colspan="2">{{ number_format($total_money) }} đ</td>
                            </tr>

                        </table>
                        <form action="{{ asset('admin/don-hang/danh-sach/1') }}" method="get" cla>
                            <div  class="row m-2">
                                <div class="form-group col-md-4 col-12">
                                    <label for="">Từ ngày:</label>
                                    <input type="date" class="form-control" name="from_date"
                                        value="{{ $from_date }}">
                                </div>

                                <div class="form-group col-md-4 col-12">
                                    <label for="">Đến ngày:</label>
                                    <input type="date" class="form-control" name="to_date" value="{{ $to_date }}">
                                </div>
                                <div class="form-group col-md-4 col-12">
                                    <label for="">Trạng thái:</label>
                                    <select class="form-control" id="exampleSelect1" name="status">
                                        <option value="">Tất cả</option>
                                        <option value="1" @if ($status == 1) {{ 'selected' }} @endif>Chờ xác
                                            nhận</option>
                                        <option value="2" @if ($status == 2) {{ 'selected' }} @endif>Đang giao
                                            hàng</option>
                                        <option value="3" @if ($status == 3) {{ 'selected' }} @endif>Đã giao
                                            hàng</option>
                                        <option value="4" @if ($status == 4) {{ 'selected' }} @endif>Đã huỷ
                                        </option>
                                    </select>
                                </div>


                            </div>
                            <div class="row m-2">
                                <div class="form-group col-md-4 col-12">
                                    <label for="">Mã đơn hàng:</label>
                                    <input type="text" class="form-control" name="sku"
                                        value="{{ $sku }}">
                                </div>

                                <div class="form-group col-md-4 col-12">
                                    <label for="">Tên CTV:</label>
                                    <input type="number" class="form-control" name="name" value="{{ $name }}">
                                </div>

                                <div class="form-group mt-2 col-md-2 col-12">
                                    <label for=""></label>
                                    <a href="{{ asset('admin/don-hang/danh-sach/1') }}"><button
                                            class="btn btn-primary form-control" type="submit">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button></a>
                                </div>
                            </div>

                        </form>
                    </div>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Mã CTV</th>
                                <th scope="col">Tổng sản phẩm</th>
                                <th scope="col">Tổng tiền</th>
                                <th scope="col">Ship</th>
                                <th scope="col" class="none">Thời gian mua</th>
                                <th scope="col" class="none">Nội dung mua</th>
                                <th scope="col">Trạng thái</th>
                                <th scope="col">Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($listOrder['data'] as $index => $order)
                                {{-- {{ dd($product)}} --}}
                                <?php
                                // dd($order);
                                $user = $order['user']['code_branch'] . '-' . $order['user']['code_ordinal'];
                                $date = date(' d-m-Y', strtotime($order['created_at']));
                                if ($order['status'] == 1) {
                                    $status = 'Chờ xác nhận';
                                    $none = '';
                                }
                                if ($order['status'] == 2) {
                                    $status = 'Đang giao hàng';
                                    $none = 'None';
                                }
                                if ($order['status'] == 3) {
                                    $status = 'Đã hoàn thành';
                                    $none = 'None';
                                }
                                if ($order['status'] == 4) {
                                    $status = 'Đã hủy';
                                    $none = 'None';
                                }
                                ?>
                                <tr>
                                    <th scope="row" style="vertical-align: middle ">{{ $index + 1 }}</th>
                                    <td class="products" style="vertical-align: middle ">{{ $user }}</td>
                                    <td style="vertical-align: middle ">{{ $order['total_product'] }} sản phẩm</td>

                                    <td style="vertical-align: middle ">

                                        {{ number_format($order['total_money_product']) }} đ
                                    </td>
                                    <td style="vertical-align: middle ">

                                        {{ number_format($order['ship']) }} đ
                                    </td>
                                    <td style="vertical-align: middle ">

                                        {{ $date }}
                                    </td>
                                    <td style="vertical-align: middle" class="none">
                                        {{ $order['content'] }}
                                    </td>
                                    <td style="vertical-align: middle ">
                                        {{-- <select name="" id="change" class="form-control ">
                                            <option value="1"  @if ($status == 1) {{ 'selected' }} @endif>Chờ xác nhận</option>
                                            <option value="2"  @if ($status == 2) {{ 'selected' }} @endif >Đang giao hàng</option>
                                            <option value="3"  @if ($status == 3) {{ 'selected' }} @endif >Đã giao hàng</option>
                                            <option value="4" @if ($status == 4) {{ 'selected' }} @endif>Huỷ đơn</option>

                                        </select> --}}
                                        {{ $status }}
                                    </td>


                                    {{-- <td style="vertical-align: middle ">
                                        <a href="{{ asset('admin/san-pham/cam-nhan/' . $product['id']) }}"
                                            class="btn btn-info btn-circle btn-sm">
                                            <i class="fas fa-info-circle"></i>
                                        </a>
                                    </td> --}}
                                    <td style="vertical-align: middle ">

                                        <a href="{{ asset('admin/don-hang/chi-tiet/' . $order['id']) }}"
                                            class="btn btn-info btn-circle btn-sm">
                                            <i class="fas fa-info-circle"></i>
                                        </a>

                                        <a class="btn btn-danger btn-circle btn-sm" href="" data-toggle="modal"
                                            data-target="#delete{{ $order['id'] }}">
                                            <i class="fas fa-trash"></i>

                                        </a>
                                    </td>
                                    <!-- delete Modal-->
                                    <div class="modal fade" id="delete{{ $order['id'] }}" tabindex="-1" role="dialog"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Bạn có chắn chắn muốn
                                                        xoá?</h5>
                                                    <button class="close" type="button" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-secondary" type="button"
                                                        data-dismiss="modal">Huỷ</button>
                                                    <a class="btn btn-danger"
                                                        href="{{ asset('admin/san-pham/xoa/' . $order['id']) }}">Xoá</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </tr>

                            @endforeach
                            <!-- Update-->
                            <div class="modal update fade" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Modal title</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>One fine body&hellip;</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default"
                                                data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->

                        </tbody>
                    </table>

                    <ul class="pagination justify-content-end">

                        <li class="page-item"><a class="page-link" href="#">Trước</a></li>
                        @for ($i = 1; $i <= $totalPage; $i++)
                            <li class="page-item @if ($i==$currentPage) {{ 'active' }} @endif"><a class="page-link"
                                    href="{{ asset('/admin/san-pham/danh-sach/' . $i) }}">{{ $i }}</a>
                            </li>
                        @endfor
                        <li class="page-item"><a class="page-link" href="#">Sau</a></li>
                    </ul>
                    </nav>
                </div>

                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->



            <!-- Footer -->
            @include('admin-old.layout.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>




</body>

@include('admin-old.layout.foot')
