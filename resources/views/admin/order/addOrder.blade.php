

    @include('admin/layout/head')

<body id="page-top">

    <div id="wrapper">
        @include('admin/layout/menu')
        <div id="content-wrapper" class="d-flex flex-column">

            <div id="content">

                @include('admin/layout/header')

                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Thêm mới đơn hàng: </h1>
                                </div>
                                {{-- <form class="user" method="post" action="{{route('them-don-hang')}}">
                                    @csrf
                                    <div>
                                        <select  name="product" id="" class="form-control">
                                            <option value="">Chọn sản phẩm</option>
                                            @foreach ($allProduct as $product)
                                                <option value="{{$product['id']}}">{{ $product['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                   
                                    <div class="text-center">
                                        <a href="" class="btn btn-primary btn-user " style="width:25%;" data-toggle="modal" data-target="#OrderModal">
                                            Tiếp theo
                                        </a>
                                    </div>
                                   
                                </form> --}}
                                <form method="post" action="{{ asset('admin/don-hang/add-order')}}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="input-group mb-3">
                                            <select  name="user_id" id="" class="form-control w-80">
                                                <option value="">Chọn CTV mua hàng</option>
                                                @foreach ($allCtv as $ctv)
                                                    <option value="{{$ctv['id']}}">{{ $ctv['code_branch']}}-{{$ctv['code_ordinal']}} {{$ctv['name']}}</option>
                                                @endforeach
                                            </select>
                                            </div>

                                            <div class="input-group mb-3">
                                                <input type="text" name="name_user" id="" placeholder="Họ tên người mua ..." class="form-control">
                                            </div>

                                            <div class="input-group mb-3">
                                                <input type="number" name="phone_order" id="" placeholder="SĐT người mua ..." class="form-control">
                                            </div>

                                            <div class="input-group mb-3">
                                                <input type="tẽtx" name="address_order" id="" placeholder="Địa chỉ người mua ..." class="form-control">
                                            </div>

                                            <div id="inputFormRow">
                                                <div class="input-group mb-3">
                                                    <select  name="product[]" id="" class="form-control w-80">
                                                        <option value="">Chọn sản phẩm</option>
                                                        @foreach ($allProduct as $product)
                                                            <option value="{{$product['id']}}">{{ $product['name']}}</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="number" name="number[]" id="" placeholder="Số lượng sản phẩm" class="form-control ml-2 mr-2">                                                    
                                                    <div class="input-group-append">                
                                                        <button id="addRow" type="button" class="btn btn-info">Thêm +</button>
                                                    </div>
                                                </div>
                                            </div>
                                
                                            <div id="newRow"></div>
                                            <div class="input-group mb-3">
                                                <textarea name="content" id="" class="form-control"  cols="100" rows="7" placeholder="Nhập nội dung mua hàng ..."></textarea>
                                            </div>
                                           <div class="text-center">
                                               <button class="btn btn-primary">Thêm mới</button>
                                           </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('admin/layout/footer')
        </div>


    </div>



    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    @include('admin/layout/foot')
    
    <script type="text/javascript">
        // add row
        $("#addRow").click(function () {
            var html = '';
            html += '<div id="inputFormRow">';
            html += '<div class="input-group mb-3">';
            html += ' <select  name="product[]" id="" class="form-control">';
            html+= ' <option value="">Chọn sản phẩm</option>';
            html+= ' @foreach ($allProduct as $product)';
            html+= ' <option value="{{$product['id']}}">{{ $product['name']}}</option>';
            html+= ' @endforeach </select>';
            html+=' <input type="number" name="number[]" id="" placeholder="Số lượng sản phẩm" class="form-control ml-2 mr-2">';
            html += '<div class="input-group-append">';
            html += '<button id="removeRow" type="button" class="btn btn-danger">Xoá</button>';
            html += '</div>';
            html += '</div>';

            $('#newRow').append(html);
        });

        // remove row
        $(document).on('click', '#removeRow', function () {
            $(this).closest('#inputFormRow').remove();
        });
    </script>

   

</body>

   


