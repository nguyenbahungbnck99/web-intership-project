@include('admin.layout.head')


@php
// dd($order);
$infor = $order['info'];
$ctv = $infor['user']['code_branch'] . '-' . $infor['user']['code_ordinal'];
$date = date(' d-m-Y', strtotime($infor['created_at']));

$status = $infor['status'];
@endphp

<body id="page-top">

    <div id="wrapper">
        @include('admin/layout/menu')
        <div id="content-wrapper" class="d-flex flex-column">

            <div id="content">

                @include('admin/layout/header')

                <div class="container">
                    <h1 class="text-center leader-title">Chi tiết đơn hàng: </h1>
                    <div class="card shadow mb-4">

                        <div class="card-body">
                            <div class="table-responsive">
                                <form action="{{ asset('admin/don-hang/cap-nhat/' . $infor['id']) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0"
                                        style="font-size: 15px;">
                                        <thead>
                                            <tr>
                                                <th style="width:20%">Mã CTV</th>
                                                <th>{{ $ctv }}</th>

                                            </tr>
                                            <tr>
                                                <th>Họ tên người mua</th>
                                                <th>{{ $infor['name_user'] }}</th>

                                            </tr>
                                            <tr>
                                                <th>SĐT người mua</th>
                                                <th>{{ $infor['phone_order'] }}</th>

                                            </tr>
                                            <tr>
                                                <th>Địa chỉ người mua</th>
                                                <th>{{ $infor['address_order'] }}</th>

                                            </tr>
                                            <tr>
                                                <th>Tổng sản phẩm</th>
                                                <th>{!! $infor['total_product'] !!} <br>
                                                    <a data-toggle="modal" data-target="#detail"
                                                       style="text-decoration: underline;
                                                       color: #4e73df;">
                                                        Chi tiết...
                                                    </a>
                                                      {{-- =========== modal chi tiet =============== --}}
                                            <div class="modal fade" id="detail" tabindex="-1" role="dialog"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Danh sách sản
                                                            phẩm: </h5>
                                                        <button class="close" type="button" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table">
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Tên sản phẩm</th>
                                                                <th>Giá</th>
                                                                <th>Số lượng mua</th>
                                                            </tr>
                                                            @foreach ($order['detail'] as $index => $item)
                                                                <tr>
                                                                    <td>{{ $index + 1 }}</td>
                                                                    <td>{{ $item['product_name'] }}</td>
                                                                    <td>{{ number_format($item['price']) }} đ</td>
                                                                    <td>{{ $item['number'] }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">Đóng</button>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- ================================= --}}

                                                </th>

                                            </tr>
                                          
                                            <tr>
                                                <th>Tổng giá tiền</th>
                                                <th>
                                                    {{ number_format($infor['total_money_product']) }} đ
                                                </th>

                                            </tr>
                                            <tr>
                                                <th>Tiền ship</th>
                                                <th class="form-group">
                                                    <input type="text" class="form-control" name="ship" id="ship"
                                                        value="@if (isset($infor['ship']) &&
                                                        $infor['ship'] !=null) {{ number_format($infor['ship']) }} @endif" placeholder="Nhập số tiền ship...">
                                                </th>

                                            </tr>
                                            <tr>
                                                <th>Trạng thái đơn hàng:</th>
                                                <th class="form-group">
                                                    <select name="status" class="form-control" id="">
                                                        <option value="1" @if ($status == 1) {{ 'selected' }} @endif>Chờ xác
                                                            nhận</option>
                                                        <option value="2" @if ($status == 2) {{ 'selected' }} @endif>Đang giao
                                                            hàng</option>
                                                        <option value="3" @if ($status == 3) {{ 'selected' }} @endif>Đã giao
                                                            hàng</option>
                                                        <option value="4" @if ($status == 4) {{ 'selected' }} @endif>Đã huỷ
                                                        </option>
                                                    </select>
                                                </th>

                                            </tr>
                                            <tr>
                                                <th>Thời gian mua</th>
                                                <th>{{ $date }}</th>

                                            </tr>
                                            <tr>
                                                <th>Nội dung mua</th>
                                                <th>{{ $infor['content'] }}</th>

                                            </tr>
                                            <tr>
                                                <th colspan="2" class="text-center"><button class="btn btn-primary"
                                                        type="submit">Cập nhật</button>
                                                </th>

                                            </tr>
                                        </thead>

                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            {{-- @include('layout/footer') --}}
        </div>


    </div>



    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    {{-- @include('layout/libjs') --}}

</body>

@include('admin.layout.foot')
<script>
    $("#ship").on('keyup', function() {
        var n = parseInt($(this).val().replace(/\D/g, ''), 10);
        if (!isNaN(n)) $(this).val(n.toLocaleString());
        else $(this).val();
    });
</script>
