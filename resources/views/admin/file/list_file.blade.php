@extends('admin.layouts.app')
@section('title-page', 'Danh sách file')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        {{-- <div class="card-body">
                            <form action="" method="GET" id="formSearchData">
                                <div class="row d-flex align-items-end">
                                    <div class="col-12 col-md-2">
                                        <label>Tiêu đề</label>
                                        <div class="input-group">
                                            <input type="text" autocomplete="off" class="form-control" name="title"
                                                value="{{ request('title') }}" placeholder="Tiêu đề" id="startdatepicker">
                                        </div>
                                        <!-- input-group -->
                                    </div>


                                    <button class="btn btn-primary float-right ml-auto"
                                        style="height: 40px; float:right">Search</button>

                                </div>
                            </form>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalAddFile">
                <div class="modal-dialog modal-dialog-centered ">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Thêm mới file</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ route('admin.file.store_file') }}" method="post" id="formAddCertificateItem"
                                enctype="multipart/form-data">
                                @csrf
                                @method('post')
                                <input type="hidden" name="certificate_id" id="certificate_id">
                                <div class="card-body">
                                    <div class="row">
                                        <input type="text" name="category_file_id" value="{{$category_file_id}}" hidden>
                                        <div class="col-12 ">
                                            <div class="form-group">
                                                <label for="inputName">Tên</label>
                                                <input type="text" class="form-control" id="inputName" required name="name"
                                                    placeholder="Tên chứng chỉ">
                                            </div>
                                        </div>
                                        <div class="col-12 ">
                                            <div class="form-group">
                                                <label for="imageInput">Ảnh</label>
                                                <div class="image-input">
                                                    <input type="file" accept="image/*" id="imageInput" name="image" required>
                                                    <img src="" class="image-preview" style="max-width:100px">
        
                                                </div>
        
                                            </div>
                                        </div>
                                        <div class="col-12 ">
                                            <div class="form-group">
                                                <label for="imageInput">File</label>
                                                <div class="image-input">
                                                    <input type="file" id="imageInput" name="file" required>
                                                    <img src="" class="image-preview" style="max-width:100px">
        
                                                </div>
        
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
        
                                <div class="modal-footer justify-content-between">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    <button type="submit" class="btn btn-primary">Lưu lại</button>
                                </div>
                            </form>
                        </div>
                        {{-- <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn btn-primary">Lưu lại</button>
                        </div> --}}
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h3 class="card-title">Danh sách file</h3>
                            <div class="float-right ml-auto">
                                <a class="col-md-2 col-12" href="#" data-toggle="modal" data-target="#modalAddFile">
                                    <button class="btn btn-primary btn-sm" style="">Thêm mới + </button>
                                </a>
                            </div>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="table" class="table table-bordered table-hover ">

                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col" class="text-center">Ảnh</th>
                                        <th scope="col-4">Tên</th>
                                        <th scope="col-4">file</th>

                                        <th scope="col">Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($responseData as $index => $category_file)
                                    <tr>
                                        <th scope="row" style="vertical-align: middle ">{{ ++$index }}</th>
                                        <td class="products" style="vertical-align: middle "><img
                                                src="{{ $category_file['image'] }}" width="200" class="object-fit-cover">
                                        </td>
                                        <td style="vertical-align: middle ">{{ $category_file['name'] }}</td>
                                        <td style="vertical-align: middle ">
                                            <a href="{{ $category_file['file'] }}">Tải về</a>
                                        </td>



                                        {{-- <td style="vertical-align: middle ">
                                        <a href="{{ asset('admin/san-pham/cam-nhan/' . $feedback['id']) }}"
                                            class="btn btn-info btn-circle btn-sm">
                                            <i class="fas fa-info-circle"></i>
                                        </a>
                                    </td> --}}
                                        <td style="vertical-align: middle ">
                                            <a  href="{{ route('admin.file.edit_file', $category_file['id']) }}"
                                                class="btn btn-info btn-circle btn-sm editFeedback">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a class="btn btn-danger btn-circle btn-sm" href="" data-toggle="modal"
                                                data-target="#delete{{ $category_file['id'] }}">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </td>
                                        <!-- delete Modal-->
                                        <div class="modal fade" id="delete{{ $category_file['id'] }}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Bạn có chắn chắn
                                                            muốn
                                                            xoá?</h5>
                                                        <button class="close" type="button" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">file: {{ $category_file['name'] }}</div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-secondary" type="button"
                                                            data-dismiss="modal">Huỷ</button>
                                                        <form
                                                            action="{{ route('admin.file.delete_file', $category_file['id']) }}"
                                                            method="post">
                                                            @csrf
                                                            @method('delete')
                                                            <button class="btn btn-danger" href="">Xoá</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                            {{-- <ul class="pagination justify-content-end">
                                @php
                                    $totalPage = $responseData['last_page'];
                                    $currentPage = $responseData['current_page'];

                                @endphp
                                <li class="page-item"><a class="page-link" href="#">Trước</a></li>
                                @for ($i = 1; $i <= $totalPage; $i++)
                                    <li class="page-item @if ($i==$currentPage) {{ 'active' }} @endif"><a class="page-link"
                                            href="{{ asset('/admin/san-pham/danh-sach/' . $i) }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                <li class="page-item"><a class="page-link" href="#">Sau</a></li>
                            </ul> --}}
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <div class="modal fade" id="modalUpdateFile">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cập nhật danh mục file</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="formUpdateFeedback" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <input type="text" name="category_file_id" value="{{$category_file_id}}" hidden>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="inputTitle">Tên</label>
                                        <input type="text" class="form-control" id="inputTitle" required name="name"
                                            placeholder="Tiêu đề feedback">
                                    </div>
                                </div>


                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="imageInput">Ảnh</label>
                                        <div class="image-input">
                                            <input type="file" accept="image/*" id="imageInput" name="image" >
                                            <img src="" class="image-preview" style="max-width:100px">

                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="form-group">
                                        <label for="imageInput">file</label>
                                        <div class="image-input">
                                            <input type="file" id="imageInput" name="file" >
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('after-scripts')
    <script>
        $('.editFeedback').click(function(e) {
            e.preventDefault();

            let href = $(this).attr('href');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "GET",
                url: href,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(res) {
                    let feedback = res.data;
                    $('#formUpdateFeedback input[name=name]').val(feedback.name);
                    $('#formUpdateFeedback .image-preview').attr('src', feedback.image);
                    $('#formUpdateFeedback .file-preview').attr('src', feedback.file);
                    $('#formUpdateFeedback').attr('action', res.action);
                    $('#modalUpdateFile').modal('show');
                    console.log(res);
                },
                dataType: false
            });
        });
    </script>
@endsection
