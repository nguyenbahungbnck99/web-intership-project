@extends('layouts.app')
@section('title-page', 'Chi tiết chứng chỉ')

@section('content')
    <section>
        <div class="container">

            <!--/ Title row end -->
            <div class="row">
                <div class="col-12 col-sm-12 col-md-7 ">
                    <div
                        class="text-center d-flex justify-content-center align-items-center   box-certificate box-certificate-one">
                        <div class="box-content-one">
                            <div class="name-certificate ">
                                {{ $certificate['name'] }}
                            </div>
                            <div class="title-certificate mb-4">

                                {{ $certificate['slogan'] }}
                            </div>
                            <div class="description-certificate">
                                {{ $certificate['description'] }}
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-5  d-flex justify-content-center align-content-center ">
                    <div class="box-certificate-one">
                        <img src="{{ $certificate['image'] }}" class=" object-fit-cover " height="250" alt="certificate">

                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 col-sm-12 col-md-7 ">
                    <div
                        class="box-certificate box-certificate-second box-certificate-second-left text-center d-flex justify-content-center ">
                        @if (!empty($certificate['certificate_item']))
                            @foreach ($certificate['certificate_item'] as $item)
                                <div class="box ">
                                    <div class="box-item-image-certificate">
                                        <img src="{{ $item['item_image'] }}" class="object-fit-cover item-image-icon"
                                            alt="{{ $item['name'] }}">
                                    </div>
                                    <div class="box-content-item-certificate">
                                        <div class="name-certificate-item ">
                                            {{ $item['name'] }}
                                        </div>
                                        <div class="title-certificate-item mb-4">

                                            {{ $item['title'] }}
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                        @endif
                    </div>

                </div>
                <div class="col-12 col-sm-12 col-md-5  ">
                    <div
                        class="  box-certificate box-certificate-second box-certificate-second-content  text-center d-flex justify-content-center">
                        <div class="text-second">
                            <div class="name-certificate ">
                                {{ $certificate['name'] }}
                            </div>
                            <div class="title-certificate mb-4">

                                {{ !empty($certificate['slogan_2']) ? $certificate['slogan_2'] : $certificate['slogan'] }}
                            </div>
                            <div class="description-certificate">
                                {{ !empty($certificate['description_2']) ? $certificate['description_2'] : $certificate['description'] }}
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!--/ Container end -->
    </section>
    <section>
        <div class="container">
            @if (count($certificate['certificate_item']) < 4)
                <div class="center_1_2_3 center slider">

                    @if (!empty($certificate['certificate_item']))
                        @foreach ($certificate['certificate_item'] as $item)
                            <div class="box-item-certificate-image">
                                <img src="{{ $item['image'] }}" class="w-100 image-certificate-list frame"
                                    alt="{{ $item['name'] }}">
                            </div>
                        @endforeach
                    @endif
                </div>

            @else

                <div class="center_4 center slider">

                    @if (!empty($certificate['certificate_item']))
                        @foreach ($certificate['certificate_item'] as $item)
                            <div class="box-item-certificate-image">
                                <img src="{{ $item['image'] }}" class="w-100 image-certificate-list frame"
                                    alt="{{ $item['name'] }}">
                            </div>
                        @endforeach
                    @endif
                </div>
            @endif

        </div>
    </section>
@endsection
@section('after-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('template\slick-master\slick\slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template\slick-master\slick\slick-theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/certificate.css') }}">
    <style>


    </style>
@endsection
@section('after-scripts')
    <script src="{{ asset('template\slick-master\slick\slick.js') }}"></script>

    <script>
        $(function() {
            // $(".center").slick({
            //     dots: true,
            //     infinite: true,
            //     centerMode: true,
            //     slidesToShow: 1,
            //     slidesToScroll: 1
            // });
            $(".center_1_2_3").slick({
                dots: true,
                infinite: true,
                centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });
            $('.center_4').slick({
                dots: true,
                infinite: true,
                centerMode: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                // autoplay: true,
                lazyLoad: 'ondemand',
                autoplaySpeed: 4000,
                responsive: [{
                        breakpoint: 1366,
                        settings: {
                            dots: true,
                            infinite: true,
                            centerMode: true,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    }, {
                        breakpoint: 1200,
                        settings: {
                            dots: true,
                            infinite: true,
                            centerMode: true,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 1000,
                        settings: {
                            dots: true,
                            infinite: true,
                            centerMode: true,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 800,
                        settings: {
                            dots: true,
                            infinite: true,
                            centerMode: true,
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    }, {
                        breakpoint: 700,
                        settings: {
                            dots: true,
                            infinite: true,
                            centerMode: true,
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            dots: true,
                            infinite: true,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }

                ]
            })
            $('.item-height').matchHeight();
            $('.box-certificate-second').matchHeight();
            $('.box-certificate-one').matchHeight();
        });
    </script>
@endsection
