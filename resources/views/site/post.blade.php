@extends('layouts.app')
@section('title-page', $dataContent['category']['name'])

@section('content')
<section class="all-product">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="/">Trang chủ</a>
            </li>

            <li>
                <a href="{{ route('categoryContent', $dataContent['category']['slug']) }}" class="active">

                    {{ $dataContent['category']['name'] }}
                </a>
            </li>
        </ul>

        <!--/ Title row end -->

        <div class="post-one">
            <div class="row">

                @foreach ($dataContent['postNew'] as $key => $post)
                    {{-- @if ($key == 0 || $key == 1) --}}
                    <div class="col-6 col-md-4 mb-2">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="box-image-post">
                                    <a href="{{ route('detailContent', $post['slug']) }}">
                                        <img src="{{ $post['list_image'][0]['image'] }}" class="rounded image-post objec-fit-cover"
                                            alt="{{ $post['title'] }}">
                                    </a>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="title-post-one-1 line-two">
                                    <a href="{{ route('detailContent', $post['slug']) }}">
                                        {{ $post['title'] }}
                                    </a>

                                </div>
                                {{-- <div class="description-post-one-1 line-three">
                                    {{ $post['title'] }}
                                </div> --}}
                                <div class="date-post">
                                    <i class="fa fa-calendar"></i> {{ $post['created_at'] }}
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- @endif --}}
                @endforeach
            </div>

        </div>
    </div>
    <!--/ Container end -->
</section>
@endsection
@section('after-css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/post.css') }}">
@endsection
