@extends('layouts.app')
@section('title-page', 'Chứng chỉ')

@section('content')
    {{-- <section>
        <div class="container">
            <div class="row text-left">
                <div class="col-12">
                    <h3 class="section-title">Các khóa đào tạo nổi bật</h3>
                </div>
            </div>
            <!--/ Title row end -->
            <div class="row mb-4 ">
                <div class="col-12 col-sm-12 col-md-6">
                    <div class=" box-video ">
                        <div class="box-image-video img-thumbnail rounded">
                            <img src="{{ asset('images/certificate2.png') }}" class="w-100 " alt="img">
                        </div>
                        <div class="box-content-video">
                            <div class="title-video">
                                Khoá học đào tạo bán hàng thời kì 4.0
                            </div>
                            <div class="longtime mt-2">
                                Thời lượng: 18:00
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6">
                            <div class=" box-video-right ">
                                <div class="box-image-video-right img-thumbnail rounded">
                                    <img src="{{ asset('images/certificate2.png') }}" class="w-100 " alt="img">
                                </div>
                                <div class="box-content-video-right">
                                    <div class="title-video-right">
                                        Khoá học đào tạo bán hàng thời kì 4.0
                                    </div>
                                    <div class="longtime-right ">
                                        Thời lượng: 18:00
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6">
                            <div class=" box-video-right ">
                                <div class="box-image-video-right img-thumbnail rounded">
                                    <img src="{{ asset('images/certificate2.png') }}" class="w-100 " alt="img">
                                </div>
                                <div class="box-content-video-right">
                                    <div class="title-video-right">
                                        Khoá học đào tạo bán hàng thời kì 4.0
                                    </div>
                                    <div class="longtime-right ">
                                        Thời lượng: 18:00
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6">
                            <div class=" box-video-right ">
                                <div class="box-image-video-right img-thumbnail rounded">
                                    <img src="{{ asset('images/certificate2.png') }}" class="w-100 " alt="img">
                                </div>
                                <div class="box-content-video-right">
                                    <div class="title-video-right">
                                        Khoá học đào tạo bán hàng thời kì 4.0
                                    </div>
                                    <div class="longtime-right ">
                                        Thời lượng: 18:00
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6">
                            <div class=" box-video-right ">
                                <div class="box-image-video-right img-thumbnail rounded">
                                    <img src="{{ asset('images/certificate2.png') }}" class="w-100 " alt="img">
                                </div>
                                <div class="box-content-video-right">
                                    <div class="title-video-right">
                                        Khoá học đào tạo bán hàng thời kì 4.0
                                    </div>
                                    <div class="longtime-right ">
                                        Thời lượng: 18:00
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Container end -->
    </section> --}}
    <section>
        <div class="container">
            <div class="row text-left">
                <div class="col-12">
                    <h3 class="section-title">Các khóa đào tạo</h3>
                </div>
            </div>
            <!--/ Title row end -->
            <div class="row">
                @foreach($data as $d)
                    <div class="col-12 col-sm-12 col-md-3">
                        <div class=" box-video-right ">
                            <div class="box-image-video-right img-thumbnail rounded">
                                @php
                                    $linkvd = $d['video'];
                                    $check = substr($linkvd, 0, 5);
                                    // dd($check);
                                    if ($check == 'https') {
                                        $num = 32;
                                    } else {
                                        $num = 31;
                                    }
                                    $url = substr($linkvd, $num);
                                    // dd($url);
                                @endphp
                            <iframe width="100%" height="200px"src="https://www.youtube.com/embed/{{ $url }}?autoplay=0" frameborder="0"></iframe>
                                {{-- <img src="{{ asset('images/certificate2.png') }}" class="w-100 " alt="img"> --}}
                            </div>
                            <div class="box-content-video-right">
                                <div class="title-video-right">
                                    {{$d['name']}}
                                </div>
                                <div class="longtime-right ">
                                    Thời lượng: {{$d['minute']}} phút
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{-- <div class="col-12 col-sm-12 col-md-3">
                    <div class=" box-video-right ">
                        <div class="box-image-video-right img-thumbnail rounded">
                            <img src="{{ asset('images/certificate2.png') }}" class="w-100 " alt="img">
                        </div>
                        <div class="box-content-video-right">
                            <div class="title-video-right">
                                Khoá học đào tạo bán hàng thời kì 4.0
                            </div>
                            <div class="longtime-right ">
                                Thời lượng: 18:00
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-3">
                    <div class=" box-video-right ">
                        <div class="box-image-video-right img-thumbnail rounded">
                            <img src="{{ asset('images/certificate2.png') }}" class="w-100 " alt="img">
                        </div>
                        <div class="box-content-video-right">
                            <div class="title-video-right">
                                Khoá học đào tạo bán hàng thời kì 4.0
                            </div>
                            <div class="longtime-right ">
                                Thời lượng: 18:00
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3">
                    <div class=" box-video-right ">
                        <div class="box-image-video-right img-thumbnail rounded">
                            <img src="{{ asset('images/certificate2.png') }}" class="w-100 " alt="img">
                        </div>
                        <div class="box-content-video-right">
                            <div class="title-video-right">
                                Khoá học đào tạo bán hàng thời kì 4.0
                            </div>
                            <div class="longtime-right ">
                                Thời lượng: 18:00
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-3">
                    <div class=" box-video-right ">
                        <div class="box-image-video-right img-thumbnail rounded">
                            <img src="{{ asset('images/certificate2.png') }}" class="w-100 " alt="img">
                        </div>
                        <div class="box-content-video-right">
                            <div class="title-video-right">
                                Khoá học đào tạo bán hàng thời kì 4.0
                            </div>
                            <div class="longtime-right ">
                                Thời lượng: 18:00
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3">
                    <div class=" box-video-right ">
                        <div class="box-image-video-right img-thumbnail rounded">
                            <img src="{{ asset('images/certificate2.png') }}" class="w-100 " alt="img">
                        </div>
                        <div class="box-content-video-right">
                            <div class="title-video-right">
                                Khoá học đào tạo bán hàng thời kì 4.0
                            </div>
                            <div class="longtime-right ">
                                Thời lượng: 18:00
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-3">
                    <div class=" box-video-right ">
                        <div class="box-image-video-right img-thumbnail rounded">
                            <img src="{{ asset('images/certificate2.png') }}" class="w-100 " alt="img">
                        </div>
                        <div class="box-content-video-right">
                            <div class="title-video-right">
                                Khoá học đào tạo bán hàng thời kì 4.0
                            </div>
                            <div class="longtime-right ">
                                Thời lượng: 18:00
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3">
                    <div class=" box-video-right ">
                        <div class="box-image-video-right img-thumbnail rounded">
                            <img src="{{ asset('images/certificate2.png') }}" class="w-100 " alt="img">
                        </div>
                        <div class="box-content-video-right">
                            <div class="title-video-right">
                                Khoá học đào tạo bán hàng thời kì 4.0
                            </div>
                            <div class="longtime-right ">
                                Thời lượng: 18:00
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
        <!--/ Container end -->
    </section>
@endsection
@section('after-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/video.css') }}">
@endsection
@section('after-scripts')
    <script>
        $(function() {
            // $('.item-matchheight').matchHeight();
            $('.title-file').click(function() {
                if ($(this).next().css('display') == 'none') {
                    $(this).next().show();

                    //    $('.item-matchheight').matchHeight();
                } else {
                    $(this).next().hide();
                    //    $('.item-matchheight').matchHeight();

                }
            });
        });
    </script>
@endsection
