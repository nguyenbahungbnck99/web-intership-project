@extends('layouts.app')
@section('title-page', 'Feedback')

@section('content')
    <section>
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3 class="section-title">Feedback <span style="font-size: 14px;font-weight:500">nổi bật của khách
                            hàng</span></h3>
                </div>
            </div>
            <!--/ Title row end -->
            @if (!empty($feedbackHot))
                <section class="center slider">
                    @foreach ($feedbackHot as $feedback)
                        <div class="box">
                            <div class="box-feedback rounded relative">
                                <div class="box-image">
                                    <img src="{{ $feedback['image'] }}" class="item-height w-100 object-fit-cover"
                                        alt="{{ $feedback['title'] }}">
                                </div>
                                <div class="box-content-feedback text-center absolute ">
                                    <div class="title-feedback-slide">
                                        {{ $feedback['title'] }}
                                    </div>
                                    <a href="{{ $feedback['image'] }}" class="btn btn-sm btn-download-slide mt-1"
                                        download>
                                        <i class="fas fa-cloud-download-alt"></i>Tải ảnh

                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </section>
            @endif


        </div>

        <!--/ Container end -->
    </section>
    <section>
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3 class="section-title">Tất cả feedback</h3>
                    <h4>Khách hàng của chúng tôi</h4>
                    <p class="pd-description">Sữa Anlilac giới thiệu sản phẩm công thức siêu cao cấp cho trẻ đạt chứng nhận
                        châu âu đầu tiên sản
                        xuất tại việt nam. Tự hào chinh phục các chứng chỉ quốc tế đạt tiêu chuẩn. </p>
                </div>
            </div>
            <!--/ Title row end -->

            <div class="row">
                @foreach ($listFeedback as $feedback)
                    <div class="col-12 col-md-6 col-lg-4 mb-4">
                        <div class="box-feedback-item position-relative">
                            <div class="box-content-item position-absolute bottom-0">
                                <div class="content-left d-flex align-items-center justify-content-center text-center">
                                    <div>
                                        <div class="title-feedback-item">
                                            {{ $feedback['title'] }}
                                        </div>
                                        <a href="{{ $feedback['image'] }}"
                                            class="btn btn-sm btn-download mt-3 d-inline-block">
                                            <i class="fas fa-cloud-download-alt"></i>Tải ảnh

                                        </a>
                                    </div>

                                </div>
                            </div>
                            <div class="box-image-item position-absolute rounded">
                                <img src="{{ $feedback['image'] }}" class=" w-100 h-100 rounded frame"
                                    alt="{{ $feedback['title'] }}">
                            </div>

                        </div>

                    </div>
                @endforeach

            </div>
        </div>
        {{-- <div class="col-12 col-md-3 mb-4">
            <a href="{{ $feedback['image'] }}" class="" download>
                <div class="box">
                    <div class="box-feedback rounded relative">
                        <div class="box-image mr-2 ml-2 ">
                            <img src="{{ $feedback['image'] }}" class="item-height w-100"
                                alt="{{ $feedback['title'] }}">
                        </div>
                        <div class="box-content-feedback box-content-feedback-item text-center absolute bottom-40 w-100">
                            <div class="title-feedback">
                                {{ $feedback['title'] }}
                            </div>
                            <a href="{{ $feedback['image'] }}" class="btn btn-download mt-3" download>
                                <i class="fas fa-cloud-download-alt"></i>Tải ảnh

                            </a>
                        </div>
                    </div>
                </div>

            </a>
        </div> --}}
        <!--/ Container end -->
    </section>
@endsection
@section('after-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('template\slick-master\slick\slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template\slick-master\slick\slick-theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/feedback.css') }}">
    <style>


    </style>
@endsection
@section('after-scripts')
    <script src="{{ asset('template\slick-master\slick\slick.js') }}"></script>

    <script>
        $(function() {
            // $(".center").slick({
            //     dots: true,
            //     infinite: true,
            //     centerMode: true,
            //     slidesToShow: 3,
            //     slidesToScroll: 1
            // });
            $('.center').slick({
                dots: true,
                infinite: true,
                centerMode: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                // autoplay: true,
                lazyLoad: 'ondemand',
                autoplaySpeed: 4000,
                responsive: [{
                        breakpoint: 1363,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    }, {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 1000,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 800,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    }, {
                        breakpoint: 700,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }

                ]
            })
            $('.item-height').matchHeight();
        });
    </script>
@endsection
