@extends('layouts.app')
@section('title-page', 'Trang chủ')

@section('content')
    <section>
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3 class="section-title">Sản phẩm bán chạy</h3>
                </div>
            </div>
            <!--/ Title row end -->

            <div class="row">
                @foreach ($listHotProduct as $product)
                    @if ($product['slug'] != null)
                        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mb-3">

                            <a href="{{ route('detailProduct', $product['slug']) }}" title="{{ $product['name'] }}">
                                <div class="box-product rounded relative">

                                    <div class="box__image__product box-image">
                                        <div class="CropImg CropImg80">
                                            <div class="thumbs">
                                                <img src="blank.gif" class="lazy object-fit-cover item-height-image"
                                                    data-lazy="{{ $product['image'] }}"
                                                    data-src="{{ $product['image'] }}" alt=" {{ $product['name'] }}"
                                                    src="{{ $product['image'] }}">
                                            </div>
                                        </div>
                                        <div class="detail">
                                            <a href="{{ route('detailProduct', $product['slug']) }}"
                                                class="btn btn-success btn-sm">Chi
                                                tiết</a>
                                        </div>
                                    </div>

                                    <div class="box-content text-center item-hot-product-content">
                                        <div class="title-product title__product">
                                            <a href="{{ route('detailProduct', $product['slug']) }}">

                                                {{ $product['name'] }}
                                            </a>

                                        </div>
                                        <div class="price-product">
                                            {{ number_format($product['price'], 0, '', '.') }} đ

                                        </div>
                                    </div>


                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach

            </div>
        </div>
        <!--/ Container end -->
    </section>
    @if (!empty($newProduct))
        <section class="product-new">
            <div class="container">
                <div class="row text-center">
                    <div class="col-12">
                        <h3 class="section-title">Sản phẩm mới</h3>
                    </div>
                </div>
                <div class="row">
                    {{-- <a href="{{ route('detailProduct', $product['slug']) }}"> --}}

                    <div class="col-12 col-sm-12 col-md-4">
                        <div class="box-image" style="border-bottom:none">
                            <img src="{{ $newProduct['image'] }}" class=" w-100 objec-fit-cover rounded"
                                alt="{{ $newProduct['name'] }}">

                        </div>
                    </div>
                    {{-- </a> --}}

                    <div class="col-12 col-sm-12 col-md-8">
                        <div class="box-right-product-new">
                            <div class="label-product-new">
                                Sản phẩm mới nhất
                            </div>
                            <div class="title-product-new">
                                <a href="{{ route('detailProduct', $newProduct['slug']) }}">

                                    {{ $newProduct['name'] }}
                                </a>

                            </div>
                            <div class="description-product-new">
                                {!! $newProduct['description'] !!}
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </section>
    @endif


    <section class="all-product">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3 class="section-title">Tất cả sản phẩm của chúng tôi</h3>
                </div>
            </div>
            <!--/ Title row end -->

            <div class="row">

                @foreach ($listAllProduct['data'] as $product)
                    <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                        <a href="{{ route('detailProduct', $product['slug']) }}" title="{{ $product['name'] }}">
                            <div class="box-product rounded relative">

                                <div class="box__image__product box-image">
                                    <div class="CropImg CropImg80">
                                        <div class="thumbs">
                                            <img src="blank.gif" class="lazy object-fit-cover item-height-image"
                                                data-lazy="{{ $product['image'] }}" data-src="{{ $product['image'] }}"
                                                alt=" {{ $product['name'] }}" src="{{ $product['image'] }}">
                                        </div>
                                    </div>
                                    <div class="detail">
                                        <a href="{{ route('detailProduct', $product['slug']) }}"
                                            class="btn btn-success btn-sm">Chi
                                            tiết</a>
                                    </div>
                                </div>

                                <div class="box-content text-center item-hot-product-content">
                                    <div class="title-product title__product">
                                        <a href="{{ route('detailProduct', $product['slug']) }}">

                                            {{ $product['name'] }}
                                        </a>

                                    </div>
                                    <div class="price-product">
                                        {{ number_format($product['price'], 0, '', '.') }} đ

                                    </div>
                                </div>


                            </div>
                        </a>

                    </div>

                    {{-- @endif --}}
                @endforeach

            </div>

            {{-- </section> --}}

        </div>
        <!--/ Container end -->
    </section>
@endsection
@section('after-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('template\slick-master\slick\slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template\slick-master\slick\slick-theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">
    <style>
        .slider {
            width: 100%;
            margin: 0 auto;
        }

        .slick-list {
            padding: 0 !important;
            margin-left: 0px !important;
        }

        .slick-slide {
            margin: 0px 3px;
        }

    </style>
@endsection
@section('after-scripts')
    <script src="{{ asset('template\slick-master\slick\slick.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.slideProduct').slick({

                slidesToShow: 6,
                slidesToScroll: 3,
                // autoplay: true,
                lazyLoad: 'ondemand',
                autoplaySpeed: 4000,
                responsive: [{
                        breakpoint: 1366,
                        settings: {
                            slidesToShow: 6,
                            slidesToScroll: 3,
                            infinite: true,
                        }
                    }, {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 6,
                            slidesToScroll: 3,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 1000,
                        settings: {
                            slidesToShow: 6,
                            slidesToScroll: 3,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 800,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 2
                        }
                    }, {
                        breakpoint: 700,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    }

                ]
            })
        });
    </script>
@endsection
