<a href="{{ route('detailProduct', $product['slug']) }}"
    title="{{ $product['name'] }}">
    <div class=" ">
        <div class="box-product rounded relative">

            <div class="box__image__product box-image">
                <div class="CropImg CropImg80">
                    <div class="thumbs">
                        <img src="blank.gif" class="lazy" data-lazy="{{ $product['image'] }}"
                            data-src="{{ $product['image'] }}"
                            alt="KEM DƯỠNG DA, DƯỠNG ẨM TOÀN THÂN LÔ HỘI ALOINS EAUDE CREAM"
                            src="{{ $product['image'] }}">
                    </div>
                </div>
                <div class="detail">
                    <a href="{{ route('detailProduct', $product['slug']) }}"
                        class="btn btn-success btn-sm">Chi
                        tiết</a>
                </div>

            </div>

            <div class="box-content text-center item-hot-product-content">
                <div class="title-product title__product">
                    <a href="{{ route('detailProduct', $product['slug']) }}">

                        {{ $product['name'] }}
                    </a>

                </div>
                <div class="price-product">
                    {{ number_format($product['price'], 0, '', '.') }} đ

                </div>
            </div>


        </div>
    </div>
</a>
