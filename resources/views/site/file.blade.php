@extends('layouts.app')
@section('title-page', 'Chứng chỉ')

@section('content')
    <section>
        <div class="container">
            {{-- <div class="row text-center">
                <div class="col-12">
                    <h3 class="section-title">Chứng chỉ <span style="font-size: 14px;font-weight:500">Chất lượng sản
                            phẩm</span></h3>
                </div>
            </div> --}}
            <!--/ Title row end -->
            @foreach ($data as $d)
                <div class="row mb-4 box-file rounded">
                    <div class="col-12 col-sm-12 col-md-7">
                        <div class="  ">
                            <div class="title-file">
                                <i class="fas fa-folder icon-file"></i>
                                {{$d['name']}}
                            </div>
                            <div class="list-file">
                                    @if(count($d['file']) > 0)
                                        @foreach ($d['file'] as $f)
                                            <div class="box-item-file d-flex align-items-center">
                                                <div class="box-item-img">
                                                    <img class="mr-3" src="{{ $f['image'] }}" class="img-100"
                                                        style="width:60px" alt="Generic placeholder image">
                                                </div>
                                                {{-- <div class="d-flex">

                                                </div> --}}
                                                <div class="title-item-file">
                                                    {{ $f['name'] }}
                                                </div>
                                                <a href="{{ $f['file'] }}" class="d-inline-block ml-auto">
                                                    <div class="item-download ml-auto" style="width:90px">
                                                        <i class="fas fa-cloud-download-alt mr-2"></i>Tải về
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    @endif
                            </div>
                            <div class="datetime-file mb-0 bottom-10">
                                <i class="far fa-star"></i>
                                {{$d['file_count']}} file(s), updated {{date('d-m-Y', strtotime($d['created_at']))}}
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-5">
                        {{-- <a href="{{ route('detailCertificate') }}" class=" d-inline-block"> --}}
                            <img src="{{ $d['image'] }}" class=" w-100"
                                alt="certificate">
                        {{-- </a> --}}
                    </div>
                </div>
            @endforeach


        </div>
        <!--/ Container end -->
    </section>
@endsection
@section('after-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/file.css') }}">
@endsection
@section('after-scripts')
    <script>
        $(function() {
            // $('.item-matchheight').matchHeight();
            $('.title-file').click(function() {
                if ($(this).next().css('display') == 'none') {
                    $(this).next().show();

                //    $('.item-matchheight').matchHeight();
                } else {
                    $(this).next().hide();
                //    $('.item-matchheight').matchHeight();

                }
            });
        });
    </script>
@endsection
