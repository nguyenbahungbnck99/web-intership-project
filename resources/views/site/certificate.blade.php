@extends('layouts.app')
@section('title-page', 'Chứng chỉ')

@section('content')
    <section>
        <div class="container">

            <section>
                <div class="container">
                    <div class="row text-center">
                        <div class="col-12">
                            <h3 class="section-title">Chứng chỉ <span style="font-size: 14px;font-weight:500">Chất lượng sản
                                    phẩm</span></h3>
                        </div>
                    </div>
                    <!--/ Title row end -->
                    <div class="row mb-4">
                        @foreach ($twoCertificate as $key => $certificate)
                            @if ($key == 0)
                                <div class="col-12 col-sm-12 col-md-6">
                                    <div class="box-certificate" style="background:#b6ebde">
                                        <div class="box-certificate-content">
                                            <div class="title-certificate">
                                                <a href="{{ route('detailCertificate', $certificate['slug']) }}">
                                                    {{ $certificate['name'] }}
                                                </a>
                                            </div>
                                            <div class="description-certificate">
                                                {{ $certificate['description'] }}
                                            </div>
                                        </div>

                                        <div class=" row">
                                            <div
                                                class="col-12 col-sm-12 col-md-8 d-flex  align-items-center justify-content-center">
                                                <div class="list-box-certificate-image ">
                                                    @foreach ($certificate['certificate_item'] as $item)
                                                        <img src="{{ $item['item_image'] }}" width="60px" height="60"
                                                            class="rounded-circle" alt="{{ $item['name'] }}">
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4">
                                                <img src="{{ $certificate['image'] }}" class="w-100 rounded frame"
                                                    alt="{{ $certificate['name'] }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="col-12 col-sm-12 col-md-6">
                                    <div class="box-certificate certificate_2" style="background:#f2f0ff">
                                        <div class="box-certificate-content">
                                            <div class="title-certificate">
                                                <a href="{{ route('detailCertificate', $certificate['slug']) }}">
                                                    {{ $certificate['name'] }}
                                                </a>
                                            </div>
                                            <div class="description-certificate">
                                                {{ $certificate['description'] }}
                                            </div>
                                        </div>

                                        <div class=" row">
                                            <div
                                                class="col-12 col-sm-12 col-md-8 d-flex  align-items-center justify-content-center">
                                                <div class="list-box-certificate-image ">
                                                    @foreach ($certificate['certificate_item'] as $item)
                                                        <img src="{{ $item['item_image'] }}" width="60px" height="60"
                                                            class="rounded-circle" alt="{{ $item['name'] }}">
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4">
                                                <img src="{{ $certificate['image'] }}" class="w-100 frame"
                                                    alt="{{ $certificate['name'] }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        @endforeach


                    </div>
                    <div class="row text-center">
                        <div class="col-12">
                            <h3 class="section-title">Tất cả chứng chỉ của chúng tôi</h3>
                        </div>
                    </div>
                    <div class="row">

                        @foreach ($listCertificate as $certificate)
                            <div class="col-12 col-sm-6 col-md-3 col-lg-3 mb-3">
                                <a href="{{ route('detailCertificate', $certificate['slug']) }}">
                                    <div class="box-product rounded">
                                        <div class="box-image item-hot-product-image">
                                            <img src="{{ $certificate['image'] }}"
                                                class=" w-100 item-height-image object-fit-contain rounded " alt="sua">
                                        </div>
                                        <div class="box-content-certificate-item text-center item-hot-product-content">
                                            <div class="title-product-item line-two">
                                                {{ $certificate['name'] }}
                                            </div>
                                            <div class="d-flex align-items-center  justify-content-center">
                                                @foreach ($certificate['certificate_item'] as $key => $item)
                                                    @if ($key < 3)

                                                        <img src="{{ $item['item_image'] }}" class="rounded-circle item-icon-list"
                                                            alt="{{ $item['name'] }}">
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </a>

                            </div>


                        @endforeach

                    </div>

                </div>
                <!--/ Container end -->
            </section>
        </div>
        <!--/ Container end -->
    </section>
@endsection
@section('after-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/certificate.css') }}">
@endsection
@section('after-scripts')
    <script src="{{ asset('template\slick-master\slick\slick.js') }}"></script>

    <script>
        $(function() {

            $('.description-certificate').matchHeight();

            $('.title-certificate').matchHeight();

            $(".center").slick({
                dots: true,
                infinite: true,
                centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        });
    </script>
@endsection
