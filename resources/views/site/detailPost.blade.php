@extends('layouts.app')
@section('title-page', $content['content']['title'] )

@section('content')
<section class="all-product">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="/">Trang chủ</a>
            </li>

            <li>
                <a href="{{ route('categoryContent', $content['content']['category']['slug']) }}" class="active">
                    {{ $content['content']['category']['name'] }}
                </a>
            </li>
            {{-- <li>
                <a href="{{ route('detailContent',$content['slug'] )}}" class="active">
                    {{ $content['title']}}
                </a>
            </li> --}}
        </ul>

        <div class="row ">
            <div class="col-12 col-md-8">
                <h1 class="title-detail-post"> {{ $content['content']['title'] }}</h1>
                <p>
                    {!! $content['content']['content'] !!}
                </p>
                {{-- <div class="fb-share-button" data-href="{{ route('detailContent', $content['content']['slug']) }}"
                    data-layout="button_count" data-size="small">
                    <a target="_blank"
                        href="https://www.facebook.com/sharer/sharer.php?u={{ route('detailContent', $content['content']['slug']) }}&amp;src=sdkpreparse"
                        class="fb-xfbml-parse-ignore">Chia sẻ
                    </a>
                </div> --}}
            </div>
            <div class="col-12 col-md-4">
                <h3>Bài viết liên quan</h3>
                @foreach ($content['postRelated'] as $post)
                    <div class="row mb-2">
                        <div class="col-4 col-md-3">
                            <a href="{{ route('detailContent', $post['slug']) }}">

                                <div class="box-image-post">
                                    <img src="{{ $post['list_image'][0]['image'] }}"
                                        class="image-post-right w-100 objec-fit-cover rounded"
                                        alt="{{ $post['title'] }}">
                                </div>
                            </a>
                        </div>
                        <div class="col-8 col-md-9">
                            <div class="title-post-one-1 title-post-right line-two">
                                <a href="{{ route('detailContent', $post['slug']) }}">
                                    {{ $post['title'] }}
                                </a>

                            </div>
                            {{-- <div class="description-post-one-1 line-three">
                                {{ $post['title'] }}
                            </div> --}}
                            <div class="date-post">
                                <i class="fa fa-calendar"></i> {{ $post['created_at'] }}
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
        <!--/ Title row end -->


    </div>
    <!--/ Container end -->
</section>


@endsection
@section('after-css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/post.css') }}">
@endsection
{{-- @section('after-scripts')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v11.0&appId=390443529002528&autoLogAppEvents=1"
nonce="6AY9b1Xq"></script>
@endsection --}}
