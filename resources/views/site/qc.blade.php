@extends('layouts.app')
@section('title-page', 'Chứng chỉ')

@section('content')
    <section>
        <div class="container">
            {{-- <div class="row text-center">
                <div class="col-12">
                    <h3 class="section-title">Chứng chỉ <span style="font-size: 14px;font-weight:500">Chất lượng sản
                            phẩm</span></h3>
                </div>
            </div> --}}
            <!--/ Title row end -->
            {{-- <div class="row mb-4">
                <div class="col-12 col-sm-12 col-md-7">
                    <div class="box-certificate"> --}}
            {{-- <div class="title-certificate">
                            Chứng chỉ chất lượng sản phẩm sữa enililac
                        </div>
                        <div class="description-certificate">
                            Sữa Anlilac giới thiệu sản phẩm công thức siêu cao cấp cho trẻ đạt chứng nhận châu âu đầu tiên
                            sản xuất tại việt nam.
                        </div> --}}
            {{-- <a href="{{ route('detailCertificate') }}"> --}}
            {{-- <img src="{{ asset('images/certificate1.png') }}" class="item-matchheight w-100"
                            alt="certificate"> --}}
            {{-- </a> --}}
            {{-- </div>
                </div>
                <div class="col-12 col-sm-12 col-md-5 d-flex align-items-center justify-content-center">
                    <div class="text-qc-right">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis distinctio praesentium enim at
                        tempore esse quidem, animi alias numquam quod inventore magnam! Officia dolore error fuga
                        consequatur ipsum harum ullam?
                    </div>
                </div>
            </div> --}}
            <section>
                <div class="container">
                    <!--/ Title row end -->
                    <div class="row mb-4 qc">
                        @foreach ($data as $d)
                            <div class="col-12 col-sm-12 col-md-4">
                                <a href="{{ route('detailQc', $d['id']) }}">

                                    <div class="box position-relative item-height">
                                        {{-- <a href="{{ route('detailCertificate') }}"> --}}
                                        <div class="box-img">
                                            <img src="{{ $d['main_image'] }}" class="item-height img-qc w-100"
                                                alt="certificate">
                                        </div>

                                        {{-- </a> --}}
                                        <div class="text-qc position-absolute bt-10">
                                            {{ $d['name'] }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!--/ Container end -->
            </section>
        </div>
        <!--/ Container end -->
    </section>
@endsection
@section('after-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/qc.css') }}">
@endsection
@section('after-scripts')
    <script src="{{ asset('template\slick-master\slick\slick.js') }}"></script>

    <script>
        $(function() {
            $(".center").slick({
                dots: true,
                infinite: true,
                centerMode: true,
                slidesToShow: 3,
                slidesToScroll: 1
            });
        });
    </script>
@endsection
