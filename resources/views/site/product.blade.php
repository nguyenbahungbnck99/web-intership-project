@extends('layouts.app')
@section('title-page', 'Danh sách sản phẩm')

@section('content')
    <section class="all-product">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3 class="section-title">
                        <h3 class="section-title">Tất cả sản phẩm trong danh mục của chúng tôi</h3>
                    </h3>
                </div>
            </div>
            <!--/ Title row end -->

            <div class="row">
                @foreach ($listProduct as $product)
                <div class="col-6 col-sm-4 col-md-2 mb-3">
                    <div class="box-product rounded">
                        <div class="box-image item-hot-product-image">
                            <a href="{{ route('detailProduct', $product['slug']) }}">
                                <img src="{{ $product['image'] }}"
                                    class=" w-100 item-height-image objec-fit-cover rounded" alt="sua">
                            </a>
                            <div class="detail">
                                <a href="{{ route('detailProduct', $product['slug']) }}"
                                    class="btn btn-success btn-sm">Chi
                                    tiết</a>
                            </div>
                        </div>
                        <div class="box-content text-center item-hot-product-content">
                            <div class="title-product title__product">
                                <a href="{{ route('detailProduct', $product['slug']) }}">

                                    {{ $product['name'] }}
                                </a>

                            </div>
                            <div class="price-product">
                                {{ number_format($product['price'], 0, '', '.') }} đ

                            </div>
                        </div>
                    </div>

                </div>
                @endforeach

            </div>
        </div>
        <!--/ Container end -->
    </section>
@endsection
