@extends('layouts.app')
@section('title-page', $product['name'])

@section('content')
    <section class="all-product">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3 class="section-title">
                        <h3 class="section-title">Thông tin sản phẩm</h3>
                    </h3>
                </div>
            </div>
            <!--/ Title row end -->

            <div class="row">

                <div class="col-12 col-sm-6 col-md-4">
                    <div class="box-image-product">
                        <img class="zoom-img" id="zoom_09" src="{{ $product['image'] }}"
                            data-zoom-image="{{ $product['image'] }}" class="w-100" />

                        <section class="regular slider" id="gallery_09">
                            <a href="#" class="elevatezoom-gallery active" data-update=""
                                data-image="{{ asset($product['image']) }}"
                                data-zoom-image="{{ asset($product['image']) }}">

                                <img src="{{ asset($product['image']) }}" width="80" />
                            </a>

                            @foreach ($product['product_image'] as $image)
                                <a href="#" class="elevatezoom-gallery" data-image="{{ asset($image['image']) }}"
                                    data-zoom-image="{{ asset($image['image']) }}"><img
                                        src="{{ asset($image['image']) }}" width="80" /></a>
                            @endforeach
                        </section>
                    </div>

                </div>
                <div class="col-12 col-sm-6 col-md-8">
                    <div class="box-content ">
                        <div class="title-product title-product-detail">
                            {{ $product['name'] }}
                        </div>
                        <div class="price-product-detail mt-3">
                            {{ number_format($product['price'], 0, '', '.') }} đ
                        </div>
                        <div class="description-product">
                            <div class="title-description  mt-3">
                                Công dụng
                            </div>
                            {!! $product['usefulness'] !!}
                        </div>
                    </div>

                </div>
                <div class="col-12">
                    <h3 class="section-title">Chi tiết sản phẩm</h3>
                    <div class="content">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active title-description" id="nav-detail-product"
                                    data-toggle="tab" href="#nav-detail" role="tab" aria-controls="nav-detail"
                                    aria-selected="true">Chi tiết
                                    sản phẩm</a>
                                <a class="nav-item nav-link title-description" id="nav-user-object" data-toggle="tab"
                                    href="#nav-objec" role="tab" aria-controls="nav-objec" aria-selected="false">Đối tượng
                                    sử dụng</a>
                                <a class="nav-item nav-link title-description" id="nav-user_manual" data-toggle="tab"
                                    href="#nav-manual" role="tab" aria-controls="nav-manual" aria-selected="false">Hướng dẫn
                                    sử dụng</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-detail" role="tabpanel"
                                aria-labelledby="nav-detail-product">
                                {!! $product['content'] !!}
                            </div>
                            <div class="tab-pane fade" id="nav-objec" role="tabpanel" aria-labelledby="nav-user-object">
                                {!! $product['user_object'] !!}

                            </div>
                            <div class="tab-pane fade" id="nav-manual" role="tabpanel" aria-labelledby="nav-user_manual">
                                {!! $product['user_manual'] !!}

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 d-flex">
                    @foreach ($product['product_image'] as $item)
                        <div class="item-height">
                            <img src="{{ $item['image'] }}" alt="{{ $product['image'] }}" class="object-fit-cover "
                                width="80">
                        </div>
                    @endforeach

                </div>

            </div>
        </div>
        <!--/ Container end -->
    </section>
    <section class="all-product">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3 class="section-title">
                        <h3 class="section-title">Sản phẩm liên quan</h3>
                    </h3>
                </div>
            </div>
            <!--/ Title row end -->

            <div class="row">
                @foreach ($productRelated as $item)
                    @if ($item['slug'] != null)
                        <div class="col-6 col-sm-4 col-md-2 mb-3">
                            <a href="{{ route('detailProduct', $item['slug']) }}">
                                <div class="box-product rounded">
                                    <div class="box-image ">
                                        <img src="{{ $item['image'] }}" class="img-fluid item-height" alt="sua">
                                        <div class="detail">
                                            <a href="{{ route('detailProduct', $item['slug']) }}"
                                                class="btn btn-success btn-sm">Chi tiết</a>
                                        </div>
                                    </div>
                                    <div class="box-content text-center">
                                        <div class="title-product title__product">
                                            {{ $item['name'] }}
                                        </div>
                                        <div class="price-product">
                                            {{ number_format($item['price'], 0, '', '.') }} đ
                                        </div>
                                    </div>
                                </div>
                            </a>

                        </div>
                    @endif
                @endforeach

            </div>
        </div>
        <!--/ Container end -->
    </section>
@endsection
@section('after-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('template\slick-master\slick\slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template\slick-master\slick\slick-theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">
    <style>
        .slider {
            width: 90%;
            margin: 0 auto;
        }

        .slick-list {
            padding: 0 !important;
            margin-left: 0px !important;
        }

        img#zoom_09 {
            width: 100%;
        }

        #gallery_09 a.active {
            border: 1px solid #7e33e0;
            border-radius: 4px;
        }

    </style>
@endsection
@section('after-scripts')
    <script src="{{ asset('template\slick-master\slick\slick.js') }}"></script>


    <script src="{{ asset('template\elevatezoom-plus-master\src\jquery.ez-plus.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(".regular").slick({
                dots: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3
            });
            console.log($(document).width());
            console.log($(window).width());
            if ($(document).width() > 576) {
                $("#zoom_09").ezPlus({
                    gallery: 'gallery_09',
                    cursor: 'pointer',
                    galleryActiveClass: "active",
                    // imageCrossfade: true,
                    // loadingIcon: "https://www.elevateweb.co.uk/spinner.gif"
                });
            }
        });
    </script>
@endsection
