<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Packet extends Model
{
    //
    protected $table = 'packet';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'image',
        'content',
        'price_from',
        'price_to',
        'status'
    ];
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/avatar_default.jpg');
        }
        return asset($image);
    }
    public function buyPacket()
    {
        return $this->HasMany(BuyPacket::class, 'packet_id');
    }
}
