<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;

class Contents extends Model
{
    protected $table = 'contents';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'user_id',
        'category_id',
        'title',
        'content',
        'status',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function listimage()
    {
        return $this->hasMany(ListImage::class, 'content_id');
    }
}
