<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListImage extends Model
{
    protected $table = 'list_image';
    protected $primaryKey = 'id';
    protected $fillable = [
        'content_id',
        'image',
    ];
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return null;
        }
        return asset($image);
    }
    public function content()
    {
        return $this->belongsTo(Contents::class, 'content_id');
    }
}
