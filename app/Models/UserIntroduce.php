<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class UserIntroduce extends Model
{
    //
    protected $table = 'user_introduce';
    protected $primaryKey = 'id';
    protected $fillable = [
        'by_user_id', 'to_user_id'
    ];
    public function byUser()
    {
        return $this->belongsTo(User::class, 'by_user_id');
    }
    public function toUser()
    {
        return $this->belongsTo(User::class, 'to_user_id');
    }
}
