<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Order extends Model
{
    //
    protected $table = 'order';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'name_user',
        'phone_order',
        'address_order',
        'total_product',
        'total_money_product',
        'content',
        'ship',
        'status'
    ];
  
    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function DetailOrder()
    {
        return $this->hasMany(DetailOrder::class, 'order_id');
    }
}
