<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFeel extends Model
{
    //
    protected $table = 'product_feel';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'avatar',
        'content',
        'product_id',
        'video'
    ];
    public function getAvatarAttribute($avatar)
    {
        if ($avatar == null) {
            return asset('/image_default/avatar_default.jpg');
        }
        return asset($avatar);
    }
}
