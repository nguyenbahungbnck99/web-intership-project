<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'product';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'price',
        'price_sale',
        'image',
        'description',
        'strategic_vision'
    ];
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/avatar_default.jpg');
        }
        return asset($image);
    }
    
    public function productFeel()
    {
        return $this->HasMany(ProductFeel::class, 'product_id');
    }
    public function relatedProduct()
    {
        return $this->HasMany(RelatedProduct::class, 'product_id');
    }
}
