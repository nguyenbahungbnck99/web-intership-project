<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    const URI_API = "http://intership-server.hanybu.com/api/";
    // const URI_API = "http://intershipproject.test/api/";
}
