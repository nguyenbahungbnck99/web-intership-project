<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\ControllerBase;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cookie;

class OrderController extends ControllerBase
{
    public function listOrder($i, Request $request)
    {
        // try {
            // dd($i);

            $token = Cookie::get('tokenvn24h');
            if (isset($token) && $token != null) {

                $from_date  = '';
                $to_date    = '';
                $status = '';

                if ($request->from_date) {
                    $from_date = date('Y-m-d', strtotime($request->from_date));
                } else {
                    $from_date  = date('Y-m-01');
                }
                if ($request->to_date) {
                    $to_date = date('Y-m-d', strtotime($request->to_date));
                } else {
                    $to_date = date('Y-m-t');
                }
                if ($request->status) {
                    $status = $request->status;
                }
                $name = $request->name;
                $sku = $request->sku;
                $client = new Client();
                $data = $client->get($this->urlAPI() . 'admin/order/list-order?page=' . $i .'&from_date='.$from_date.'&to_date='.$to_date.'&status='.$status.'&name='.$name.'sku='.$sku, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);
                // dd($response);
                if ($response['status'] == 1) {
                    $listOrder = $response['data']['data'];
                    $total_money = $response['data']['total_money'];
                    $total_order = $response['data']['total_order'];
                } else {
                    if ($response['code'] == 502) {
                        alert()->warning($response['message']);
                        AdminController::removeToken();
                    }

                    return redirect()->route('trang-chu');
                }
                // dd($listOrder);
                // return view('admin-old.includes.order-old.listOrder', compact('listOrder', 'total_order', 'total_money', 'from_date', 'to_date', 'status', 'name', 'sku'));
                return view('admin.order.listOrder', compact('listOrder', 'total_order', 'total_money', 'from_date', 'to_date', 'status', 'name', 'sku'));
            }

            return view('admin-old.includes.login');
        // } catch (\Throwable $th) {
        //     alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
        //     return back();
        // }
    }

    public function detailOrder($id)
    {
        try {
            // dd($i);

            $token = Cookie::get('tokenvn24h');
            if (isset($token) && $token != null) {
                # code...

                $client = new Client();
                $data = $client->get($this->urlAPI() . 'admin/order/detail-order/' . $id, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);
                // dd($response);
                if ($response['status'] == 1) {
                    $order = $response['data'];
                } else {
                    if ($response['code'] == 502) {
                        alert()->warning($response['message']);
                        AdminController::removeToken();
                    }

                    return redirect()->route('trang-chu');
                }
                // dd($listOrder);
                return view('admin-old.includes.order.detailOrder', compact('order'));
            }

            return view('admin-old.includes.login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }

    public function updateOrder(Request $request, $id)
    {
        // try {
            // dd(1);
            $ship = (int)str_replace([',', '.', ':', '\\', '/', '*'], '', $request->ship);
            $token = Cookie::get('tokenvn24h');
            $body = [
                'ship'    => $ship,
                'status'   => $request->status,
            ];
            $input = json_encode($body);

            $url = $this->urlAPI() . 'admin/order/update-order/'.$id;
            // dd($input);
            $client = new Client([
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/json'
                ],
            ]);
            $req = $client->put(
                $url,
                ['body' => $input]
            );

            $response = json_decode($req->getBody()->getContents(), true);
            //   dd($response);
            if ($response['status'] == 1) {

                alert()->success($response['message']);
                return redirect()->route('listOrder', ['i'=>1]);
            } else {
                if ($response['code'] == 502) {
                    alert()->warning($response['message']);
                    AdminController::removeToken();
                }

                alert()->warning($response['message']);
                return redirect()->route('detailOrder', ['id'=>$id]);

            }
        // } catch (\Throwable $th) {
        //     alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
        //     return back();
        // }
    }

    public function viewAddOrder()
    {
        try {
            // dd($i);

            $token = Cookie::get('tokenvn24h');
            if (isset($token) && $token != null) {
                # code...

                $client = new Client();
                $data = $client->get($this->urlAPI() . 'admin/product/list-all-product', [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);

                $data2 = $client->get($this->urlAPI() . 'admin/list-all-ctv', [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response2 = json_decode($data2->getBody()->getContents(), true);
                // dd($response);
                if ($response['status'] == 1 && $response2['status']==1) {
                    $allProduct = $response['data'];
                    $allCtv = $response2['data'];
                } else {
                    if ($response['code'] == 502) {
                        alert()->warning($response['message']);
                        AdminController::removeToken();
                    }

                    return redirect()->route('trang-chu');
                }
                // dd($listOrder);
                return view('admin-old.includes.order.addOrder', compact('allProduct', 'allCtv'));
            }

            return view('admin-old.includes.login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }


    public function addOrder(Request $request){
         dd($request->all());
    }

}
