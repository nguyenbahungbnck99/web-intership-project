<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\ControllerBase;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cookie;

class ContentController extends ControllerBase
{
    public function listCategory($i, Request $request)
    {
        // try {

        $token = Cookie::get('tokenvn24h');
        if (isset($token) && $token != null) {

            $client = new Client();
            $data = $client->get($this->urlAPI() . 'admin/content/list-category', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json',
                ],
            ]);
            $response = json_decode($data->getBody()->getContents(), true);
            // dd($response);
            if ($response['status'] == 1) {
                $listCategory = $response['data'];
            } else {
                if ($response['code'] == 502) {
                    alert()->warning($response['message']);
                    AdminController::removeToken();
                }

                return redirect()->route('trang-chu');
            }
            // dd($listOrder);
            return view('admin-old.includes.content.listCategory', compact('listCategory'));
        }

        return view('admin-old.includes.login');
        // } catch (\Throwable $th) {
        //     alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
        //     return back();
        // }
    }

    public function listContent($i, Request $request)
    {
        // try {

        $token = Cookie::get('tokenvn24h');
        if (isset($token) && $token != null) {

            $client = new Client();
            $data = $client->get($this->urlAPI() . 'admin/content/list-content', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json',
                ],
            ]);
            $response = json_decode($data->getBody()->getContents(), true);
            // dd($response);
            if ($response['status'] == 1) {
                $listContent = $response['data'];
            } else {
                if ($response['code'] == 502) {
                    alert()->warning($response['message']);
                    AdminController::removeToken();
                }

                return redirect()->route('trang-chu');
            }
            // dd($listOrder);
            return view('admin-old.includes.content.listContent', compact('listContent'));
        }

        return view('admin-old.includes.login');
        // } catch (\Throwable $th) {
        //     alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
        //     return back();
        // }
    }

    public function deleteContent($id)
    {
        try {

            $token = Cookie::get('tokenvn24h');

            if (isset($token) && $token != null) {
                # code...

                $client = new Client();
                $data = $client->delete($this->urlAPI() . 'admin/content/delete-content/' . $id, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);

                if ($response['status'] == 1) {
                    alert()->success($response['message']);
                    return redirect()->route('listContent', ['i' => 1]);
                } else {
                    alert()->warning($response['message']);
                    return back();
                }

                return redirect()->back();
            }

            return view('admin-old.includes.login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }

    public function addContent(Request $request){
        // try {
            $token = $request->cookie('tokenvn24h');
            $listImage = $request->file('image');
            if (isset($listImage) && $listImage != null) {
                foreach ($listImage as $key => $data) {
                    $image_path = $data->getPathname();
                    $image_mime = $data->getmimeType();
                    $image_org  = $data->getClientOriginalName();
                    $body[$key] = [
                        'name'   => 'image[]',
                        'filename' => $image_org,
                        'Mime-Type' => $image_mime,
                        'contents' => fopen($image_path, 'r'),
                    ];
                }
            }
            $length = count($body);
            $body[$length] = [
                'name'   => 'title',
                'contents' => $request->title,
            ];
            $body[$length+1] = [
                'name'   => 'category_id',
                'contents' => $request->category_id,
            ];
            $body[$length+2] = [
                'name'   => 'content',
                'contents' => $request->content,
            ];

            // dd($body);
                $input = json_encode($body);

                $url = $this->urlAPI() . 'admin/content/add-content';
                $client = new Client([
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token
                    ],
                ]);
                $req = $client->post(
                    $url,
                    [
                        'multipart' => $body


                    ],
                    // ['form_params']
                );
                $response = json_decode($req->getBody()->getContents(), true);
                // dd($response);
                if ($response['status'] == 0) {
                    alert()->warning($response['message']);
                    return back();
                }
                else {
                    alert()->success($response['message']);
                    $url = redirect()->route('listContent', ['i'=>1])->getTargetUrl();

                    return redirect($url);
                }
        // } catch (\Throwable $th) {
        //     //throw $th;
        // }
    }

    public function updateContent(Request $request, $id){
        // try {

            $token = $request->cookie('tokenvn24h');

            $body = [
                'title' => $request->title,
                'category_id'=> $request->category_id,
                'content'    => $request->content,
                'status'     => $request->status,
                'user_id'    => $request->user_id,
                '_method'    => 'put',
            ];
            // dd($body);

                $input = json_encode($body);

                $url = $this->urlAPI() . 'admin/content/update-content/'.$id;
                $client = new Client([
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token
                    ],
                ]);

                $req = $client->post($url,
                    ['body' => $input]
                );

                $response = json_decode($req->getBody()->getContents(), true);

                if ($response['status'] == 0) {
                    alert()->warning($response['message']);
                    return back();
                }
                else {
                    alert()->success($response['message']);
                    $url = redirect()->route('listContent', ['i'=>1])->getTargetUrl();

                    return redirect($url);
                }
        // } catch (\Throwable $th) {
        //     //throw $th;
        // }
    }

    public function viewUpdateContent($id)
    {
       // try {

        $token = Cookie::get('tokenvn24h');
        if (isset($token) && $token != null) {

            $client = new Client();
            $data = $client->get($this->urlAPI() . 'admin/content/list-all-category', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json',
                ],
            ]);
            $response = json_decode($data->getBody()->getContents(), true);

            $data2 = $client->get($this->urlAPI() . 'admin/content/detail-content/'.$id, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json',
                ],
            ]);
            $response2 = json_decode($data2->getBody()->getContents(), true);
            // dd($response);
            if ($response['status'] == 1 && $response2['status']==1) {
                $listCategory = $response['data'];
                $content = $response2['data'];
            } else {
                if ($response['code'] == 502) {
                    alert()->warning($response['message']);
                    AdminController::removeToken();
                }

                return redirect()->route('trang-chu');
            }
            // dd($listOrder);
            return view('admin-old.includes.content.updateContent', compact('listCategory', 'content'));
        }

        return view('admin-old.includes.login');
        // } catch (\Throwable $th) {
        //     alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
        //     return back();
        // }
    }

    public function detailContent($id)
    {
       // try {

        $token = Cookie::get('tokenvn24h');
        if (isset($token) && $token != null) {

            $client = new Client();

            $data2 = $client->get($this->urlAPI() . 'admin/content/detail-content/'.$id, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json',
                ],
            ]);
            $response2 = json_decode($data2->getBody()->getContents(), true);
            // dd($response);
            if ( $response2['status']==1) {
                $content = $response2['data'];
            } else {
                if ($response2['code'] == 502) {
                    alert()->warning($response2['message']);
                    AdminController::removeToken();
                }

                return redirect()->route('trang-chu');
            }
            // dd($listOrder);
            return view('admin-old.includes.content.detailContent', compact('content'));
        }

        return view('admin-old.includes.login');
        // } catch (\Throwable $th) {
        //     alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
        //     return back();
        // }
    }




    public function viewCreateContent()
    {
       // try {

        $token = Cookie::get('tokenvn24h');
        if (isset($token) && $token != null) {

            $client = new Client();
            $data = $client->get($this->urlAPI() . 'admin/content/list-all-category', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json',
                ],
            ]);
            $response = json_decode($data->getBody()->getContents(), true);
            // dd($response);
            if ($response['status'] == 1) {
                $listCategory = $response['data'];
            } else {
                if ($response['code'] == 502) {
                    alert()->warning($response['message']);
                    AdminController::removeToken();
                }

                return redirect()->route('trang-chu');
            }
            // dd($listOrder);
            return view('admin-old.includes.content.createContent', compact('listCategory'));
        }

        return view('admin-old.includes.login');
        // } catch (\Throwable $th) {
        //     alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
        //     return back();
        // }
    }

    public function addCategory(Request $request)
    {
        try {

            $token = Cookie::get('tokenvn24h');
            if (isset($token) && $token != null) {
                $body = [
                    'name'         => $request->name,
                ];
                $input = json_encode($body);

                $url = $this->urlAPI() . 'admin/content/add-category';
                // dd($input);
                $client = new Client([
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Content-Type' => 'application/json'
                    ],
                ]);
                $req = $client->post(
                    $url,
                    ['body' => $input]
                );

                $response = json_decode($req->getBody()->getContents(), true);

                if ($response['status'] == 1) {

                    alert()->success($response['message']);
                    return redirect()->route('listCategory', ['i'=>1]);
                } else {
                    if ($response['code'] == 502) {
                        alert()->warning($response['message']);
                        AdminController::removeToken();
                    }

                    return redirect()->route('trang-chu');
                }
            }
            return redirect()->route('login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }

    public function updateCategory(Request $request, $id){
        // try {

            $token = $request->cookie('tokenvn24h');

            $body = [
                'name' => $request->name,
                '_method'    => 'put',
            ];

                $input = json_encode($body);

                $url = $this->urlAPI() . 'admin/content/update-category/'.$id;
                $client = new Client([
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token
                    ],
                ]);

                $req = $client->post($url,
                    ['body' => $input]
                );

                $response = json_decode($req->getBody()->getContents(), true);

                if ($response['status'] == 0) {
                    alert()->warning($response['message']);
                    return back();
                }
                else {
                    alert()->success($response['message']);
                    $url = redirect()->route('listCategory', ['i'=>1])->getTargetUrl();

                    return redirect($url);
                }
        // } catch (\Throwable $th) {
        //     //throw $th;
        // }
    }

    public function deleteCategory($id)
    {
        try {

            $token = Cookie::get('tokenvn24h');

            if (isset($token) && $token != null) {
                # code...

                $client = new Client();
                $data = $client->delete($this->urlAPI() . 'admin/content/delete-category/' . $id, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);

                if ($response['status'] == 1) {
                    alert()->success($response['message']);
                    return redirect()->route('listCategory', ['i' => 1]);
                } else {
                    alert()->warning($response['message']);
                    return back();
                }

                return redirect()->back();
            }

            return view('admin-old.includes.login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }
}
