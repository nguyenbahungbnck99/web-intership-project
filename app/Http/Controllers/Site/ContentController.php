<?php

namespace App\Http\Controllers\Site;

use App\Helpers\Helpers;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;

class ContentController extends ControllerBase
{
    //
    public function categoryContent($category_slug)
    {
        $client = new Client();

        $data = $client->get($this->urlAPI() . 'listContentByCategorySlug/' . $category_slug, [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $dataContent = $response['data'];
        // dd($response);

        return view('site.post', compact('dataContent'));
    }
    public function detailContent($content_slug)
    {
        $client = new Client();

        $data = $client->get($this->urlAPI() . 'detailContent/' . $content_slug, [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $content = $response['data'];
        $information = [
            'url' => route('detailProduct',$content_slug),
            'title' => $content['content']['title'],
            'description' => $content['content']['title'],
            'image' => Helpers::imageHome(),
        ];
        return view('site.detailPost', compact('content','information'));

    }
}
