<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class QcController extends ControllerBase
{
    //
    public function index(Request $request){
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'list-advertisement', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $data = $response['data'];
        // dd($data);
        if ($response['status'] == 1) {
            return view('site.qc', compact('data'));
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    public function detailQc($id){
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'advertisement-detail/' . $id, [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $data = $response['data'];
        if ($response['status'] == 1) {
            return view('site.detail-qc', compact('data'));
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
}
