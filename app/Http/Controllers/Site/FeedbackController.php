<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class FeedbackController extends ControllerBase
{
    //
    public function index(Request $request){
        $client = new Client();

        $data = $client->get($this->urlAPI() . 'feedbackHot', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $feedbackHot = $response['data'];
        $data = $client->get($this->urlAPI() . 'listFeedback', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $listFeedback = $response['data'];
        return view('site.feedback',compact('feedbackHot','listFeedback'));
    }
}
