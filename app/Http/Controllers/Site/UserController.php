<?php

namespace App\Http\Controllers\Site;

use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class UserController extends ControllerBase
{
    //
    public function login(Request $request)
    {
        if ($request->all()) {
            try {
                $body = [
                    'phone' => $request->phone,
                    'password' => $request->password,
                    'device_id' => $request->device_id,
                ];
                // dd($body);
                $input = json_encode($body);
                $url = $this->urlAPI() . 'site/login';
                $client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                ]);
                $req = $client->post(
                    $url,
                    ['body' => $input]
                );

                $response = json_decode($req->getBody()->getContents(), true);
                if ($response['status'] == 1) {
                    Cookie::queue("tokenLoginCustomer", $response['data']['token'], 1440);
                    Cookie::queue("name_customer", $response['data']['user']['name'], 1440);
                    alert()->success($response['message']);
                    return redirect()->route('home');
                } else {
                    alert()->error($response['message']);
                    return back();
                }
            } catch (\Throwable $th) {
                alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
                return back();
            }
        }
        return view('site.login');
    }
    public function register(Request $request)
    {
        if ($request->all()) {
            try {
                $dataSend = Helpers::dataSend($request);
                $url = $this->urlAPI() . 'site/register';
                $client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                ]);
                $req = $client->post(
                    $url,
                    ['multipart' => $dataSend]
                );

                $response = json_decode($req->getBody()->getContents(), true);
                if ($response['status'] == 1) {
                    alert()->success($response['message']);
                    return redirect()->route('home');
                } else {
                    alert()->error($response['message'], '');
                    return back();
                }
            } catch (\Throwable $th) {
                alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
                return back();
            }
        }
        return view('site.register');
    }
    public function logout()
    {
        try {
            $token = Cookie::get('tokenLoginCustomer');
            $client = new Client();
            $data = $client->post($this->urlAPI() . 'site/logout', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json',
                ],
            ]);
            $response = json_decode($data->getBody()->getContents(), true);
            if ($response['status'] == 1) {
                Cookie::queue(Cookie::forget('tokenLoginCustomer'));
                Cookie::queue(Cookie::forget('name_customer'));
                return redirect()->route('home');
            }

            return redirect()->route('home');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }
}
