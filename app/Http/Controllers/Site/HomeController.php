<?php

namespace App\Http\Controllers\Site;

use App\Helpers\Helpers;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;

class HomeController extends ControllerBase
{
    //
    public function index()
    {

        $client = new Client();
        $data = $client->get($this->urlAPI() . 'get-new-product', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $newProduct = $response['data'];
        $data = $client->get($this->urlAPI() . 'list-hot-product', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $listHotProduct = $response['data'];

        $data = $client->get($this->urlAPI() . 'list-all-product', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $listAllProduct = $response;



        $information = [
            'url' => route('home'),
            'title' => Helpers::title_home,
            'description' => Helpers::description_home,
            'image' => Helpers::imageHome(),
        ];
        return view('site.home', compact(
            'listHotProduct',
            'newProduct',
            'listAllProduct',
            'information'
        ));
    }
    public function page($page_id)
    {

        $client = new Client();
        $data = $client->get($this->urlAPI() . 'get-new-product', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $newProduct = $response['data'];
        $data = $client->get($this->urlAPI() . 'list-hot-product', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $listHotProduct = $response['data'];

        $data = $client->get($this->urlAPI() . 'list-all-product', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $listAllProduct = $response['data'];

        $data = $client->get($this->urlAPI() . 'listProductMilk', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        // dd($response);

        $listProductMilk = $response['data'];

        $data = $client->get($this->urlAPI() . 'listProductFunctionalFoods', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $listProductFunctionalFoods = $response['data'];

        $data = $client->get($this->urlAPI() . "listProductAll?page=$page_id", [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $listProductAll = $response['data'];
        // dd($listProductMilk);
        return view('site.home', compact('listHotProduct', 'newProduct', 'listAllProduct', 'listProductFunctionalFoods', 'listProductMilk', 'listProductAll'));
    }
    public function listFile()
    {
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'list-category-file', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $data = $response['data'];
        // dd($data);
        if ($response['status'] == 1) {
            return view('site.file', compact('data'));
        } else {
            alert()->error($response['message'], '');
            return back();
        }

    }
    public function listVideo()
    {
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'list-video', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $data = $response['data'];
        // dd($data);
        if ($response['status'] == 1) {
            return view('site.video', compact('data'));
        } else {
            alert()->error($response['message'], '');
            return back();
        }

    }
}
