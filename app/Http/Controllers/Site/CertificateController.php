<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CertificateController extends ControllerBase
{
    //
    public function index(){
        $client = new Client();

        $data = $client->get($this->urlAPI() . 'getTwoCertificate', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $twoCertificate = $response['data'];
        // dd($twoCertificate);
        $data = $client->get($this->urlAPI() . 'listCertificate', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $listCertificate = $response['data'];
        return view('site.certificate',compact('twoCertificate','listCertificate'));
    }
    public function detailCertificate($certificate_slug){
        $client = new Client();

        $data = $client->get($this->urlAPI() . 'detailCertificate/'.$certificate_slug, [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $certificate = $response['data'];
        // dd($certificate);
        return view('site.detail-certificate',compact('certificate'));
    }
}
