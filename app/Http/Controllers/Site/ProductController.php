<?php

namespace App\Http\Controllers\Site;

use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ProductController extends ControllerBase
{
    //
    public function listProductByCategory($slug)
    {
        $client = new Client();

        $data = $client->get($this->urlAPI() . 'list-product-by-category/'.$slug, [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $listProduct = $response['data'];
        // dd($listProduct);
        return view('site.product', compact('listProduct'));

    }
    public function search(Request $request )
    {
        $client = new Client();

        $data = $client->get($this->urlAPI() . "search?key_search=$request->key_search", [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $listProduct = $response['data'];
        return view('site.product', compact('listProduct'));

    }
    public function productDetail($slug)
    {
        $client = new Client();

        $data = $client->get($this->urlAPI() . 'product-detail-by-slug/'.$slug, [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $product = $response['data'];
        // dd($product);
        $data = $client->get($this->urlAPI() . 'getProductRelated/'.$product['id'], [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        // dd($product);
        $response = json_decode($data->getBody()->getContents(), true);
        $productRelated = $response['data'];
        // dd($product);
        $information = [
            'url' => route('detailProduct',$product['slug']),
            'title' => $product['name'],
            'description' => $product['name'],
            'image' => $product['image'],
        ];

        return view('site.detailProduct', compact('product','productRelated','information'));

    }
}
