<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class AdvController extends ControllerBase
{
    public function index(Request $request)
    {
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . "admin/advertisement/list-advertisement", [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $responseData = $response['data'];
        return view('admin.advertisement.index', compact('responseData'));
    }
    public function imageAttribute($image, $name)
    {
        $image_org = $image->getClientOriginalName();
        $image_mime = $image->getmimeType();
        $image_path = $image->getPathname();
        $arr = [
            'name' => $name,
            'filename' => $image_org,
            'Mime-Type' => $image_mime,
            'contents' => fopen($image_path, 'r'),
        ];
        return $arr;
    }
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $token = $request->cookie('tokenLogin');

        $adv = [];
        foreach ($data as $key => $value) {
            if ($key == 'main_image') {

                if ($request->hasFile('main_image')) {
                    $adv[] = $this->imageAttribute($request->main_image, 'main_image');

                }
            } elseif ($key == 'list_image') {
                if ($request->hasFile('list_image')) {
                    foreach ($request->list_image as $image) {

                        $adv[] = $this->imageAttribute($image, 'list_image[]');

                    }
                }
            } else {
                $adv[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }
        // dd($adv);
        $url = $this->urlAPI() . 'admin/advertisement/create-advertisement';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $adv,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return back();
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    public function edit($id)
    {
        //
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'admin/advertisement/advertisement-detail/' . $id, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        if ($response['status'] == 1) {
            $category_file = $response['data'];
            return [
                'data' => $category_file,
                'code' => 200,
                'action' => route('admin.advertisement.update', $id),
            ];
        } else {
            return [
                'code' => 500,
            ];
        }
    }
    public function update(Request $request, $id)
    {
        //
        $data = $request->all();

        $token = $request->cookie('tokenLogin');

        $adv = [];
        foreach ($data as $key => $value) {
            if ($key == 'main_image') {

                if ($request->hasFile('main_image')) {
                    $adv[] = $this->imageAttribute($request->main_image, 'main_image');

                }
            } elseif ($key == 'list_image') {
                if ($request->hasFile('list_image')) {
                    foreach ($request->list_image as $image) {

                        $adv[] = $this->imageAttribute($image, 'list_image[]');

                    }
                }
            } else {
                $adv[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }
        $adv [] = [
            'name' => '_method',
            'contents' => 'put',
        ];
        $url = $this->urlAPI() . 'admin/advertisement/update-advertisement/' . $id;
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $adv,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return back();
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    public function delete($id)
    {
        try {

            $token = Cookie::get('tokenLogin');
            if (!empty($token)) {
                # code...

                $client = new Client();
                $data = $client->delete($this->urlAPI() . 'admin/advertisement/delete-advertisement/' . $id, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);
                // dd($response);
                if ($response['status'] == 1) {
                    alert()->success($response['message']);
                    return back();
                } else {
                    alert()->warning($response['message']);
                    return back();
                }

                return redirect()->back();
            }

            return redirect()->route('login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }
}
