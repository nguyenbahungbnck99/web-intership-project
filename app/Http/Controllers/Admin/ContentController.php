<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class ContentController extends ControllerBase
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function page($page_id)
    {
        $token = Cookie::get('tokenLogin');

        $client = new Client();
        $data = $client->get($this->urlAPI() . "admin/content?page=$page_id", [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $responseData = $response['data'];
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'admin/categoryContent/listAll', [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $category_contents = $response['data'];
        return view('admin.content.index', compact('responseData', 'category_contents'));
    }
    public function index(Request $request)
    {
        //

        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . "admin/content?title=$request->title&category_id=$request->category_id", [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $responseData = $response['data'];
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'admin/categoryContent/listAll', [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $category_contents = $response['data'];
        return view('admin.content.index', compact('responseData', 'category_contents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
        try {
            //code...
            $token = Cookie::get('tokenLogin');

            $client = new Client();
            $data = $client->get($this->urlAPI() . 'admin/categoryContent/listAll', [
                'headers' => [
                    'Authorization' => 'Bearer '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            $response = json_decode($data->getBody()->getContents(), true);

            if ($response['status'] == 0) {
                alert()->success($response['message']);

                return redirect()->route('login');
            }
            $category_contents = $response['data'];
            return view('admin.content.addProduct', compact('category_contents'));
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->route('login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $token = $request->cookie('tokenLogin');

        $dataSend = $this->dataSend($request);
        $url = $this->urlAPI() . 'admin/content/create';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $dataSend,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.content.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    public function storeAddInCategory(Request $request, $category_id)
    {
        $token = $request->cookie('tokenLogin');
        $dataSend = $this->dataSend($request);

        $url = $this->urlAPI() . 'admin/content/add-product';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $dataSend,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.content.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $token = Cookie::get('tokenLogin');

        $client = new Client();

        $data = $client->get($this->urlAPI() . 'admin/content/edit/' . $id, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $product = $response['data'];

        $client = new Client();
        $data = $client->get($this->urlAPI() . 'list-category-product', [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $category_contents = $response['data'];
        return view('admin.content.edit', compact('product', 'category_contents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public  function dataSend($request)
    {
        $dataSend = [];
        foreach ($request->all() as $key => $value) {
            if ($key == 'image') {
                if ($request->hasFile('image')) {
                    foreach ($request->image as $item) {

                        $dataSend[] = Helpers::imageAttribute($item, 'image[]');
                    }
                }
            } else {
                $dataSend[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }
        return $dataSend;
    }
    public function update(Request $request, $product_id)
    {
        $token = $request->cookie('tokenLogin');

        $dataSend = $this->dataSend($request);


        $url = $this->urlAPI() . 'admin/content/update/' . $product_id;
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $dataSend,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.content.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        try {

            $token = Cookie::get('tokenLogin');
            if (!empty($token)) {
                # code...

                $client = new Client();
                $data = $client->delete($this->urlAPI() . 'admin/content/delete/' . $id, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);
                // dd($response);
                if ($response['status'] == 1) {
                    alert()->success($response['message']);
                    return redirect()->route('admin.content.index');
                } else {
                    alert()->warning($response['message']);
                    return back();
                }

                return redirect()->back();
            }

            return redirect()->route('login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }
}
