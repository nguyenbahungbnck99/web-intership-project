<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helpers;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class FileController extends ControllerBase
{
    public function index(Request $request)
    {
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . "admin/category-file/list-category-file", [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $responseData = $response['data'];
        return view('admin.file.index', compact('responseData'));
    }

    public function listFile($category_file_id){
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . "admin/file/list-file/" . $category_file_id, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $responseData = $response['data'];
        return view('admin.file.list_file', compact('responseData', 'category_file_id'));
    }
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $token = $request->cookie('tokenLogin');

        $category_file = [];
        foreach ($data as $key => $value) {
            if ($key == 'image') {

                if ($request->hasFile('image')) {
                    $category_file[] = Helpers::imageAttribute($request->image, 'image');

                }

            } else {
                $category_file[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }

        $url = $this->urlAPI() . 'admin/category-file/create-category-file';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $category_file,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.file.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    public function storeFile(Request $request)
    {
        //
        $data = $request->all();
        $token = $request->cookie('tokenLogin');

        $category_file = [];
        foreach ($data as $key => $value) {
            if ($key == 'image') {
                if ($request->hasFile('image')) {
                    $category_file[] = Helpers::imageAttribute($request->image, 'image');
                }
            }else if($key == 'file'){
                if ($request->hasFile('file')) {
                    $category_file[] = Helpers::imageAttribute($request->file, 'file');
                }
            }else {
                $category_file[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }

        $url = $this->urlAPI() . 'admin/file/create-file';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $category_file,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return back();
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    public function edit($id)
    {
        //
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'admin/category-file/category-file-detail/' . $id, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        if ($response['status'] == 1) {
            $category_file = $response['data'];
            return [
                'data' => $category_file,
                'code' => 200,
                'action' => route('admin.file.update', $id),
            ];
        } else {
            return [
                'code' => 500,
            ];
        }
    }
    public function editFile($id)
    {
        //
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'admin/file/file-detail/' . $id, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        if ($response['status'] == 1) {
            $file = $response['data'];
            return [
                'data' => $file,
                'code' => 200,
                'action' => route('admin.file.update_file', $id),
            ];
        } else {
            return [
                'code' => 500,
            ];
        }
    }
    public function update(Request $request, $id)
    {
        //
        $data = $request->all();

        $token = $request->cookie('tokenLogin');

        $category_file = [];
        foreach ($data as $key => $value) {
            if ($key == 'image') {

                if ($request->hasFile('image')) {
                    $category_file[] = Helpers::imageAttribute($request->image, 'image');

                }

            } else {
                $category_file[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }

        $url = $this->urlAPI() . 'admin/category-file/update-category-file/' . $id;
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $category_file,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.file.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    public function updateFile(Request $request, $id)
    {
        //
        $data = $request->all();

        $token = $request->cookie('tokenLogin');

        $category_file = [];
        foreach ($data as $key => $value) {
            if ($key == 'image') {

                if ($request->hasFile('image')) {
                    $category_file[] = Helpers::imageAttribute($request->image, 'image');

                }

            } else if ($key == 'file') {

                if ($request->hasFile('file')) {
                    $category_file[] = Helpers::imageAttribute($request->file, 'file');

                }

            } else {
                $category_file[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }

        $url = $this->urlAPI() . 'admin/file/update-file/' . $id;
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $category_file,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return back();
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    public function delete($id)
    {
        try {

            $token = Cookie::get('tokenLogin');
            if (!empty($token)) {
                # code...

                $client = new Client();
                $data = $client->delete($this->urlAPI() . 'admin/category-file/delete-category-file/' . $id, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);
                // dd($response);
                if ($response['status'] == 1) {
                    alert()->success($response['message']);
                    return redirect()->route('admin.file.index');
                } else {
                    alert()->warning($response['message']);
                    return back();
                }

                return redirect()->back();
            }

            return redirect()->route('login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }
    public function deleteFile($id)
    {
        try {

            $token = Cookie::get('tokenLogin');
            if (!empty($token)) {
                # code...

                $client = new Client();
                $data = $client->delete($this->urlAPI() . 'admin/file/delete-file/' . $id, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);
                // dd($response);
                if ($response['status'] == 1) {
                    alert()->success($response['message']);
                    return back();
                } else {
                    alert()->warning($response['message']);
                    return back();
                }

                return redirect()->back();
            }

            return redirect()->route('login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }
}
