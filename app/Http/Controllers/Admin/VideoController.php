<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class VideoController extends ControllerBase
{
    public function index(Request $request)
    {
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . "admin/video/list-video", [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $responseData = $response['data'];
        return view('admin.video.index', compact('responseData'));
    }
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $token = $request->cookie('tokenLogin');

        $category_file = [];
        foreach ($data as $key => $value) {
            $category_file[] = [
                'name' => $key,
                'contents' => $value,
            ];
        }

        $url = $this->urlAPI() . 'admin/video/create-video';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $category_file,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return back();
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    public function edit($id)
    {
        //
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'admin/video/video-detail/' . $id, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        if ($response['status'] == 1) {
            $category_file = $response['data'];
            return [
                'data' => $category_file,
                'code' => 200,
                'action' => route('admin.video.update', $id),
            ];
        } else {
            return [
                'code' => 500,
            ];
        }
    }
    public function update(Request $request, $id)
    {
        //
        $data = $request->all();

        $token = $request->cookie('tokenLogin');

        $category_file = [];
        foreach ($data as $key => $value) {
                $category_file[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
        }

        $url = $this->urlAPI() . 'admin/video/update-video/' . $id;
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $category_file,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return back();
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    public function delete($id)
    {
        try {

            $token = Cookie::get('tokenLogin');
            if (!empty($token)) {
                # code...

                $client = new Client();
                $data = $client->delete($this->urlAPI() . 'admin/video/delete-video/' . $id, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);
                // dd($response);
                if ($response['status'] == 1) {
                    alert()->success($response['message']);
                    return back();
                } else {
                    alert()->warning($response['message']);
                    return back();
                }

                return redirect()->back();
            }

            return redirect()->route('login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }
}
