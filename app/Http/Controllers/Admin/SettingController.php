<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class SettingController extends ControllerBase
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // try {

        $token = Cookie::get('tokenLogin');
        if (!$token) {
            alert()->error('Tài khoản đã hết phiên đăng nhập');

            return redirect()->route('admin.login');
        }
        $client = new Client();
        $data = $client->get($this->urlAPI() . "admin/setting", [
            'headers' => [
                'Authorization' => 'Bearer ' .  $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $infor = $response['data'];

        return view('admin.setting.index', compact(
            'infor',
        ));
        // } catch (\Throwable $th) {
        //     alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
        //     return back();
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $token = $request->cookie('tokenLogin');
            if (!$token) {
                alert()->error('Tài khoản đã hết phiên đăng nhập');
                return redirect()->route('admin.login');
            }
            $dataSend = Helpers::dataSend($request);
            $url = $this->urlAPI() . 'admin/setting/store';
            $client = new Client([
                'headers' => [
                    'Authorization' => 'Bearer ' .  $token,
                    'Content-Type' => 'application/json',
                ],
            ]);
            $req = $client->post(
                $url,
                [
                    'multipart' => $dataSend,

                ],
                // ['form_params']
            );
            $response = json_decode($req->getBody()->getContents(), true);
            if ($response['status'] == 1) {

                alert()->success($response['message']);
                return redirect()->route('admin.setting.index');
            } else {
                alert()->error($response['message']);
                return back();
            }
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
