<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helpers;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class SliderController extends ControllerBase
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . "admin/banner", [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $responseData = $response['data'];
        return view('admin.slider.index', compact('responseData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();

        $token = $request->cookie('tokenLogin');

        $slider = [];
        foreach ($data as $key => $value) {
            if ($key == 'image') {

                if ($request->hasFile('image')) {
                    $slider[] = Helpers::imageAttribute($request->image, 'image');

                }

            } else {
                $slider[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }

        $url = $this->urlAPI() . 'admin/banner/create';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $slider,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.slider.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'admin/banner/edit/' . $id, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        if ($response['status'] == 1) {
            $slider = $response['data'];
            return [
                'data' => $slider,
                'code' => 200,
                'action' => route('admin.slider.update', $id),
            ];
        } else {
            return [
                'code' => 500,
            ];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $request->all();

        $token = $request->cookie('tokenLogin');

        $slider = [];
        foreach ($data as $key => $value) {
            if ($key == 'image') {

                if ($request->hasFile('image')) {
                    $slider[] = Helpers::imageAttribute($request->image, 'image');

                }

            } else {
                $slider[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }

        $url = $this->urlAPI() . 'admin/banner/update/' . $id;
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $slider,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.slider.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    public function active(Request $request, $id)
    {
        //
        $data = $request->all();

        $token = $request->cookie('tokenLogin');

        $body = [
            'is_active' => $request->is_active,
        ];
        // dd($body);
        $input = json_encode($body);
        $url = $this->urlAPI() . 'admin/banner/active/' . $id;
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->put(
            $url,
            ['body' => $input]
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.slider.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        try {

            $token = Cookie::get('tokenLogin');
            if (!empty($token)) {
                # code...

                $client = new Client();
                $data = $client->delete($this->urlAPI() . 'admin/banner/delete/' . $id, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);
                // dd($response);
                if ($response['status'] == 1) {
                    alert()->success($response['message']);
                    return redirect()->route('admin.slider.index');
                } else {
                    alert()->warning($response['message']);
                    return back();
                }

                return redirect()->back();
            }

            return redirect()->route('login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }
}
