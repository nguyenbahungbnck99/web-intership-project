<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helpers;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class ProductController extends ControllerBase
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function page($page_id)
    {
        $token = Cookie::get('tokenLogin');

        $client = new Client();
        $data = $client->get($this->urlAPI() . "admin/product?page=$page_id", [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $responseData = $response['data'];
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'admin/categoryProduct/listAll', [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $category_products = $response['data'];
        return view('admin.product.index', compact('responseData', 'category_products'));
    }
    public function index(Request $request)
    {
        //

        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . "admin/product?name=$request->name&category_id=$request->category_id", [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $responseData = $response['data'];
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'admin/categoryProduct/listAll', [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $category_products = $response['data'];
        return view('admin.product.index', compact('responseData', 'category_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
        try {
            //code...
            $token = Cookie::get('tokenLogin');

            $client = new Client();
            $data = $client->get($this->urlAPI() . 'admin/categoryProduct/listAll', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json',
                ],
            ]);
            $response = json_decode($data->getBody()->getContents(), true);
            if ($response['status'] == 0) {
                alert()->success($response['message']);

                return redirect()->route('login');
            }
            $category_products = $response['data'];
            return view('admin.product.addProduct', compact('category_products'));
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->route('login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        try {
            //code...
            $token = $request->cookie('tokenLogin');

            $dataSend = Helpers::dataSend($request);
            $url = $this->urlAPI() . 'admin/product/create';
            $client = new Client([
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/json',
                ],
            ]);
            $req = $client->post(
                $url,
                [
                    'multipart' => $dataSend,

                ],
                // ['form_params']
            );

            $response = json_decode($req->getBody()->getContents(), true);

            if ($response['status'] == 1) {

                alert()->success($response['message']);
                return redirect()->route('admin.product.index');
            } else {
                alert()->error($response['message'], '');
                return back();
            }
        } catch (\Throwable $th) {
            alert()->error($th->getMessage());
            return back();

        }
    }
    public function storeAddInCategory(Request $request, $category_id)
    {
        $token = $request->cookie('tokenLogin');
        $data = $request->all();
        $dataSend['category_id'] = $category_id;
        $dataSend = Helpers::dataSend($request);

        $url = $this->urlAPI() . 'admin/product/add-product';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $dataSend,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.product.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $token = Cookie::get('tokenLogin');

        $client = new Client();

        $data = $client->get($this->urlAPI() . 'admin/product/edit/' . $id, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $product = $response['data'];

        $client = new Client();
        $data = $client->get($this->urlAPI() . 'list-category-product', [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $category_products = $response['data'];
        return view('admin.product.edit', compact('product', 'category_products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product_id)
    {
        $token = $request->cookie('tokenLogin');

        $dataSend = Helpers::dataSend($request);


        $url = $this->urlAPI() . 'admin/product/update/' . $product_id;
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $dataSend,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.product.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        try {

            $token = Cookie::get('tokenLogin');
            if (!empty($token)) {
                # code...

                $client = new Client();
                $data = $client->delete($this->urlAPI() . 'admin/product/delete/' . $id, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);
                // dd($response);
                if ($response['status'] == 1) {
                    alert()->success($response['message']);
                    return redirect()->route('admin.product.index');
                } else {
                    alert()->warning($response['message']);
                    return back();
                }

                return redirect()->back();
            }

            return redirect()->route('login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }
}
