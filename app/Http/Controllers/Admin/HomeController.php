<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class HomeController extends ControllerBase
{
    //
    public function index()
    {
        //
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . "admin/categoryProduct", [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        if ($response['status'] == 1) {
            return view('admin.dashboard.index');
        }else{
            alert()->warning($response['message']);
            return redirect()->route('login');
        }

    }
}
