<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CategoryContentController extends ControllerBase
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // try {
        $token = Cookie::get('tokenLogin');
        if (isset($token) && $token != null) {

            $client = new Client();
            $data = $client->get($this->urlAPI() . 'admin/categoryContent', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json',
                ],
            ]);
            $response = json_decode($data->getBody()->getContents(), true);
            $responseData = $response['data'];
            return view('admin.categoryContent.index2', compact('responseData'));
        }
        // } catch (\Throwable $th) {
        //     alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
        //     return back();
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $token = Cookie::get('tokenLogin');
            if (isset($token) && $token != null) {
                $dataSend = Helpers::dataSend($request);


                $url = $this->urlAPI() . 'admin/categoryContent/create';
                $client = new Client([
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Content-Type' => 'application/json',
                    ],
                ]);
                $req = $client->post(
                    $url,
                    [
                        'multipart' => $dataSend,

                    ],
                    // ['form_params']
                );
                $response = json_decode($req->getBody()->getContents(), true);

                alert()->success($response['message']);
                return redirect()->route('admin.categoryContent.index');
            }
            return redirect()->route('login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try {
            $token = Cookie::get('tokenLogin');
            $client = new Client();
            $data = $client->get($this->urlAPI() . 'admin/categoryContent/edit/' . $id, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json',
                ],
            ]);
            $response = json_decode($data->getBody()->getContents(), true);
            alert()->success($response['message']);
            return redirect()->route('admin.categoryContent.index');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $token = $request->cookie('tokenLogin');

            $dataSend = Helpers::dataSend($request);


            $url = $this->urlAPI() . 'admin/categoryContent/update/' . $id;
            $client = new Client([
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/json',
                ],
            ]);
            $req = $client->post(
                $url,
                [
                    'multipart' => $dataSend,

                ],
                // ['form_params']
            );

            $response = json_decode($req->getBody()->getContents(), true);
            alert()->success($response['message']);
            return redirect()->route('admin.categoryContent.index');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $token = Cookie::get('tokenLogin');
            if (!empty($token)) {
                # code...

                $client = new Client();
                $data = $client->delete($this->urlAPI() . 'admin/categoryContent/delete/' . $id, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);
                alert()->success($response['message']);
                return redirect()->route('admin.categoryContent.index');
            }

            return redirect()->route('login');
        } catch (\Throwable $th) {
            alert($th)->error('Sản phẩm đang có trong danh mục không thể xóa');
            return back();
        }
    }
}
