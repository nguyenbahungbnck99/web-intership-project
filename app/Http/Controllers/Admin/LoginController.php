<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class LoginController extends ControllerBase
{
    //
    public function checkLogin(Request $request)
    {
        // try {
        $body = [
            'phone' => $request->phone,
            'password' => $request->password,
            'device_id' => $request->device_id,
        ];
        // dd($body);
        $input = json_encode($body);
        $url = $this->urlAPI() . 'admin/login';
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json'],
        ]);
        $req = $client->post(
            $url,
            ['body' => $input]
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {
            Cookie::queue("tokenLogin", $response['data']['token'], 1440);
            Cookie::queue("name", $response['data']['user']['name'], 1440);
            // dd($response['data']);
            alert()->success($response['message']);
            return redirect('/admin');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
        // } catch (\Throwable $th) {
        //     alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
        //     return back();
        // }
    }
    public function view(Request $request)
    {

        return view('admin.layouts.login');
    }
    public function logout()
    {
        // try {
            $token = Cookie::get('tokenLogin');
            $client = new Client();
            $data = $client->post($this->urlAPI() . 'admin/logout', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json',
                ],
            ]);
            $response = json_decode($data->getBody()->getContents(), true);
            // dd($response);
            if ($response['status'] == 1) {
                Cookie::queue(Cookie::forget('tokenLogin'));
                return redirect()->route('login');
            }

            return redirect()->route('login');
        // } catch (\Throwable $th) {
        //     alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
        //     return back();
        // }
    }
}
