<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helpers;
use App\Http\Controllers\ControllerBase;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CertificateController extends ControllerBase
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        //
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . "admin/certificate/listCertificate?name=$request->name", [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $responseData = $response['data'];
        // dd($response);
        return view('admin.certificate.index', compact('responseData'));
    }
    public function page($page_id)
    {
        //
        //
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . "admin/certificate/listCertificate?page=$page_id", [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $responseData = $response['data'];
        return view('admin.certificate.index', compact('responseData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'list-category-product', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $category_products = $response['data'];
        return view('admin.certificate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $token = $request->cookie('tokenLogin');

        $certificate = [];
        foreach ($request->all() as $key => $value) {
            if ($key == 'image') {

                if ($request->hasFile('image')) {
                    $certificate[] = Helpers::imageAttribute($request->image, 'image');

                }
            } elseif ($key == 'list_images') {
                if ($request->hasFile('list_images')) {
                    foreach ($request->list_images as $image) {

                        $certificate[] = Helpers::imageAttribute($image, 'list_images[]');

                    }
                }
            } else {
                $certificate[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }

        $url = $this->urlAPI() . 'admin/certificate/add';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $certificate,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.certificate.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    public function addItem(Request $request)
    {
        // dd($request->all());
        $token = $request->cookie('tokenLogin');

        $certificate = [];
        foreach ($request->all() as $key => $value) {
            if ($key == 'image') {

                if ($request->hasFile('image')) {
                    $certificate[] = Helpers::imageAttribute($request->image, 'image');

                }
            } elseif ($key == 'item_image') {
                if ($request->hasFile('item_image')) {
                    if ($request->hasFile('item_image')) {
                        $certificate[] = Helpers::imageAttribute($request->item_image, 'item_image');

                    }
                }
            } else {
                $certificate[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }

        $url = $this->urlAPI() . 'admin/certificate/addItem';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $certificate,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.certificate.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // dd($id);
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'admin/certificate/detail-certificate/' . $id, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        if ($response['status'] == 1) {
            $certificate = $response['data'];
            return [
                'data' => $certificate,
                'code' => 200,
                'action' => route('admin.certificate.update',$id)
            ];
        } else {
            return [
                'code' => 500
            ];
        }
    }
    public function editItem($item_id)
    {
        //
        // dd($item_id);
        $token = Cookie::get('tokenLogin');
        $client = new Client();
        $data = $client->get($this->urlAPI() . 'admin/certificate/detail-certificate-item/' . $item_id, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        if ($response['status'] == 1) {
            $certificate = $response['data'];
            return [
                'data' => $certificate,
                'code' => 200,
                'action' => route('admin.certificate.updateItem',$item_id)
            ];
        } else {
            return [
                'code' => 500
            ];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $token = $request->cookie('tokenLogin');

        $certificate = [];
        foreach ($request->all() as $key => $value) {
            if ($key == 'image') {

                if ($request->hasFile('image')) {
                    $certificate[] = Helpers::imageAttribute($request->image, 'image');

                }
            } elseif ($key == 'list_images') {
                if ($request->hasFile('list_images')) {
                    foreach ($request->list_images as $image) {

                        $certificate[] = Helpers::imageAttribute($image, 'list_images[]');

                    }
                }
            } else {
                $certificate[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }
        $url = $this->urlAPI() . 'admin/certificate/update-certificate/'.$id;
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $certificate,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.certificate.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }

    }
    public function updateItem(Request $request,$item_id)
    {
        // dd($request->all());
        $token = $request->cookie('tokenLogin');

        $certificate = [];
        foreach ($request->all() as $key => $value) {
            if ($key == 'image') {

                if ($request->hasFile('image')) {
                    $certificate[] = Helpers::imageAttribute($request->image, 'image');
                }
            } elseif ($key == 'item_image') {
                if ($request->hasFile('item_image')) {
                    $certificate[] = Helpers::imageAttribute($request->item_image, 'item_image');
                }
            } else {
                $certificate[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }
        // dd($certificate);
        $url = $this->urlAPI() . 'admin/certificate/updateItem/'.$item_id;
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
        ]);
        $req = $client->post(
            $url,
            [
                'multipart' => $certificate,

            ],
            // ['form_params']
        );

        $response = json_decode($req->getBody()->getContents(), true);
        // dd($response);

        if ($response['status'] == 1) {

            alert()->success($response['message']);
            return redirect()->route('admin.certificate.index');
        } else {
            alert()->error($response['message'], '');
            return back();
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {

            $token = Cookie::get('tokenLogin');
            if (!empty($token)) {
                # code...

                $client = new Client();
                $data = $client->delete($this->urlAPI() . 'admin/certificate/delete-certificate/' . $id, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);
                // dd($response);
                if ($response['status'] == 1) {
                    alert()->success($response['message']);
                    return redirect()->route('admin.certificate.index');
                } else {
                    alert()->warning($response['message']);
                    return back();
                }

                return redirect()->back();
            }

            return redirect()->route('login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }
    public function deleteItem($id)
    {
        try {

            $token = Cookie::get('tokenLogin');
            if (!empty($token)) {
                # code...

                $client = new Client();
                $data = $client->delete($this->urlAPI() . 'admin/certificate/delete-certificate-item/' . $id, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                        'Accept' => 'application/json',
                    ],
                ]);
                $response = json_decode($data->getBody()->getContents(), true);
                // dd($response);
                if ($response['status'] == 1) {
                    alert()->success($response['message']);
                    return redirect()->route('admin.certificate.index');
                } else {
                    alert()->warning($response['message']);
                    return back();
                }

                return redirect()->back();
            }

            return redirect()->route('login');
        } catch (\Throwable $th) {
            alert($th)->error('Hệ thống đang được bảo trì. Vui lòng thử lại sau!');
            return back();
        }
    }
}
