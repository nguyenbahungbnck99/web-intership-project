<?php

namespace App\Http\Controllers;

use App\Models\BaseModel;
use GuzzleHttp\Client;

class ControllerBase extends Controller
{
    public function __construct()
    {
        $client = new Client();


        $data = $client->get($this->urlAPI() . 'listSlider', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $listSlider = $response['data'];
        if (!$listSlider) {
            $listSlider = [];
            $listSlider[] = [
                'link' => '#',
                'title' => 'banner 1',
                'image' => asset('template\constra\images\banner\banner1.jpg'),
            ];
            $listSlider[] = [
                'link' => '#',
                'title' => 'banner 2',
                'image' => asset('template\constra\images\banner\banner2.jpg'),
            ];
            $listSlider[] = [
                'link' => '#',
                'title' => 'banner 3',
                'image' => asset('template\constra\images\banner\banner3.jpg'),
            ];
        }

        $data = $client->get($this->urlAPI() . 'list-category-product', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);

        $category_products = $response['data'];

        $data = $client->get($this->urlAPI() . 'listCategoryContent', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $category_contents = $response['data'];


        $data = $client->get($this->urlAPI() . 'getInforWebsite', [
            'headers' => [
                'Authorization' => 'Bearer ',
                'Accept' => 'application/json',
            ],
        ]);
        $response = json_decode($data->getBody()->getContents(), true);
        $infor = $response['data'];
        view()->share([
            'infor' => $infor,
            'category_contents' => $category_contents,
            'category_products' => $category_products,
            'listSlider' => $listSlider,
        ]);
    }
    public function urlAPI()
    {
        return BaseModel::URI_API;
    }
}
