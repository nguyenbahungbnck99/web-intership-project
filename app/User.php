<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\Hash;
use App\Models\Role;
use App\Models\Order;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'cmt', 'role_id', 'address', 'password', 'status', 'code_branch', 'code_ordinal', 'device_id', 'cart_total_money', 'cart_total_product'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function setPasswordAttribute($password)
    {
        if ($password) {
            $this->attributes['password'] = Hash::make($password);
        }
    }
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
    public function getCodeBranchAttribute($code_branch)
    {
        if ($code_branch != null) {
            return 'S' . (String) $code_branch;
        }
        return null;
    }
    public function order()
    {
        return $this->HasMany(Order::class, 'id');
    }
}
