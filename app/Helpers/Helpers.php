<?php

namespace App\Helpers;

class Helpers
{

    const title_home = 'Healthmoomy chuyên phân phối các dòng sản phẩm dinh dưỡng cho mẹ bầu và bé';
    const description_home = ' Healthmoomy chuyên phân phối các dòng sản phẩm dinh dưỡng cho mẹ bầu và bé. Để đảm bảo dương chất đầy đủ cho mẹ và bé thì cần những vi chất cần thiết như đạm, kẽm, máu để mẹ và bé có sức khỏe tốt nhất';
    public static function text_to_slug($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        return $str;
    }
    public static function imageAttribute($image, $name)
    {
        $image_org = $image->getClientOriginalName();
        $image_mime = $image->getmimeType();
        $image_path = $image->getPathname();
        $arr = [
            'name' => $name,
            'filename' => $image_org,
            'Mime-Type' => $image_mime,
            'contents' => fopen($image_path, 'r'),
        ];
        return $arr;
    }
    public static function imageHome()
    {

        return  asset("images/logo.jpg");
    }
    public static function dataSend($request)
    {
        $dataSend = [];
        foreach ($request->all() as $key => $value) {
            if ($key == 'image') {

                if ($request->hasFile('image')) {
                    $dataSend[] = self::imageAttribute($request->image, 'image');
                }
            } elseif ($key == 'list_images') {
                if ($request->hasFile('list_images')) {
                    foreach ($request->list_images as $image) {

                        $dataSend[] = self::imageAttribute($image, 'list_images[]');
                    }
                }
            } elseif ($key == 'logo') {

                if ($request->hasFile('logo')) {
                    $dataSend[] = self::imageAttribute($request->logo, 'logo');
                }
            } else {
                $dataSend[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }
        return $dataSend;
    }
}
